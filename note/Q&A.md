# Java基础
### 如何理解对象
- https://blog.csdn.net/m0_51167384/article/details/114699859
### java 对象头
- https://blog.csdn.net/sumengnan/article/details/125035218
### java 线程的状态及转换
- https://baijiahao.baidu.com?sid=1764378797413438841&wfr=spider&for=pc
### ConcurrentHashMap 原理
- https://blog.csdn.net/weixin_46574815/article/details/123636105
### LinkedHashMap JDK1.8源码分析
- https://blog.csdn.net/weixin_45480785/article/details/114213377
### synchronized 锁作用范围
- https://www.cnblogs.com/enhance/p/17043311.html
### Lock 与 Synchronized 区别
- https://blog.csdn.net/weixin_41010294/article/details/123528508
### java 死锁产生的四个必要条件
- 互斥使用，即当资源被一个线程使用(占有)时，别的线程不能使用
- 不可抢占，资源请求者不能强制从资源占有者手中夺取资源，资源只能由资源占有者主动释放。
- 请求和保持，即当资源请求者在请求其他的资源的同时保持对原有资源的占有。
- 循环等待，即存在一个等待队列：P1占有P2的资源，P2占有P3的资源，P3占有P1的资源。这样就形成了一个等待环路。
### 锁升级的过程
- https://www.jianshu.com/p/31651962ea4c
### java AQS 的原理
- https://zhuanlan.zhihu.com/p/642177775
### 什么是幂等性
- https://www.python100.com/html/104264.html
### 接口保证幂等性的方法
- https://www.zhihuclub.com/185085.shtml
### 什么是IO的多路复用机制
- https://www.cnblogs.commic112p16576545.html
### 什么是IO，BIO，NIO，AIO
- https://blog.csdn.net/qq_28323595/article/details/88909278
### java 17 新特性
- https://zhuanlan.zhihu.com/p/626729509
### OOM 异常排查思路
- https://blog.csdn.net/qq_31363843/article/details/117038001
-----
# JVM
### JVM 主要组成部分
- https://blog.csdn.net/weixin_43946462/article/details/107175745
### JVM 运行时数据区内存模型
- https://blog.csdn.net/weixin_48268269/article/details/125534970
### JVM 类加载机制、双亲委派机制
- https://blog.csdn.net/D812359/article/details/124075194
### CMS 收集器和 G1 收集器的区别
- https://blog.csdn.net/qq_39552268/article/details/122584628
-----
# Spring
### 什么是 BeanDefinition
- 详情：https://zhuanlan.zhihu.com/p/487514250
- 使用：https://blog.csdn.netmeism5articledetails113913388
### Spring 中 bean 的生命周期
- https://www.cnblogs.com/lostxxx/p/16244626.html
### 拦截器和过滤器有什么区别
- https://blog.csdn.net/qq_41973594/article/details/118164586
### 一级缓存、二级缓存和三级缓存
- https://www.inte.net/news235486.html
- https://blog.csdn.net/WX10301075WX/article/details/123904543
### @Resource 与 @Autowired 的区别
- https://blog.csdn.net/wang489687009/article/details/119908896
### @Lazy 原理分析
- https://blog.csdn.net/wang489687009/article/details/120577472
### @Transactional 原理
- https://blog.csdn.net/qq_20597727/article/details/84868035
### @Transactional propagation 事务的传播行为
- https://zhuanlan.zhihu.com/p/459076061
-----
# 数据库
### mysql 索引的使用
- https://blog.csdn.net/weixin_69413377/article/details/126198473
### innodb 内存架构
- https://blog.csdn.net/m0_64818669/article/details/129439548
### mysql 锁分类
- https://blog.csdn.net/zht245648124/article/details/126442426
### mysql 的 MVCC
### 分库分表排序分页查询方案
- https://zhuanlan.zhihu.com/p/468309538
### mybatis 中的一级缓存与二级缓存
- https://blog.csdn.net/zy_zhangruichen/article/details/122592504
### mysql 读写分离的四种方案
- https://blog.csdn.net/weixin_44842613/article/details/116604132
### B树 B+树 B树的区别
- https://blog.csdn.net/jinking01/article/details/115538868
### 布隆过滤器
- https://blog.csdn.net/tongkongyu/article/details/124842847
### MySQL中什么是脏读、幻读、不可重复读
- https://blog.csdn.net/hjl_and_djj/article/details/144152839
-----
# 微服务组件
### SpringCloud 常用组件
- 注册中心 Nacos, Eureka, Zookeeper
- 配置中心 Nacos, Apollo
- 负载均衡 Ribbon, Feign, OpenFeign, Dubbo
- 熔断降级 Hystrix, Sentinel, resilience4j
- 路由网关 zuul2, gateway
- 分布式事务 seata
- 分库分表 Sharding Sphere
### nacos 中的 CAP 理论
- https://blog.csdn.net/weixin_42437633/article/details/105337390
### seata 的 AT 模式原理
- https://blog.csdn.net/qq_31960623/article/details/124681112
### openFeign 原理
### 负载均衡的三种方式
- https://www.php.cn/faq/500515.html
### 路由算法
- https://blog.csdn.net/moshowgame/article/details/123765412
-----
# 消息队列
### kafka 和 rabbitmq 对比
- https://blog.csdn.net/myhes/article/details/83247108
### kafka 解决消息重复
- https://www.cnblogs.com/smartloli/p/11922639.html
### kafka 如何实现高吞吐
- https://blog.csdn.net/WenWu_Both/article/details/122433938
### kafka 怎么保证消息的一致性
- https://blog.csdn.net/m0_65277803/article/details/126745351
### kafka 判断一个节点是否还活着的两个条件
- 节点必须可以维护和ZooKeeper 的连接，Zookeeper 通过心跳机制检查每个节点的连接
- 如果节点是个follower,他必须能及时的同步leader 的写操作， 延时不能太久
### kafka 生产者发消息流程
- https://blog.csdn.net/Tonystark_lz/article/details/126842104
-----
# Redis
### Redis为什么快
### Redis 的持久化 RDB与AOF
- https://blog.csdn.net/weixin_46935110/article/details/127816987
### Redis 跳表 skiplist
- https://blog.csdn.netweixin_46935110articledetails127816987
### Redis key 的过期策略和淘汰机制
- https://blog.csdn.net/u011663149/article/details/95229935
# Mybatis
### Mybatis中的设计模式
- https://blog.csdn.net/xing_jian1/article/details/123973117