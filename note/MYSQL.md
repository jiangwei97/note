# 安装
- [下载（华为镜像）](https://repo.huaweicloud.com/mysql/Downloads/)
## Windows
- 进入 bin 目录
- 初始化mysql数据库执行命令：`mysqld --initialize-insecure`，会在安装目录下出现data文件夹
- 注册mysql服务执行命令：`mysqld -install`
- 启动mysql服务命令输入：`net start mysql`
- 修改默认账户密码输入命令：`mysqladmin -u root password 1234`
- 登录mysql输入命令：`mysql -uroot -p1234`
# SQL
## 基本命令
### 登录mysql
```sql
mysql -h {ip} -uroot -p
```
### 查看mysql数据库表的最后修改时间
```sql
SELECT TABLE_NAME, UPDATE_TIME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = '{数据库名}';
```
### 重置自增ID
```sql
alter table {表名} auto_increment={开始ID}
```
### 列出所有表名
```sql
show tables;

SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'your_database_name';
```
## 锁
```sql
-- 查看正在进行中的事务
SELECT * FROM information_schema.INNODB_TRX
-- 查看正在锁的事务
SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCKS;
-- 查看等待锁的事务
SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCK_WAITS;
-- 查询是否锁表
SHOW OPEN TABLES where In_use > 0;
-- 在发生死锁时，这几种方式都可以查询到和当前死锁相关的信息。
-- 查看最近死锁的日志
show engine innodb status
-- 查看当前正在进行中的进程
show processlist
-- 杀掉进程对应的进程 id
kill id
```
## 字符串
### 取JSON属性
```sql
TRIM(BOTH '\"' FROM JSON_EXTRACT(task_data, '$.id'))  
``` 
### 从头/尾截取字符串
```sql
left(str,lenth) ,right(str,lenth) 
``` 
### 任意截取字符串
```sql
substring ({被截取字段}, {从第几位开始截取}, {截取长度(不填截取到最后)})
``` 
### 替换字符串
```sql
REPLACE(str,old_string,new_string)
``` 
### 按关键字截取字符串
```sql
select substring_index("",".",1) 
-- www
-- （注：如果关键字出现的次数是负数 如-2 则是从后倒数，到字符串结束）
``` 
### 拼接字符串
```sql
SELECT CONCAT('index-', id, '-XXX') FROM TABLE
```
### GROUP BY时多值逗号分隔：
```sql
SELECT id, `name`, GROUP_CONCAT( tool_name ) tool_names FROM TABLE GROUP BY id
``` 
### 大小写
```sql
-- 转大写
select upper('abc')
-- 转小写
select lower('ABC')
-- 首字母大写，其他小写
select (upper(left('abCdsE',1))+lower(SUBSTRING('abCdsE',2,len('abCdsE'))) )
``` 

## 日期
### 日期： https://www.cnblogs.com/awzf/p/9987526.html
### 查询今天的所有记录：
```sql
select * from `article` where to_days(date_format(from_UNIXTIME(`add_time`),'%Y-%m-%d')) = to_days(now());  
select * from `article` where to_days(`add_time`) = to_days(now());
```   
### 查询昨天的所有记录
```sql
select * from `article` where to_days(now()) = 1 + to_days(`add_time`);
```  
### 近7天的信息记录：
```sql
select * from `article` where date_sub(curdate(), INTERVAL 7 DAY) <= date(`add_time`);
```  
### 近30天的信息记录：
```sql
select * from `article` where date_sub(curdate(), INTERVAL 30 DAY) <= date(`add_time`);
```  
### 查询本月的记录
```sql
select * from `article` where date_format(`add_time`, '%Y%m') = date_format(curdate() , '%Y%m');
```  
### 上一个月的记录
```sql
select * from `article` where period_diff(date_format(now() , '%Y%m') , date_format(`add_time`, '%Y%m')) =1;
```  
### 两个日期相减的秒数
```sql
select * from proxy where UNIX_TIMESTAMP(finish_time)### UNIX_TIMESTAMP(satrt_time) > 100;
```  
### 两个日期的差值
```sql
-- 返回两个TIME或DATETIME值之间的差值
TIMEDIFF(dt1, dt2);
-- 以使用TIMESTAMPDIFF函数来计算两个日期或时间之间的小时差。
SELECT TIMESTAMPDIFF(HOUR, start_time, end_time) AS hour_diff FROM your_table;
```
### 日期格式化
```sql
SELECT DATE_FORMAT(CURDATE(), '%Y%m');
```
## 分组
### 根据job_id分组，保留last_updated最大的值
```sql
SELECT t.* 
FROM table t
RIGHT JOIN ( 
    SELECT SUBSTRING_INDEX( group_concat( id ORDER BY last_updated DESC ), ',', 1 ) AS id 
    FROM table 
    GROUP BY job_id 
) tmp 
ON t.id = tmp.id order by t.id;
```
## 查询
### 行转列、列转行
- https://blog.csdn.net/qq_47776903/article/details/121379395
### WHEN CASE
```sql
# 简单CASE函数法
CASE 要判断的字段或表达式
  WHEN 常量1 THEN 要显示的值1或语句1（如果是语句，结尾需要加上分号;）
 [WHEN 常量2 THEN 要显示的值2或语句2]
 […]
 [ELSE 要显示的值n或语句n]
END

# CASE搜索函数法
CASE
  WHEN 条件1 THEN 要显示的值1或语句1（如果是语句，结尾需要加上分号;）
 [WHEN 条件2 THEN 要显示的值2或语句2]
 […]
 [ELSE 要显示的值n或语句n]
END
```