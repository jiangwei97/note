# 官网
 - [官网](http://www.lua.org/)
 - [文档](https://wiki.luatos.com/)
 - [在线调试](https://wiki.luatos.com/_static/luatos-emulator/lua.html)
 - [参考手册](https://cloudwu.github.io/lua53doc/contents.html)
# 语法
# 变量
```lua
-- 全局变量赋值
a = 1
-- 局部变量赋值
local b = 2
-- 多个变量同时赋值
local c, d = 3, 4
-- 打印
print(a, b, c, d)
print(e) -- e未声明 打印nil(等同于null)
```
# number类型
## 声明
```lua
-- 12的十六进制
a = 0x12
-- 科学计数法 2乘10的5次方
b = 2e5
print(a, b) -- 18	200000.0
```
## 运算符
```lua
-- + - * / % 加减乘除余
-- 2的4次方
print(2^4) -- 16.0
-- 2进制左移右移
print(1<<3) -- 8
print(15>>2) -- 3
```
# String类型
## 声明 "" 或者 ''
```lua
a = "aaaaa"
b = 'bbbbb'
c = 'ccccc\nddddd\teeeee'
print(a, b, c)
-- aaaaa	bbbbb	ccccc
-- ddddd	eeeee
```
## 字符串原始值 [[]]
```lua
a = [[
aaa d
bbb
ccc/tddd
]]
print(a) -- 打印中括号内原始值
```
## 字符串相加 ..
```lua
a = 'hello '
b = 'world'
print(a..b) -- hello world
```
## string/number转换
```lua
-- 数字转字符串
a = tostring(10)
-- 字符串转数字
b = tonumber("10")
c = tonumber("abc") -- 转换失败 c为nil
```
## 字符串长度
```lua
a = 'abcde'
print(#a) -- 5
```
## ASCII值
```lua
-- ASCII值转字符串
str = string.char(0x30, 0x31, 0x32, 0x33)
print(str) -- 0123
-- 获得str第二个字符的ASCII值
v = string.byte(str, 2)
print(v) -- 49
```
# 函数
## 声明
```lua
function f(a, b)
    return a + b
end

r = f(3, 2)
print(r)
```
## 返回多个值
```lua
function f(a, b)
    return a, b
end

local i, j = f(3, 2)
print(i) -- 3
print(j) -- 2
```
# table
## 声明 {}
```lua
-- 一个table元素可以放数组、字符串、table、函数
a = {1, "abc", {}, function() end}
-- 元素下标从1开始
print(a[1])
print(a[2])
print(a[3])
print(a[4])
print(a[5])
a[5] = "def" -- 给下标为5的位置赋值
print(a[5])
-- [11:04:27] 1
-- [11:04:27] abc
-- [11:04:27] table: 0x154
-- [11:04:27] function: 0x155
-- [11:04:27] nil
-- [11:06:08] def
```
## 获取table长度
```lua
a = {1, "abc", {}, function() end}
print(#a) -- 4
a[5] = "def"
print(#a) -- 5
```
## 插入
```lua
-- table末尾插入
a = {1, "abc", {}, function() end}
table.insert(a, "A")
print(a[5]) -- A
-- 指定位置插入 后面的元素都向后移一位
table.insert(a, 2, "B")
print(a[2]) -- B
-- a:{1, "B", "abc", {}, function() end, "A"}
```
## 删除
```lua
a = {1, "B", "abc", {}, function() end, "A"}
-- 移除table a的第二个元素，后面的元素都向前移动一位
-- 返回值是移除的元素
local b = table.remove(a, 2)
print(#a) -- 5
print(b) -- B
```
## 字符串作为下标的table
```lua
t = {
    a = 1,
    b = "ABC",
    c = {},
    d = function() end,
    [",:;"] = "DE"
}
print(t["b"]) -- ABC
print(t['b']) -- ABC
print(t.b) -- ABC
print(t[",:;"]) -- DE
t["flag"] = "flag"
print(t.flag) -- flag
print(t.xxx) -- nil
```
## 全局表 _G
```lua
-- 全局变量都在全局表_G中
a = "ABC"
print(_G.a) -- ABC
print(_G.table.insert) -- function: 0x155
```
# 布尔类型
## 布尔运算
```lua
a = false
b = true
print(a) -- false
print(b) -- true
print(1>2) -- false
print(1<2) -- true
print(1>=2) -- false
print(1<=2) -- true
print(1==2) -- false
print(1~=2) -- 不等于 true
print(a and b) -- 与 false
print(a or b) -- 或 true
print(not a) -- 非 true
-- 只有nil和false代表假 其他代表真（0代表真）
c = 5
print(c > 10 and "yes" or "no") -- no
```
## 分支语句
```lua
a = false
b = true
if a then
    print("分支1")
elseif b then
    print("分支2")
else
    print("分支3")
end
```
## 循环
### for循环
```lua
for i=1,10 do
    -- 循环途中i不能修改
    print(i)
end
-- 1 2 3 4 5 6 7 8 9 10

for i=1,10,2 do
    print(i)
end
-- 1 3 5 7 9
```
### while
```lua
local n = 10
while n > 1 do
    n = n-1
    print(n)
end
-- 9 8 7 6 5 4 3 2 1
```
### break
```lua
for i=1,10 do
    print(i)
    if i==5 then break end
end
-- 1 2 3 4 5

local n = 10
while n > 1 do
    n = n-1
    print(n)
    if n==5 then break end
end
-- 9 8 7 6 5
```