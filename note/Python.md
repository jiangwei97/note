# 基本命令
#### 显示已安装包及其版本
```python
# 显示所有包和版本
pip list
# 同上 格式不一样
pip freeze
# 显示指定包版本
pip show {package}
python3 -m django --version
```
#### 生成requirement.txt
```python
pip list --format=freeze > requirements.txt
```
# 虚拟环境
### 创建虚拟环境
```python
python -m  venv {env_name} # env_name是虚拟环境的名称
```
### 激活虚拟环境
```python
# LINUX
{env_name}\scripts\activate
# WINDOWS
source {env_name}/bin/activate
```
### 退出虚拟环境
```python
deactivate
```
# 执行CMD命令
```python
def exec_shell(cmd):
    cmd_result = {}
    res = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        stdout, stderr = res.communicate()
        cmd_result["input_str"] = str(stdout, 'UTF-8')
        cmd_result["code"] = res.returncode
        res.stdout.close()
    except Exception as e:
        res.kill()
        res.terminate()
        cmd_result["code"] = -1
        cmd_result["input_str"] = '[ERROR] Unknown Error : ' + str(e)
    return cmd_result
```
# 字符串
### 字符串、数组长度
```python
len({str/arr})
```
### int转str
```python
str({num})
```
### for循环
```python
# 加强for
list = [1, 2, 3, 4, 5]
for item in list:
    print(item)
# 带索引for
for i, diff_file in enumerate(diff_file_list):
    print(i + ":" + item)
# 循环5次
for i in range(0, 5):
    print(i)
```
### 对象转json
```python
item = {
    "name": "zhangsan",
    "age": 25,
    "region": "china"
}
item_json = json.dumps(item)
```
# 文件
### 写入文件
```python
with open('C:/test/xxx.json', mode='w', encoding='utf-8') as f:
    f.write(<TEXT>)
    f.close
```
### 查询
```python
# 文件行列表
path = r"c:\test\xxx.josn"
f = open(path, 'r')
lines = f.readlines()
```
### 追加
```python
# 向文件后追加字符串
path = r"c:\test\xxx.josn"
f = open(path, 'a', encoding='utf-8')
f.write('context')
```
### 清空内容
```python
# 向文件后追加字符串
f= open(file_path, mode="r+")
# 清空
f.truncate()
# 新内容
f.write("context")
```