# JsonPath For Java
## pom.xml
```xml
<!-- https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path -->
<dependency>
    <groupId>com.jayway.jsonpath</groupId>
    <artifactId>json-path</artifactId>
    <version>2.7.0</version>
</dependency>
```
## 使用
```java
import com.jayway.jsonpath.JsonPath;

public void getStrByXmlPath(String jsonStr, String jpath) {
    Object read = JsonPath.read(jsonStr, jpath)
    System.out.println(read);
}

```
## JsonPath语法
``` json
{
    "key": "KEY_VALUE",
    "list": [
        {
            "id": "1",
            "name": "A"
        },
        {
            "id": "2",
            "name": "B"
        },
        {
            "id": "3",
            "name": "C"
        }
    ]
}
```
#### 获得key的值
```java
String jpath = "$.key" 
// KEY_VALUE
```
#### 获得list的长度
```java
String jpath = "$.list.length()"
// 3
```
#### 获得list的第二条
```java
String jpath = "$.list[1]"
// {id=2, name=B}
```
#### 获得list的第二条中的name
```java
String jpath = "$.list[1].name"
// B
```
#### 获得name的列表
```java
String jpath = "$.list[*].name"
// ["A","B","C"]
```
#### 获得id=1的列表1
```java
String jpath = "$.list.[?(@.id == '1')]" 
// [{"id":"A","name":"a"}]
```
#### 获得id=1和id=2的列表
```java
String jpath = "$.list.[?(@.id in ['1', '2'])]"
// [{"id":"1","name":"A"},{"id":"2","name":"B"}]
```
