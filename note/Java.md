# JAVA
## 生成证书
1. 使用管理员身份运行命令提示符
2. 使用命令进入到jdk安装路径的bin文件夹下
3. 随后使用命令生成密钥：
```bash
keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
```
## 事务回滚
```java
class Main{
    public static void main(String[] args) {
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }
}
```
## 多线程计数器
```java
class Main{
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        CountDownLatch countDownLatch = new CountDownLatch(list.size());
        for (String item : list) {
            countDownLatch.countDown(); // 计数器-1
            if (countDownLatch.getCount() == 0) { // 计数器为0
                // TODO
            }
        }
    }
}
```
## 线程池
### 创建线程池
```java
class Main{
    private ThreadPoolExecutor getExecutor(String threadPoolName) {
        return new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                Integer.MAX_VALUE,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(),
                new ThreadFactoryBuilder().setNamePrefix(threadPoolName).build(),
                (r, exec) -> {
                    log.error("Temp Executor full");
                    rejectedMethod(r);
                }
        );
    }
    
    private void rejectedMethod(Runnable r){
        // TODO 拒绝策略
    }
}
```
### 向线程池添加任务
```java
class Main{
    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor = getExecutor(threadName);
        threadPoolExecutor.submit(() -> {
            try {
                // TODO 执行的任务
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                // TODO
            }
        });
        
    }
}
```
### 判断线程池是否已满
```java
public class Main {
    public static void main(String[] args) {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                10,  // 核心线程数
                10,  // 最大线程数
                0,   // 空闲线程的存活时间
                TimeUnit.MILLISECONDS, // 存活时间的单位
                new SynchronousQueue<>(), // 用于保存等待执行的任务的阻塞队列
//                (runnable, exec) -> {
//                    if (exec.getQueue().remainingCapacity() == 0) {
//                        if (exec.getQueue().remainingCapacity() == 0) {
//                            System.out.println("暂无可用线程");
//                        } else {
//                            exec.execute(runnable);
//                        }
//                    }
//                }
                new ThreadPoolExecutor.AbortPolicy() // 丢弃任务并抛出RejectedExecutionException异常
        );
        for (int i = 0; i < 15; i++) {
            try {
                executor.execute(() -> {
                    try {
                        Thread.sleep(2000); // 模拟任务执行时间
                        System.out.println("Task executed successfully");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }catch (RejectedExecutionException e){
                System.out.println("线程池已满");
            }
        }
        executor.shutdown();
        try {
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
### springboot线程池bean
```java
package com.jiangwei.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/4/11 10:54
 */

@EnableAsync
@Configuration
@Slf4j
public class ExecutorConfig implements AsyncConfigurer {

    @Bean("asyncExecutor")
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(20);
        executor.setQueueCapacity(100);
        executor.setKeepAliveSeconds(60);
        // 核心线程数设置超时等待
        executor.setAllowCoreThreadTimeOut(false);
        executor.setThreadNamePrefix("asyncExecutor");
        // 拒绝策略
        executor.setRejectedExecutionHandler((r, executor1) -> {
            log.error("syncExecutor full");
            executorFailSendMQ(r);
        });
        // 优雅停机
        executor.setAwaitTerminationSeconds(60);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }

    private void executorFailSendMQ(Runnable r){
        // TODO
    }
}

```
## 跨域
```java
@Component
public class CORSFilter implements Filter {
 
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Credentials", "true");
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader(
            "Access-Control-Allow-Headers", 
            "Content-Type, X-CAF-Authorization-Token, sessionToken, X-TOKEN"
            );
        if (((HttpServletRequest) request).getMethod().equals("OPTIONS")) {
            response.getWriter().println("ok");
            return;
        }
        chain.doFilter(request, response);
    }
    @Override
    public void destroy() {
    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
```
## 数学运算
```xml
<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-jexl3 -->
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-jexl3</artifactId>
    <version>3.2.1</version>
</dependency>
```
```java
class Main{
    public static String method(String str) {
        JexlEngine jexlEngine = new JexlBuilder().create();
        JexlExpression jexlExpression = jexlEngine.createExpression(str);
        return jexlExpression.evaluate(null).toString();
    }
    
    public static void main(String[] args) {
        System.out.println(method("1+1")); // 2
        System.out.println(method("10+2*3")); // 16
        System.out.println(method("19%4")); // 3
    }
}
```
## 深拷贝
### ObjectMapper
```java
class Main{
    ObjectMapper objectMapper = new ObjectMapper();
    User oldUser = new User();
    User newUser = objectMapper.readValue(
            objectMapper.writeValueAsString(oldUser), User.class
    );
}
```
### Apache Commons BeanUtils 库
- 使用 BeanUtils.cloneBean() 方法可以对一个对象进行浅拷贝。
```java
Person person1 = new Person("Alice", 20);
Person person2 = BeanUtils.cloneBean(person1);
```
### Apache Commons Lang 库
- Apache Commons Lang 库提供了 SerializationUtils.clone() 方法，可以对对象进行深拷贝。
```java
Person person1 = new Person("Alice", 20);
Person person2 = SerializationUtils.clone(person1);
```
### Spring Framework
- 使用 Spring Framework 的 ObjectUtils.clone() 方法可以对一个对象进行深拷贝。
```java
Person person1 = new Person("Alice", 20);
Person person2 = (Person) ObjectUtils.clone(person1);
```
# 字符串
## String反向format
```java
package com.jiangwei.test;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2023-01-11 16:44
 **/

public class Main{
    public static void main(String[] args) {
        String format = "query: \"{0}\" variables: \"{1}\"";
        String text = MessageFormat.format(format, "asdasddsadasd", "{\"aaa\":\"bbb\"}");
        System.out.println(text);
        System.out.println(Arrays.toString(reFormat(format, text)));
    }

    private static String[] reFormat(String regex, String text) {
        regex = regex.replaceAll("\\{([0-9])*\\}", "(.+)");
        regex = regex.replaceAll("\\[",  "\\\\[");
        Matcher m = Pattern.compile(regex).matcher(text);
        String[] paramVars;
        if (m.find()) {
            int count = m.groupCount();
            paramVars = new String[count];
            int start = 1;
            while (start <= count) {
                paramVars[start - 1] = (m.group(start++));
            }
            return paramVars;
        }
        return null;
    }
}

```
# SpringBoot启动时运行
## 实现CommandLineRunner接口
```java
@Order(1) // 加载顺序，值越小，优先级越高
@Component
public class TaskRun2 implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("执行方法");
    }
}
```
## 实现ApplicationRunner接口
 - 如果同时实现ApplicationListener和CommandLineRunner两个接口，ApplicationRunner接口的方法先执行，CommandLineRunner后执行
```java
@Component
@Order(value = 1)
public class AfterRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("执行方法");
    }
}
```
## 注解@PostConstruct 
 - 缺点：极端情况下，会出现该方法依赖的组件还没被加载到容器里面，导致方法执行失败。
```java
@Slf4j
@Component
public class Main{
    @PostConstruct
    public void show(){
        System.out.println("执行方法");
    }
}
```
## 启动类调用
```java
public class Application {
    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(Application.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = StringUtils.isEmpty(env.getProperty("server.servlet.context-path"))
                ? "" : env.getProperty("server.servlet.context-path");
        log.info("\n" +
                "--------------------------------------------------------------\n" +
                "\tSwagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n" +
                "--------------------------------------------------------------\n");
        TaskService taskService = application.getBean(TaskService.class);
        taskService.task();
    }
}
```
# 自定义注解
## 依赖
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```
## annotation
```java
import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Access {
    String name() default "";
}
```
## aspect
```java
package com.jiangwei.common.annotation.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2023-05-11 16:22
 **/

@Aspect
@Component
public class AccessAspect {

    @Pointcut("@annotation(com.jiangwei.common.annotation.Access)")
    public void points(){

    }

    @Around("points()")
    public Object around(ProceedingJoinPoint point) {
        // 方法名
        System.out.println(point.getSignature() + " start");
        Object proceed;
        try {
            // 参数列表
            Object[] args = point.getArgs();
            // 注解上写的属性
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();
            Access annotation = method.getAnnotation(Access.class);
            String name = annotation.name();
            // 执行方法
            proceed = point.proceed(args);
            System.out.println(Arrays.toString(args));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        System.out.println(point.getSignature() + " end");
        return proceed;
    }

}

```
## AnnotationResolver 解析方法上的参数 #{xxx}
```java
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.Objects;

public class AnnotationResolver {
    private static AnnotationResolver resolver;

    public static AnnotationResolver newInstance() {
        return Objects.requireNonNullElseGet(resolver, () -> resolver = new AnnotationResolver());
    }

    public Object resolver(JoinPoint joinPoint, String str) {
        if (str == null) {
            return null;
        }
        Object value = null;
        if (str.matches("#\\{\\D*\\}")) {// 如果name匹配上了#{},则把内容当作变量
            String newStr = str.replaceAll("#\\{", "").replaceAll("\\}", "");
            if (newStr.contains(".")) { // 复杂类型
                try {
                    value = complexResolver(joinPoint, newStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                value = simpleResolver(joinPoint, newStr);
            }
        } else {
            value = str;
        }
        return value;
    }


    private Object complexResolver(JoinPoint joinPoint, String str) throws Exception {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String[] names = methodSignature.getParameterNames();
        Object[] args = joinPoint.getArgs();
        String[] arr = str.split("\\.");
        for (int i = 0; i < names.length; i++) {
            if (arr[0].equals(names[i])) {
                Object obj = args[i];
                Method method = obj.getClass().getDeclaredMethod(getMethodName(arr[1]), null);
                Object value = method.invoke(args[i]);
                return getValue(value, 1, arr);
            }
        }
        return null;
    }

    private Object getValue(Object obj, int index, String[] arr) {
        try {
            if (obj != null && index < arr.length - 1) {
                Method method = obj.getClass().getDeclaredMethod(getMethodName(arr[index + 1]), null);
                obj = method.invoke(obj);
                getValue(obj, index + 1, arr);
            }
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getMethodName(String name) {
        return "get" + name.replaceFirst(name.substring(0, 1), name.substring(0, 1).toUpperCase());
    }

    private Object simpleResolver(JoinPoint joinPoint, String str) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String[] names = methodSignature.getParameterNames();
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < names.length; i++) {
            if (str.equals(names[i])) {
                return args[i];
            }
        }
        return null;
    }
}
```
# 文件
## 文件下载
```java
public class Main{
    private ResponseEntity<FileSystemResource> download1(File file) {
        if (file == null) {
            return null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename=" + file.getName());
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Last-Modified", new Date().toString());
        headers.add("ETag", String.valueOf(System.currentTimeMillis()));
        return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType("application/octet-stream")).body(new FileSystemResource(file));
    }

    // 解决资源关闭过早
    private ResponseEntity<ByteArrayResource> download2(File file) {
        if (fileContent == null) {
            return null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename=" + file.getName());
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Last-Modified", new Date().toString());
        headers.add("ETag", String.valueOf(System.currentTimeMillis()));
        byte[] bytes = FileUtils.readFileToByteArray(file);
        ByteArrayResource resource = new ByteArrayResource(bytes);
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}
```
## 文件上传
```java
public class Main{
    public Result<String> uploadfile(MultipartFile file) {
        if (file == null || file.getOriginalFilename() == null) {
            log.error("script file is null");
            return Result.fail("script file is null");
        }
        String originalName = file.getOriginalFilename();
        if (!checkFileType(originalName)) {
            log.error("file type is not support");
            return Result.fail("file type is not support");
        }
        String localPath = "/data/xxdir";
        File folder = new File(localPath);
        if (!folder.exists()) {
            boolean mkdir = folder.mkdirs();
            if (!mkdir) {
                log.error("mkdir failed");
                return Result.fail("mkdir failed");
            }
        }
        Path copyPath = new File(localPath + File.separator + originalName).toPath();
        try {
            Files.copy(file.getInputStream(), copyPath, StandardCopyOption.REPLACE_EXISTING);
            return Result.ok("upload success");
        } catch (IOException e) {
            log.error("save script file failed");
            e.printStackTrace();
            return Result.fail(e.getMessage());
        }
    }
}
```
## 读取文件文本内容
```xml
<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>1.4</version>
</dependency>
```
```java
@Test
public void readFile() throws IOException {
    File file = new File("D:\test.json");
    String context = FileUtils.readFileToString(file, "UTF-8");
    System.out.println(context);
}
```
# Stream
## 排序sorted
```java
package com.demo.test;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<SortModel> list = new ArrayList<>();
        list.add(new SortModel("f", 4, 5));// 1
        list.add(new SortModel("a", 1, 6));// 5
        list.add(new SortModel("d", 3, 2));// -1
        list.add(new SortModel("c", 3, 1));// -2
        list.add(new SortModel("e", 3, 3));// 0
        list.add(new SortModel("b", 2, 4));// 2
        // 按照value的升序
        List<SortModel> valueAsc = list.stream()
                .sorted(Comparator.comparing(SortModel::getValue))
                .collect(Collectors.toList());
        System.out.println(valueAsc);
        // 按照value的降序
        List<SortModel> valueDesc = list.stream()
                .sorted(Comparator.comparing(SortModel::getValue).reversed())
                .collect(Collectors.toList());
        System.out.println(valueDesc);
        // 先按照value的升序排序，再按照type升序排序
        List<SortModel> valueTypeAsc = list.stream()
                .sorted(Comparator.comparing(SortModel::getValue).thenComparing(SortModel::getType))
                .collect(Collectors.toList());
        System.out.println(valueTypeAsc);
        // 先按照value的升序排序，再按照type降序排序
        List<SortModel> valueTypeDesc = list.stream()
                .sorted(Comparator.comparing(SortModel::getValue).thenComparing((o1, o2)->o2.getType()-o1.getType()))
                .collect(Collectors.toList());
        System.out.println(valueTypeDesc);
        // 按照 type-value 排序
        List<SortModel> typeMinusValueAsc = list.stream()
                .sorted(Comparator.comparing((o)->o.getType()-o.getValue()))
                .collect(Collectors.toList());
        System.out.println(typeMinusValueAsc);
    }
}
@Data
@AllArgsConstructor
class SortModel{
    private String name;
    private Integer value;
    private Integer type;
}
```
## list 转 map
```java
public class Main{
    public static void main(String[] args){
        //以id为主键转为map
        Map<Long, User> map = list.stream().collect(Collectors.toMap(User::getId,Function.identity()));

        // 不想返回对象，只返回对象里某个属性时 采用这种方式
        Map<Long, String> map = list.stream().collect(Collectors.toMap(User::getId, User::getAge, (key1, key2) -> key2));

        Map<Integer, List> map = list.stream().collect(Collectors.groupingBy(User::getId));
    }
}
```
## list 分组
```java
public class Main{
    public static void main(String[] args){
        Map<String, List<User>> collect = userList.stream().collect(Collectors.groupingBy(User::getOrg));
    }
}
```
## list 去重
### 简单去重
```java
public class Main{
    public static void main(String[] args){
        List<Integer> list = List.of(1, 2, 3, 2, 1, 4, 5, 4);
        List<Integer> distinctList = list.stream().distinct().collect(Collectors.toList());
    }
}
```
### 对象去重
```java
// 根据User的id去重
public class Main{
    public static void main(String[] args){
        List<User> distinctPeople = people.stream()
        .collect(Collectors.toMap(User::getId, Function.identity(), (p1, p2) -> p1))
        .values()
        .stream()
        .collect(Collectors.toList());
    }
}
```
### 多字段去重
```java
// 根据User的name, code, phone去重
public class Main{
    public static void main(String[] args){
        List<User> list = userList.stream()
	    .collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
            new TreeSet<>
            (Comparator.comparing(User::getName)
            .thenComparing(User::getCode)
            .thenComparing(User::getPhone))),
            ArrayList::new));
    }
}
```
# JVM
## jps
- 查看运行的java进程
```bash
jps
```
- 查看运行的java文件
```bash
jps -l
```
## [jstat](https://blog.csdn.net/javalingyu/article/details/124800644)
- gc状态
```bash
# S0C ：年轻代中S0区的容量 （字节）
# S1C ：年轻代中S1区的容量 (字节)
# S0U ：年轻代中S0区目前已使用空间 (字节)
# S1U ：年轻代中S1区目前已使用空间 (字节)
# EC ：年轻代中Eden区的容量 (字节)
# EU ：年轻代中Eden区目前已使用空间 (字节)
# OC ：老年代的容量 (字节)
# OU ：老年代目前已使用空间 (字节)
# YGC ：从应用程序启动到采样时年轻代中GC次数
# YGCT ：从应用程序启动到采样时年轻代中GC所用时间(s)
# FGC ：从应用程序启动到采样时老年代(全GC)GC次数
# FGCT ：从应用程序启动到采样时老年代(全GC)GC所用时间(s)
# GCT：从应用程序启动到采样时GC用的总时间(s)
jstat -gc <PID>
# -gc :打印相关的统计参数
# -t:  在每行日志之前加上JVM的启动时间
# <PID>目标Java进程的ID
# 10000: jstat命令执行间隔时间(milliseconds)，10000表示每10s打印一行日志
# 10: jstat命令的执行次数，(和上面的时间间隔一起，表示jstat会每10s执行1次，总共执行10次).
jstat -gc -t <PID> 10000 10
```
- 类装载情况
```bash
jstat -class <PID>
```
## [jstack](https://blog.csdn.net/weixin_44588186/article/details/124680586)

## java VisualVM
- 启动工具
```bash
jvisualvm
```
- 插件下载
```
https://visualvm.github.io/pluginscenters.html
```
## Arthas
- [下载](https://arthas.aliyun.com/arthas-boot.jar)
- 启动
```bash
java -jar arthas-boot.jar
```
- 退出
```bash
stop
```
- 看板
```bash
dashboard
```
- 线程状态
```bash
thread <ID>
```
- 查看死锁
```bash
thread -b
```
- 反编译代码
```bash
jad <REFERENCE_PATH>
# jad com.jiangwei.Test
```
# JAVA 11 HttpClient
```java
public class Main{
    public static void main(String[] args) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://api.github.com/xxx"))
                .header(HttpHeaders.AUTHORIZATION, "XXX")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .timeout(Duration.ofSeconds(15))
                .POST(HttpRequest.BodyPublishers.ofString("""
                        {"aaa": "bbb"}
                        """))
                .build();
        try {
            String body = client.send(httpRequest, HttpResponse.BodyHandlers.ofString()).body();
            System.out.println(body);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
```