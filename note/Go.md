# 安装
# 指令
 - 版本: `go version`
 - 环境信息: `go env`
 - 设置环境信息： `go env -w <key>=<val>`
# Hello World
## 测试
```go
// go run helloWorld.go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello World")
}

```
# 语法
## 变量
### 声明和初始化
``` go
import (
	"fmt"
)

func main() {
	// 声明 赋值 追加
	var name string = "jiang "
	var age int = 20
	name += "wei"
	fmt.Println(name)
	fmt.Println(age)
	// 只声明 默认值
	var (
		region	string
		addr	string
	)
	region = "beijing"
	fmt.Println(region)
	fmt.Println(addr)
	// := 自动推导类型
	sex := "man"
	fmt.Println(sex)
	// 打印类型
	fmt.Printf("%T, %T", name, age)
    // 打印内存地址 重新赋值内存地址不变
    var num int
    num = 100
    fmt.Printf("num:%d,内存地址: %p", num, &num)
    fmt.Println()
    num = 200
    fmt.Printf("num:%d,内存地址: %p", num, &num)
}
```
### string方法
```go
func main() {
	str := "hello world"
	// 打印字符串长度
	fmt.Println(len(str)) // 11
	// 打印索引0的char
	fmt.Println(str[0]) // 104
	// 转字符串
	fmt.Printf("%c", str[0]) // h
}
```
### 变量交换
```go
// 变量交换
a := 100
b := 200
println(a, b)
a, b = b, a
println(a, b)
```
### 匿名函数
```go
func main() {
    // 只想要a, 不想要b
	a, _ := test()
	println(a)
}
// 返回两个结果, 类型分别是int, string
func test()(int, string){
	return 100, "abc"
}
```
### 变量的作用域
- 同java
- Go语言程序中全局变量与局部变量名称可以相同,但是函数体内的局部变量会被优先考虑
### 常量
```go
func main() {
	const URL1 string = "www.baidu.com"
	const URL2 = "www.google.com"
	const a, b, c = 2.5, "jiangwei", false
	fmt.Println(URL1)
	fmt.Println(URL2)
	fmt.Println(a, b, c)
}
```
### iota
iota,特殊常量,可以认为是一个可以被编译器修改的常量  
iota是go语言的常量计数器  
iota 在 **const关键字出现时**将被重置为 0(const 内部的第一行之前),const 中每新增一行常量声明将使 iota 计数一次(iota 可理解为 const 语句块
中的行索引),
```go
func main() {
	const (
		a = iota
		b = iota
		c = iota
		d
		e
		f = "abc"
		g
		h = iota
	)
	fmt.Println(a, b, c, d, e, f, g, h)
}
```
## 数据类型
### 布尔类型
- bool
- 默认：false
```go
var flag1 bool
var flag2 bool = true
fmt.Printf("%T, %t\n", flag1, flag1)
fmt.Printf("%T, %t", flag2, flag2)
```
### 数值类型
- 整数 int
    - 默认0
    - 范围 -9223372036854775808~9223372036854775807
- 整数 byte
    - 默认0
    - 范围0~255
    - 相当于uint8
- 浮点数 float64
    - 默认0.000000
```go
var age int = 18
var money float64 = 3.14
fmt.Printf("%T, %d\n", age, age)
// 默认保留6位小数
fmt.Printf("%T, %f", money, money)
// 保留几小数 四舍五入
fmt.Printf("%T, %.1f", money, money)
```
### 字符串类型
- 字符串 默认空字符串
```go
func main() {
	var str string
	str = "hello"
	fmt.Printf("%T, %s\n", str, str)
	// 单引号
	v1 := 'A' //ASCII
	v2 := "A"
	fmt.Printf("%T, %d\n", v1, v1)
	fmt.Printf("%T, %s\n", v2, v2)
	//string, hello
	//int32, 65
	//string, A
}
```
### 数组
```go
// 长度为5的数据
arr := [5]int{1, 2, 4, 5, 6}
// 1, 2, 4, 5, 6
arr := [4]int{1, 2, 4, 5}
// 1, 2, 4, 5, 0
```
### 数据类型转换
- string()
- int()
- float()
- bool()
- byte()
```go
func main() {
	a := 3
	b := 5.3
	// int 转 float64
	c := float64(a)
	// float64 转 int
	d := int(b)
	fmt.Printf("%T, %d\n", a, a)
	fmt.Printf("%T, %f\n", b, b)
	fmt.Printf("%T, %f\n", c, c)
	fmt.Printf("%T, %d\n", d, d)
	e := float64(a) + b
	fmt.Printf("%T, %f\n", e, e)
	//int, 3
	//float64, 5.000000
	//float64, 3.000000
	//int, 5
	//float64, 8.300000
}
```
## 运算符
### 算数运算符
- 同java：`+, -, *, /, %, ++, --`
### 关系运算符
- 同java：`==, !=, >, <, >=, <=`
### 逻辑运算符
- 同java：`&&, ||, !`
### 位运算符
- 同java：`&, |, ^, >>, <<`
## 键盘输入
```go
func main() {
	var (
		x int
		y float64
	)
	fmt.Println("输入两个数")
	fmt.Scanln(&x, &y)
	fmt.Println("x:", x)
	fmt.Println("y:", y)
}
```
## 流程控制
### 分支语句
- if
```go
func main() {
	var a = 15
	if a > 20 {
		fmt.Println("a > 20")
	} else if a > 10 {
		fmt.Println("a > 10")
	} else {
		fmt.Println("else")
	}
}
```
- if 可以包含一个初始化语句（如：给一个变量赋值）。这种写法具有固定的格式（在初始化语句后方必须加上分号）
```go
if initialization; condition {
    // do something
}
```
```go
// val变量的作用域只存在于 if 结构中
if val := 10; val > max {
    // do something
}
```
- switch
```go
func main() {
	var score = "B"
	switch score {
	case "A":
		fmt.Println("A")
	case "B":
		fmt.Println("B")
        // 穿透到下一个case
		fallthrough
	case "C":
		fmt.Println("C")
        // 终止
		break
	case "D", "E", "F":
		fmt.Println("DEF")
	default:
		fmt.Println("else")
	}
}
```
### 循环语句
- for循环
```go
func main() {
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
}
```
- for range循环,遍历数组、切片
```go
func main() {
	str := "hello world"
    // i:下标  v:对应的值
	for i, v := range str {
		fmt.Print(i)
		fmt.Printf("%c ", v) // 0h 1e 2l 3l 4o 5  6w 7o 8r 9l 10d 
	}
}
```
- 无限循环
```go
for true  {
    fmt.Printf("这是无限循环\n");
}
```
- continue, break 同java
## 函数
### 声明和使用
```go
func main() {
	fmt.Println(add(1, 2))
	fmt.Println(swap("jiang", "wei"))
	fmt.Println(sum(1, 2, 3, 4))
}

// func 函数名(参数, 参数...) (返回值, 返回值...) {
// 		函数体
// }
func add(a, b int) int {
	return a + b
}
// 多返回值
func swap(a, b string) (string, string) {
	return b, a
}
// 可变参数
func sum(nums ...int) int {
	sum := 0
	for i := 0; i < len(nums); i++ {
		sum += nums[i]
	}
	return sum
}
```
### 值传递
- 值传递：传递的是数据的副本, 修改数据对原始数据没有影响
- 值类型数据包括：基础类型、array、struct
```go
func main() {
	arr := [5]int{1, 2, 4, 5, 6}
	updateArr(arr)
	fmt.Println(arr)
}

func updateArr(arr [5]int){
	fmt.Println("接收的arr", arr)
	arr[0] = 0
	fmt.Println("修改后的arr", arr)
}
// 接收的arr [1 2 4 5 6]
// 修改后的arr [0 2 4 5 6]
// [1 2 4 5 6]
```
### 引用类型传递
- 引用类型传递：传递的地址, 修改数据对原始数据有影响
- 引用类型数据包括：slice, map, chan...
```go
func main() {
	arr := []int{1, 2, 4, 5, 6}
	updateArr(arr)
	fmt.Println(arr)
}

func updateArr(arr []int){
	fmt.Println("接收的arr", arr)
	arr[0] = 0
	fmt.Println("修改后的arr", arr)
}
// 接收的arr [1 2 4 5 6]
// 修改后的arr [0 2 4 5 6]
// [0 2 4 5 6]
```
### defer
- 函数中可以添加多个defer语句,当函数执行到最后时,defer语句按逆序执行
```go
func main() {
	f("1")
	fmt.Println("2")
	defer f("3")
	fmt.Println("4")
}

func f(s string)  {
	fmt.Println(s)
}
// 4 2 1 3
```
- defer时参数已经传入 只是最后执行
```go
func main() {
	a := 10
	fmt.Println("a1:", a)
	defer f(a)
	a++
	fmt.Println("a2:", a)
}

func f(i int)  {
	fmt.Println(i)
}
// a1: 10
// a2: 11
// 10
```
### 匿名函数
```go
func main() {
	f1()
	f2 := f1
	f2() // 函数也是变量
	f3 := func() {
		fmt.Println("f3f3f3")
	}
	f3()
	// 匿名函数
	func(a, b int) {
		fmt.Println(1 + 2)
	}(1, 2)
    //f1f1f1
	//f1f1f1
	//f3f3f3
	//3
}

func f1()  {
	fmt.Println("f1f1f1")
}
```
### 回调函数
```go
func main() {
	s := option(2, 3, add)
	fmt.Println(s)
}

func option(a int, b int, f func(int, int) int) string {
	i := f(a, b)
	return "结果为" + strconv.Itoa(i)
}

func add(a, b int) int {
	return a + b
}
```
### 闭包
```go
func main() {
	f1 := f()
	fmt.Println(f1())
	fmt.Println(f1())
	fmt.Println(f1())
	f2 := f()
	fmt.Println(f2())
	fmt.Println(f1())
	fmt.Println(f2())
}

func f() func() int {
	i := 0
	fun := func() int {
		i ++
		return i
	}
	return fun
}
```
## 指针
- 内存地址
```go
i := 10
// i 的指针
var intP = &i
fmt.Println(intP)
// 指针的值
var b = *intP
fmt.Println(b)
```
- 通过对 *p 赋另一个值来更改“对象”,这样 s 也会随之更改
```go
s := "good bye"
var p = &s
*p = "jiang"
fmt.Printf("Here is the pointer p: %p\n", p) // prints address
fmt.Printf("Here is the string *p: %s\n", *p) // prints string
fmt.Printf("Here is the string s: %s\n", s) // prints same string
//Here is the pointer p: 0xc0000441f0
//Here is the string *p: jiang
//Here is the string s: jiang
```
## 切片
### 声明
```go
// 不需要说明长度,一个切片在未初始化之前默认为 nil,长度为 0
var identifier []type
```
### 初始化
```go
arr := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
// var slice []type = arr[start:end]
// 表示 slice1 是由数组 arr 从 start 索引到 end-1 索引之间的元素构成的子集（切分数组，start:end 被称为 slice 表达式）。所以 slice[0] 就等于 arr1[start]。这可以在 arr 被填充前就定义好
// var slice1 []type = arr1[:] 那么 slice1 就等于完整的 arr1 数组, 另外一种表述方式是：slice1 = &arr1
var slice = arr[2:5]
```
### cap()
```go
// 切片提供了计算容量的函数 cap() 可以测量切片最长可以达到多少：它等于切片的长度 + 数组除切片之外的长度。如果 s 是一个切片，cap(s) 就是从 s[0] 到数组末尾的数组长度。切片的长度永远不会超过它的容量，所以对于 切片 s 来说该不等式永远成立：0 <= len(s) <= cap(s)
arr := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
var slice = arr[2:5]
for i, i2 := range slice {
    fmt.Printf("index: %d, value: %d\n", i, i2)
}
fmt.Println(cap(slice))
//index: 0, value: 2
//index: 1, value: 3
//index: 2, value: 4
//8
```
### 用 make() 创建一个切片\
当相关数组还没有定义时，我们可以使用 make() 函数来创建一个切片 同时创建好相关数组：`var slice1 []type = make([]type, len)`。\
也可以简写为 `slice1 := make([]type, len)`，这里 len 是数组的长度并且也是 slice 的初始长度。\
所以定义 `s2 := make([]int, 10)`，那么 `cap(s2) == len(s2) == 10`。\
make 接受 2 个参数：元素的类型以及切片的元素个数。\
如果你想创建一个 slice1，它不占用整个数组，而只是占用以 len 为个数个项，那么只要：`slice1 := make([]type, len, cap)`。\
make 的使用方式是：`func make([]T, len, cap)`，其中 cap 是可选参数。\
所以下面两种方法可以生成相同的切片:
```go
s1 := make([]int, 50, 100)
s2 := new([100]int)[0:50]
for i, i2 := range s1 {
    fmt.Printf("index: %d, value: %d\n", i, i2)
}
for i, i2 := range s2 {
    fmt.Printf("index2: %d, value: %d\n", i, i2)
}
fmt.Println(cap(s1))
fmt.Println(cap(s2))
```
### 切片重组（reslice）
- 改变切片长度的过程称之为切片重组 reslicing，做法如下：slice1 = slice1[0:end]，其中 end 是新的末尾索引（即长度）。
- 切片可以反复扩展直到占据整个相关数组。
```go
slice1 := make([]int, 0, 5)
for i := 0; i < cap(slice1); i++ {
    slice1 = slice1[0:i+1]
    slice1[i] = i
    fmt.Printf("The length of slice is %d\n", len(slice1))
}
// print the slice:
for i := 0; i < len(slice1); i++ {
    fmt.Printf("Slice at %d is %d\n", i, slice1[i])
}
//The length of slice is 1
//The length of slice is 2
//The length of slice is 3
//The length of slice is 4
//The length of slice is 5
//Slice at 0 is 0
//Slice at 1 is 1
//Slice at 2 is 2
//Slice at 3 is 3
//Slice at 4 is 4
```
### 切片的复制与追加
- 如果想增加切片的容量，我们必须创建一个新的更大的切片并把原分片的内容都拷贝过来。下面的代码描述了从拷贝切片的 copy 函数和向切片追加新元素的 append 函数。
```go
func main() {
    // 复制
	slFrom := []int{1, 2, 3}
	slTo := make([]int, 10)
	n := copy(slTo, slFrom)
	fmt.Println(slTo)
	fmt.Printf("Copied %d elements\n", n) // n == 3
    // 追加
	sl3 := []int{1, 2, 3}
	sl3 = append(sl3, 4, 5, 6)
	fmt.Println(sl3)
    // 如果你想将切片 y 追加到切片 x 后面，只要将第二个参数扩展成一个列表即可：x = append(x, y...)
	sl4 := []int{1, 2, 3}
	sl5 := []int{7, 8, 9}
	sl4 = append(sl4, sl5...)
	fmt.Println(sl4)
    //[1 2 3 0 0 0 0 0 0 0]
	//Copied 3 elements
	//[1 2 3 4 5 6]
    //[1 2 3 7 8 9]
}
```
## Map
### 声明、初始化和make
- map 是引用类型，可以使用如下声明
```go
var map1 map[keytype]valuetype
var map1 map[string]int
```
- 在声明的时候不需要知道 map 的长度，map 是可以动态增长的。
- 未初始化的 map 的值是 nil
- key 可以是任意可以用 == 或者 != 操作符比较的类型，比如 string、int、float。所以数组、切片和结构体不能作为 key，但是指针和接口类型可以。如果要用结构体作为 key 可以提供 Key() 和 Hash() 方法，这样可以通过结构体的域计算出唯一的数字或者字符串的 key。
- value 可以是任意类型的；通过使用空接口类型，我们可以存储任意值，但是使用这种类型作为值时需要先做一次类型断言
- map 也可以用函数作为自己的值，这样就可以用来做分支结构：key 用来选择要执行的函数
- 如果 key1 是 map1 的key，那么 `map1[key1]` 就是对应 key1 的值，就如同数组索引符号一样（数组可以视为一种简单形式的 map，key 是从 0 开始的整数）。
- 常用的 len(map1) 方法可以获得 map 中的 pair 数目，这个数目是可以伸缩的，因为 map-pairs 在运行时可以动态添加和删除。
- 声明、复制、取值
```go
var mapLit map[string]int
var mapAssigned map[string]int

mapLit = map[string]int{"one": 1, "two": 2}
mapCreated := make(map[string]float32)
mapAssigned = mapLit
fmt.Println(mapLit["two"])

mapCreated["key1"] = 4.5
mapCreated["key2"] = 3.14159
mapAssigned["two"] = 3

fmt.Println(mapLit["two"])
fmt.Println(mapLit)
fmt.Println(mapCreated)
fmt.Println(mapAssigned)

//2
//3
//map[one:1 two:3]
//map[key1:4.5 key2:3.14159]
//map[one:1 two:3]
```
- map中存方法
```go
mf := map[int]func() int{
    1: func() int { return 10 },
    2: func() int { return 20 },
    5: func() int { return 50 },
}
fmt.Println(mf)
fmt.Println(mf[2]()) // 20
```
- map中存切片
```go
mp2 := make(map[int]*[]int)
mp2[11] = &[]int{1, 2}
mp2[22] = &[]int{4, 5, 6}
fmt.Println(mp2)
fmt.Println(mp2[11])
fmt.Println(*mp2[11])
for i, j := range mp2 {
    fmt.Printf("key:%d, value:%p\n", i, *j)
    fmt.Println(*j)
}
//map[11:0xc000004480 22:0xc0000044a0]
//&[1 2]
//[1 2]
//key:22, value:0xc000012360
//[4 5 6]
//key:11, value:0xc00000a0b0
//[1 2]
```
### map 容量
- 和数组不同，map 可以根据新增的 key-value 对动态的伸缩，因此它不存在固定长度或者最大限制。但是你也可以选择标明 map 的初始容量 capacity
```go
map2 := make(map[string]float32, 100)
```
- 当 map 增长到容量上限的时候，如果再增加新的 key-value 对，map 的大小会自动加 1。所以出于性能的考虑，对于大的 map 或者会快速扩张的 map，即使只是大概知道容量，也最好先标明
### 键值对是否存在
- 使用 `val1 = map1[key1]` 的方法获取 key1 对应的值 val1。如果 map 中不存在 key1，val1 就是一个值类型的空值。这就会给我们带来困惑了：现在我们没法区分到底是 key1 不存在还是它对应的 value 就是空值。
```go
val1, isPresent = map1[key1]
```
- isPresent 返回一个 bool 值：如果 key1 存在于 map1，val1 就是 key1 对应的 value 值，并且 isPresent为true；如果 key1 不存在，val1 就是一个空值，并且 isPresent 会返回 false。
- 如果你只是想判断某个 key 是否存在而不关心它对应的值到底是多少，你可以这么做：
```go
_, ok := map1[key1] // 如果key1存在则ok == true，否则ok为false
```
```go
if _, ok := map1[key1]; ok {
    // ...
}
```
### 删除元素
- 从 map1 中删除 key1
```go
delete(map1, key1)
```
- 如果 key1 不存在，该操作不会产生错误。
### 遍历map
```go
// 遍历key和value
for key, value := range map1 {
    ...
}
// 只要value
for _, value := range map1 {
    ...
}
// 只要key
for key := range map1 {
    fmt.Printf("key is: %d\n", key)
}
```
- map 类型的切片
```go
// Version A:
items := make([]map[int]int, 5)
for i:= range items {
    items[i] = make(map[int]int, 1)
    items[i][1] = 2
}
fmt.Printf("Version A: Value of items: %v\n", items)
// Version B: NOT GOOD!
items2 := make([]map[int]int, 5)
for _, item := range items2 {
    item = make(map[int]int, 1) // item is only a copy of the slice element.
    item[1] = 2 // This 'item' will be lost on the next iteration.
}
fmt.Printf("Version B: Value of items: %v\n", items2)
//    Version A: Value of items: [map[1:2] map[1:2] map[1:2] map[1:2] map[1:2]]
//    Version B: Value of items: [map[] map[] map[] map[] map[]]
```
- 需要注意的是，应当像 A 版本那样通过索引使用切片的 map 元素。在 B 版本中获得的项只是 map 值的一个拷贝而已，所以真正的 map 元素没有得到初始化。
### map 的排序
- 如果你想为 map 排序，需要将 key（或者 value）拷贝到一个切片，再对切片排序
```go
func main() {
	var (
		barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
			"delta": 87, "echo": 56, "foxtrot": 12,
			"golf": 34, "hotel": 16, "indio": 87,
			"juliet": 65, "kali": 43, "lima": 98}
	)
	fmt.Println("unsorted:")
	for k, v := range barVal {
		fmt.Printf("Key: %v, Value: %v / ", k, v)
	}
	keys := make([]string, len(barVal))
	i := 0
	for k, _ := range barVal {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	fmt.Println()
	fmt.Println("sorted:")
	for _, k := range keys {
		fmt.Printf("Key: %v, Value: %v / ", k, barVal[k])
	}
}
```
### 将 map 的键值对调
- 调换 key 和 value。如果 map 的值类型可以作为 key 且所有的 value 是唯一的，那么通过下面的方法可以简单的做到键值对调。
```go
func main() {
	var (
		barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
			"delta": 87, "echo": 56, "foxtrot": 12,
			"golf": 34, "hotel": 16, "indio": 87,
			"juliet": 65, "kali": 43, "lima": 98}
	)

	invMap := make(map[int]string, len(barVal))
	for k, v := range barVal {
		invMap[v] = k
	}
	fmt.Println("inverted:")
	for k, v := range invMap {
		fmt.Printf("Key: %v, Value: %v\n", k, v)
	}
}
```
# 应用
## 字符串
- int 转 string
```go
var a int = 10
str := strconv.Itoa(a)
fmt.Println(str)
```
- string 转 int
```go
var a string = "10"
i, _ := strconv.Atoi(a)
fmt.Println(i)
```
- float64 转 string
```go
var f float64 = 10.25
// strconv.FormatFloat(f float64, fmt byte, prec int, bitSize int) string 
// 其中 fmt 表示格式（其值可以是 'b'、'e'、'f' 或 'g'）
// prec 表示精度, 不四舍五入
str := strconv.FormatFloat(f, 'f', 2, strconv.IntSize)
fmt.Println(str)
```
- string 转 float64
```go
var str string = "10.25"
i, _ := strconv.ParseFloat(str, strconv.IntSize)
fmt.Println(i)
```
- string 转 slice
```go
c := []byte(str)
```
- slice 转 string
```go
str := string(c)
```
- 字符串的长度
```go
s := "\u00ff\u754c"
fmt.Println(len(s))
fmt.Println(utf8.RuneCountInString(s))
```
- 判断字符串 s 是否以 prefix 开头
```go
s := "hello world"
prefix := "he"
b := strings.HasPrefix(s, prefix)
fmt.Println(b)
// true
```
- 判断字符串 s 是否以 suffix 结尾
```go
s := "hello world"
suffix := "rld"
b := strings.HasSuffix(s, suffix)
fmt.Println(b)
// true
```
- 判断字符串 s 是否包含 substr
```go
s := "hello world"
substr := "llo"
b := strings.Contains(s, substr)
fmt.Println(b)
// true
```
- 字符串 str 在字符串 s 中的索引（str 的第一个字符的索引）,-1 表示字符串 s 不包含字符串 str
```go
s := "hello world"
str := "o"
i := strings.Index(s, str)
fmt.Println(i)
// 5
```
- 字符串 str 在字符串 s 中最后出现位置的索引（str 的第一个字符的索引）,-1 表示字符串 s 不包含字符串 str
```go
s := "hello world"
str := "o"
i := strings.LastIndex(s, str)
fmt.Println(i)
// 7
```
- 字符串 str 中的前 n 个字符串 old 替换为字符串 new,如果 n = -1 则替换所有字符串 old 为字符串 new
```go
s := "hello world"
old := "o"
new := "x"
res := strings.Replace(s, old, new, -1)
fmt.Println(res)
// hellx wxrld
```
- 计算字符串 str 在字符串 s 中出现的非重叠次数
```go
s := "hello world"
str := "l"
res := strings.Count(s, str)
fmt.Println(res)
// 3
```
- 用于重复 count 次字符串 s
```go
s := "hello world"
res := strings.Repeat(s, 2)
fmt.Println(res)
// hello worldhello world
```
- 修改字符串大小写
```go
s := "hello world"
// 小写
res1 := strings.ToLower(s)
fmt.Println(res1)
// 大写
res2 := strings.ToUpper(s)
fmt.Println(res2)
```
- 修剪字符串
```go
s := "hello world"
res1 := strings.TrimSpace(s)
res2 := strings.Trim(s, "hd")
res3 := strings.TrimLeft(s, "hd")
res4 := strings.TrimRight(s, "hd")
fmt.Println(res1)
fmt.Println(res2)
fmt.Println(res3)
fmt.Println(res4)
//hello world
//ello worl
//ello world
//hello worl
```
- 分割字符串
```go
// 利用 1 个或多个空白符号来作为动态长度的分隔符将字符串分割成若干小块,并返回一个 slice,如果字符串只包含空白符号,则返回一个长度为 0 的 slice
s1 := "hello world"
res1 := strings.Fields(s1)
fmt.Println(res1)
fmt.Println(len(res1))
// 用于自定义分割符号来对指定字符串进行分割
s2 := "h,e,l,l,o"
res2 := strings.Split(s2, ",")
fmt.Println(res2)
fmt.Println(len(res2))
//[hello world]
//2
//[h e l l o]
//5
```
- 拼接 slice 到字符串
```go
sl := []string{"hello", "world"}
res := strings.Join(sl, "-")
fmt.Println(res)
// hello-world
```
## 切片
- 从字符串生成字节切片
```go
// 假设 s 是一个字符串（本质上是一个字节数组），那么就可以直接通过 c := []byte(s) 来获取一个字节的切片 c。另外，您还可以通过 copy 函数来达到相同的目的：copy(dst []byte, src string), 还可以使用 for-range 来获得每个元素
s := "hello"
for i, c := range s {
    fmt.Printf("%d:%c ", i, c)
}
// 将一个字符串追加到某一个字节切片的尾部
var b []byte
var s string
b = append(b, s...)
```
- 获取字符串的某一部分
```go
// 使用 substr := str[start:end] 可以从字符串 str 获取到从索引 start 开始到 end-1 位置的子字符串。同样的，str[start:] 则表示获取从 start 开始到 len(str)-1 位置的子字符串。而 str[:end] 表示获取从 0 开始到 end-1 的子字符串。
str := "hello world"
substr := str[3:8]
fmt.Println(substr)
```
- 判断切片是否已经被升序排序
```go
sl := []int{1, 2, 3, 4}
b := sort.IntsAreSorted(sl)
fmt.Println(b) // true
```
- 将切片升序排序
```go
sl := []int{1, 3, 2, 5, 4}
sort.Ints(sl)
fmt.Println(sl)
```
- 搜索
```go
// 想要在数组或切片中搜索一个元素，该数组或切片必须先被排序（因为标准库的搜索算法使用的是二分法）。然后，您就可以使用函数 func SearchInts(a []int, n int) int 进行搜索，并返回对应结果的索引值
sl := []int{1, 3, 2, 5, 4}
sort.Ints(sl)
index := sort.SearchInts(sl, 3)
fmt.Println(index)// 2
```
- append 函数常见操作
1. 将切片 b 的元素追加到切片 a 之后：a = append(a, b...)
2. 复制切片 a 的元素到新的切片 b 上：
```go
b = make([]T, len(a))
copy(b, a)
```
3. 删除位于索引 i 的元素：`a = append(a[:i], a[i+1:]...)`
4. 切除切片 a 中从索引 i 至 j 位置的元素：`a = append(a[:i], a[j:]...)`
5. 为切片 a 扩展 j 个元素长度：`a = append(a, make([]T, j)...)`
6. 在索引 i 的位置插入元素 x：`a = append(a[:i], append([]T{x}, a[i:]...)...)`
7. 在索引 i 的位置插入长度为 j 的新切片：`a = append(a[:i], append(make([]T, j), a[i:]...)...)`
8. 在索引 i 的位置插入切片 b 的所有元素：`a = append(a[:i], append(b, a[i:]...)...)`
9. 取出位于切片 a 最末尾的元素 x：`x, a = a[len(a)-1], a[:len(a)-1]`
10. 将元素 x 追加到切片 `a：a = append(a, x)`
## 日期
```go
t := time.Now()
fmt.Println(t) // 2023-04-23 16:18:54.5414987 +0800 CST m=+0.003999601
// 格式化
fmt.Printf("%02d/%02d/%02d\n", t.Year(), t.Month(), t.Day()) // 2023/04/23
t = time.Now().UTC()
fmt.Println(t) // 2023-04-23 08:18:54.5615332 +0000 UTC
// calculating times:
week := 60 * 60 * 24 * 7 * 1e9 // must be in nanosec
weekFromNow := t.Add(time.Duration(week))
fmt.Println(weekFromNow) // 2023-04-30 08:18:54.5615332 +0000 UTC
// formatting times:
fmt.Println(t.Format(time.RFC822)) // 23 Apr 23 08:18 UTC
fmt.Println(t.Format(time.ANSIC)) // Sun Apr 23 08:18:54 2023
// The time must be 2006-01-02 15:04:05
fmt.Println(t.Format("02 Jan 2006 15:04:05")) // 23 Apr 2023 08:23:34
fmt.Println(t.Format("2006-01-02 15:04:05")) // 2023-04-23 08:23:34
s := t.Format("20060102")
fmt.Println(t, "=>", s) // 2023-04-23 08:21:34.1882633 +0000 UTC => 20230423
```
# 结构
## 结构体定义
### 定义
```go
type struct1 struct {
	i   int
	f   float32
	str string
}

func main() {
	var s1 = new(struct1)
	s1.i = 10
	s1.f = 15.5
	s1.str = "Chris"
	var s2 *struct1
	s2 = new(struct1)
	s3 := &struct1{10, 15.5, "Chris"}
	s4 := struct1{10, 15.5, "Chris"}
	s5 := struct1{i:10, str:"string"}

	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s3)
	fmt.Println(s4)
	fmt.Println(s5)
	//&{10 15.5 Chris}
	//&{0 0 }
	//&{10 15.5 Chris}
	//{10 15.5 Chris}
	//{10 0 string}
}
```
```go
type Person struct {
	firstName   string
	lastName    string
}
func upPerson(p *Person) {
	p.firstName = strings.ToUpper(p.firstName)
	p.lastName = strings.ToUpper(p.lastName)
}
func main() {
	// 1-struct as a value type:
	var pers1 Person
	pers1.firstName = "Chris"
	pers1.lastName = "Woodward"
	upPerson(&pers1)
	fmt.Printf("The name of the person is %s %s\n", pers1.firstName, pers1.lastName)
	// 2—struct as a pointer:
	pers2 := new(Person)
	pers2.firstName = "Chris"
	(*pers2).lastName = "Woodward"  // 这是合法的
	upPerson(pers2)
	fmt.Printf("The name of the person is %s %s\n", pers2.firstName, pers2.lastName)
	// 3—struct as a literal:
	pers3 := &Person{"Chris","Woodward"}
	upPerson(pers3)
	fmt.Printf("The name of the person is %s %s\n", pers3.firstName, pers3.lastName)
}
```
### 二叉树
```go
type Tree struct {
    le      *Tree
    data    float64
    ri      *Tree
}
```
## 使用工厂方法创建结构体实例
### 结构体工厂
- Go 语言不支持面向对象编程语言中那样的构造子方法，但是可以很容易的在 Go 中实现 “构造子工厂”方法。为了方便通常会为类型定义一个工厂，按惯例，工厂的名字以 new 或 New 开头。
```go
type File struct {
	fd      int     // 文件描述符
	name    string  // 文件名
}

func NewFile(fd int, name string) *File {
	if fd < 0 {
		return nil
	}
	return &File{fd, name}
}

func main() {
	f := NewFile(10, "./test.txt")
	fmt.Println(*f)
	size := unsafe.Sizeof(f)
	fmt.Println(size)
}
```
### 匿名字段和内嵌结构体
- 结构体可以包含一个或多个 匿名（或内嵌）字段，即这些字段没有显式的名字，只有字段的类型是必须的，此时类型就是字段的名字。匿名字段本身可以是一个结构体类型，即 结构体可以包含内嵌结构体。
- 可以粗略地将这个和面向对象语言中的继承概念相比较，随后将会看到它被用来模拟类似继承的行为。Go 语言中的继承是通过内嵌或组合来实现的，所以可以说，在 Go 语言中，相比较于继承，组合更受青睐。
```go
type innerS struct {
	in1 int
	in2 int
}
type outerS struct {
	b    int
	c    float32
	int  // anonymous field
	innerS //anonymous field
}
func main() {
	outer := new(outerS)
	outer.b = 6
	outer.c = 7.5
	outer.int = 60
	outer.in1 = 5
	outer.in2 = 10
	fmt.Println("outer1 is:", *outer)
	// 使用结构体字面量
	outer2 := outerS{6, 7.5, 60, innerS{5, 10}}
	fmt.Println("outer2 is:", outer2)
}
//outer1 is: {6 7.5 60 {5 10}}
//outer2 is: {6 7.5 60 {5 10}}
```
### 方法
#### 使用
- 在方法名之前，`func` 关键字之后的括号中指定 receiver。
- 如果 `recv` 是 receiver 的实例，Method1 是它的方法名，那么方法调用遵循传统的 object.name 选择器符号：recv.Method1()。
- 如果 recv 是一个指针，Go 会自动解引用。
```go
func (recv receiver_type) methodName(parameter_list) (return_value_list) { ... }
```
- `recv` 就像是面向对象语言中的 `this` 或 `self`，但是 Go 中并没有这两个关键字。
```go
type TwoInts struct {
	a int
	b int
}
func (tn *TwoInts) AddThem() int {
	return tn.a + tn.b
}
func (tn *TwoInts) AddToParam(param int) int {
	return tn.a + tn.b + param
}
func main() {
	two1 := new(TwoInts)
	two1.a = 12
	two1.b = 10
	fmt.Printf("The sum is: %d\n", two1.AddThem())
	fmt.Printf("Add them to the param: %d\n", two1.AddToParam(20))
	two2 := TwoInts{3, 4}
	fmt.Printf("The sum is: %d\n", two2.AddThem())
	// The sum is: 22
	//Add them to the param: 42
	//The sum is: 7
}
```
- 下面是非结构体类型上方法的例子：
```go
type IntVector []int
func (v IntVector) Sum() (s int) {
	for _, x := range v {
		s += x
	}
	return
}
func main() {
	fmt.Println(IntVector{1, 2, 3}.Sum()) // 输出是6
}
//6
```
#### 内嵌类型的方法和继承
```go
type Point struct {
	x, y float64
}
func (p *Point) Abs() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)
}
func (n *NamedPoint) Abs() float64 {
	return n.Point.Abs() * 100.
}
type NamedPoint struct {
	Point
	name string
}
func main() {
	n := &NamedPoint{Point{3, 4}, "Pythagoras"}
	fmt.Println(n.Abs())
}
//500
```
#### 如何在类型中嵌入功能
- 聚合（或组合）：包含一个所需功能类型的具名字段。
```go
type Log struct {
	msg string
}
type Customer struct {
	Name string
	log  *Log
}
func main() {
	// shorter
	c = &Customer{"Barak Obama", &Log{"1 - Yes we can!"}}
	// fmt.Println(c) &{Barak Obama 1 - Yes we can!}
	c.Log().Add("2 - After me the world will be a better place!")
	//fmt.Println(c.log)
	fmt.Println(c.Log())
}
func (l *Log) Add(s string) {
	l.msg += "\n" + s
}
func (l *Log) String() string {
	return l.msg
}
func (c *Customer) Log() *Log {
	return c.log
}
```
- 内嵌：内嵌（匿名地）所需功能类型。
```go
type Log struct {
    msg string
}
type Customer struct {
    Name string
    Log
}
func main() {
    c := &Customer{"Barak Obama", Log{"1 - Yes we can!"}}
    c.Add("2 - After me the world will be a better place!")
    fmt.Println(c)
}
func (l *Log) Add(s string) {
    l.msg += "\n" + s
}
func (l *Log) String() string {
    return l.msg
}
func (c *Customer) String() string {
    return c.Name + "\nLog:" + fmt.Sprintln(c.Log.String())
}
```
#### 多重继承
```go
type Camera struct{}
func (c *Camera) TakeAPicture() string {
	return "Click"
}
type Phone struct{}
func (p *Phone) Call() string {
	return "Ring Ring"
}
type CameraPhone struct {
	Camera
	Phone
}
func main() {
	cp := new(CameraPhone)
	fmt.Println("It exhibits behavior of a Camera: ", cp.TakeAPicture())
	fmt.Println("It works like a Phone too: ", cp.Call())
}
//It exhibits behavior of a Camera:  Click
//It works like a Phone too:  Ring Ring
```
#### 类型的 String() 方法和格式化描述符
- 如果类型定义了 String() 方法，它会被用在 fmt.Printf() 中生成默认的输出：等同于使用格式化描述符 %v 产生的输出。还有 fmt.Print() 和 fmt.Println() 也会自动使用 String() 方法。
```go
type TwoInts struct {
	a int
	b int
}
func main() {
	two1 := new(TwoInts)
	two1.a = 12
	two1.b = 10
	fmt.Printf("two1 is: %v\n", two1)
	fmt.Println("two1 is:", two1)
	fmt.Print("two1 is: ", two1, "\n")
	fmt.Printf("two1 is: %T\n", two1)
	fmt.Printf("two1 is: %#v\n", two1)
	//two1 is: (12/10)
	//two1 is: (12/10)
	//two1 is: (12/10)
	//two1 is: *main.TwoInts
	//two1 is: &main.TwoInts{a:12, b:10}
}
func (tn *TwoInts) String() string {
	return "(" + strconv.Itoa(tn.a) + "/" + strconv.Itoa(tn.b) + ")"
}
```
# 包
## regexp 包
- 正则表达式
```go
func main() {
	//目标字符串
	searchIn := "John: 2578.34 William: 4567.23 Steve: 5632.18"
	pat := "[0-9]+.[0-9]+" //正则
	f := func(s string) string{
		v, _ := strconv.ParseFloat(s, 32)
		return strconv.FormatFloat(v * 2, 'f', 2, strconv.IntSize)
	}
	if ok, _ := regexp.Match(pat, []byte(searchIn)); ok {
		fmt.Println("Match Found!")
	}
	re, _ := regexp.Compile(pat)
	//将匹配到的部分替换为"##.#"
	str := re.ReplaceAllString(searchIn, "##.#")
	fmt.Println(str)
	//参数为函数时
	str2 := re.ReplaceAllStringFunc(searchIn, f)
	fmt.Println(str2)
    //Match Found!
	//John: ##.# William: ##.# Steve: ##.#
	//John: 5156.68 William: 9134.46 Steve: 11264.36
}
```
## 锁和 sync 包
- 假设 info 是一个需要上锁的放在共享内存中的变量。通过包含 `Mutex` 来实现的一个典型例子如下
```go
type Info struct {
	mu   sync.Mutex
	name string
}
```
- 如果一个函数想要改变这个变量
```go
func Update(info *Info) {
	info.mu.Lock()
	// critical section:
	info.name = "aaa"
	// end critical section
	info.mu.Unlock()
}
```
- 还有一个很有用的例子是通过 Mutex 来实现一个可以上锁的共享缓冲器
```go
type SyncedBuffer struct {
    lock     sync.Mutex
    buffer  bytes.Buffer
}
```
## 精密计算和 big 包
```go
func main() {
	// Here are some calculations with bigInts:
	im := big.NewInt(math.MaxInt64)
	in := im
	io := big.NewInt(1956)
	ip := big.NewInt(1)
	ip.Mul(im, in).Add(ip, im).Div(ip, io)
	fmt.Printf("Big Int: %v\n", ip)
	// Here are some calculations with bigInts:
	rm := big.NewRat(math.MaxInt64, 1956)
	rn := big.NewRat(-1956, math.MaxInt64)
	ro := big.NewRat(19, 56)
	rp := big.NewRat(1111, 2222)
	rq := big.NewRat(1, 1)
	rq.Mul(rm, rn).Add(rq, ro).Mul(rq, rp)
	fmt.Printf("Big Rat: %v\n", rq)
}
//Big Int: 43492122561469640008497075573153004
//Big Rat: -37/112
```
# 接口
## 定义和实现
### 通过如下格式定义接口
```go
type Namer interface {
    Method1(param_list) return_type
    Method2(param_list) return_type
    ...
}
```
### 特性
- 类型不需要显式声明它实现了某个接口：接口被隐式地实现。多个类型可以实现同一个接口。
- 实现某个接口的类型（除了实现接口方法外）可以有其他的方法。
- 一个类型可以实现多个接口。
- 接口类型可以包含一个实例的引用， 该实例的类型实现了此接口（接口是动态类型）。
### 示例一
```go
// Shaper 接口
type Shaper interface {
    Area() float32
    // Perimeter() float32
}

// Square 方块实现类
type Square struct {
    side float32
}

func (sq *Square) Area() float32 {
    return sq.side * sq.side
}

// Rectangle 矩形实现类
type Rectangle struct {
    length, width float32
}

func (r Rectangle) Area() float32 {
    return r.length * r.width
}

func main() {
    var areaInt1 Shaper
    areaInt1 = &Square{5}
    fmt.Printf("The square has area: %f\n", areaInt1.Area())
    var areaInt2 Shaper
    areaInt2 = &Rectangle{5, 3}
    fmt.Printf("The rectangle has area: %f\n", areaInt2.Area())
}
//The square has area: 25.000000
//The rectangle has area: 15.000000
```
- 上面的程序定义了一个结构体 `Square` 和一个接口 `Shaper`，接口有一个方法 `Area()`。  
  在 `main()` 方法中创建了一个 `Square` 的实例。在主程序外边定义了一个接收者类型是 `Square` 方法的 `Area()`，用来计算正方形的面积：结构体 `Square` 实现了接口 `Shaper` 。

- 现在接口变量包含一个指向 `Square` 变量的引用，通过它可以调用 `Square` 上的方法 `Area()`。

- 这是 多态 的 Go 版本，多态是面向对象编程中一个广为人知的概念：根据当前的类型选择正确的方法，或者说：同一种类型在不同的实例上似乎表现出不同的行为。

- 如果 `Square` 没有实现 `Area()` 方法，编译器将会给出清晰的错误信息：
```go
cannot use sq1 (type *Square) as type Shaper in assignment:
*Square does not implement Shaper (missing Area method)
```
- 如果 `Shaper` 有另外一个方法 `Perimeter()`，但是 `Square` 没有实现它，即使没有人在 `Square` 实例上调用这个方法，编译器也会给出上面同样的错误。
### 示例二
- 有两个类型 `stockPosition` 和 `car`，它们都有一个 `getValue()` 方法，我们可以定义一个具有此方法的接口 `valuable`。接着定义一个使用 `valuable` 类型作为参数的函数 `showValue()`，所有实现了 `valuable` 接口的类型都可以用这个函数。
```go
type stockPosition struct {
    ticker     string
    sharePrice float32
    count      float32
}
/* method to determine the value of a stock position */
func (s stockPosition) getValue() float32 {
    return s.sharePrice * s.count
}
type car struct {
    make  string
    model string
    price float32
}
/* method to determine the value of a car */
func (c car) getValue() float32 {
    return c.price
}
/* contract that defines different things that have value */
type valuable interface {
    getValue() float32
}
func showValue(asset valuable) {
    fmt.Printf("Value of the asset is %f\n", asset.getValue())
}
func main() {
    var s valuable = stockPosition{"GOOG", 577.20, 4}
    showValue(s)
    var c = car{"BMW", "M3", 66500}
    showValue(c)
}
//Value of the asset is 2308.800049
//Value of the asset is 66500.000000
```
## 接口嵌套接口
- 一个接口可以包含一个或多个其他的接口，这相当于直接将这些内嵌接口的方法列举在外层接口中一样。
- 比如接口 `File` 包含了 `ReadWrite` 和 `Lock` 的所有方法，它还额外有一个 `Close()` 方法。
```go
type ReadWrite interface {
    Read(b Buffer) bool
    Write(b Buffer) bool
}
type Lock interface {
    Lock()
    Unlock()
}
type File interface {
    ReadWrite
    Lock
    Close()
}
```
## 接口类型断言
### 类型断言
```go
v := varI.(T)       // unchecked type assertion
```
varI 必须是一个接口变量，否则编译器会报错
```go
invalid type assertion: varI.(T) (non-interface type (type of varI) on left)
```
### 示例一
```go
type Square struct {
	side float32
}
type Circle struct {
	radius float32
}
type Shaper interface {
	Area() float32
}
func main() {
	var areaIntf Shaper = &Square{5}
	// Is Square the type of areaIntf?
	if t, ok := areaIntf.(*Square); ok {
		fmt.Printf("The type of areaIntf is: %T\n", t)
		fmt.Printf("The area is: %f\n", t.Area())
	}

	if u, ok := areaIntf.(*Circle); ok {
		fmt.Printf("The type of areaIntf is: %T\n", u)
		fmt.Printf("The area is: %f\n", u.Area())
	}
}
func (sq *Square) Area() float32 {
	return sq.side * sq.side
}
func (ci *Circle) Area() float32 {
	return ci.radius * ci.radius * math.Pi
}
//The type of areaIntf is: *main.Square
//The area is: 25.000000
```
- 如果忽略 areaIntf.(*Square) 中的 * 号，会导致编译错误
```go
impossible type assertion: Square does not implement Shaper (Area method has pointer receiver)
```
### type-switch
- 接口变量的类型也可以使用一种特殊形式的 switch 来检测
```go
func main() {
	var areaIntf Shaper = &Square{5}

	switch t := areaIntf.(type) {
	case *Square:
		fmt.Printf("Type Square %T with value %v\n", t, t)
	case *Circle:
		fmt.Printf("Type Circle %T with value %v\n", t, t)
	case nil:
		fmt.Printf("nil value: nothing to check?\n")
	default:
		fmt.Printf("Unexpected type %T\n", t)
	}
}
// Type Square *main.Square with value &{5}
```
- 变量 `t` 得到了 areaIntf 的值和类型， 所有 `case` 语句中列举的类型（`nil` 除外）都必须实现对应的接口（在上例中即 Shaper），如果被检测类型没有在 `case` 语句列举的类型中，就会执行 `default` 语句。
- 可以用 `type-switch` 进行运行时类型分析，但是在 `type-switch` 不允许有 `fallthrough`
### 示例二：类型分类函数
- 它有一个可变长度参数，可以是任意类型的数组，它会根据数组元素的实际类型执行不同的动作
```go
func classifier(items ...interface{}) {
    for i, x := range items {
        switch x.(type) {
        case bool:
            fmt.Printf("Param #%d is a bool\n", i)
        case float64:
            fmt.Printf("Param #%d is a float64\n", i)
        case int, int64:
            fmt.Printf("Param #%d is a int\n", i)
        case nil:
            fmt.Printf("Param #%d is a nil\n", i)
        case string:
            fmt.Printf("Param #%d is a string\n", i)
        default:
            fmt.Printf("Param #%d is unknown\n", i)
        }
    }
}
func main() {
	classifier(13, -14.3, "BELGIUM", complex(1, 2), nil, false)
}
//Param #0 is a int
//Param #1 is a float64
//Param #2 is a string
//Param #3 is unknown
//Param #4 is a nil
//Param #5 is a bool
```
## 空接口
- 空接口或者最小接口 不包含任何方法，它对实现不做任何要求
```go
type Any interface {}
```
- 示例
```go
var i = 5
var str = "ABC"
type Person struct {
	name string
	age  int
}
type Any interface{}
func main() {
	var val Any
	val = 5
	fmt.Printf("val has the value: %v\n", val)
	val = str
	fmt.Printf("val has the value: %v\n", val)
	pers1 := new(Person)
	pers1.name = "Rob Pike"
	pers1.age = 55
	val = pers1
	fmt.Printf("val has the value: %v\n", val)
	switch t := val.(type) {
	case int:
		fmt.Printf("Type int %T\n", t)
	case string:
		fmt.Printf("Type string %T\n", t)
	case bool:
		fmt.Printf("Type boolean %T\n", t)
	case *Person:
		fmt.Printf("Type pointer to Person %T\n", t)
	default:
		fmt.Printf("Unexpected type %T", t)
	}
}
//val has the value: 5
//val has the value: ABC
//val has the value: &{Rob Pike 55}
//Type pointer to Person *main.Person
```