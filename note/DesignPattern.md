# 目录
- [单例模式](#单例模式)
- [策略模式](#策略模式)
- [动态代理](#动态代理)
# 单例模式
单例模式顾名思义就是任何适合都只能有一个实例，且该类需自行创建这个实例，并对其他的类提供调用这一实例的方法
## 饿汉模式
饿汉式，就是“比较饿”，实例在初始化的时候就要创建好，不管你有没有用到 
- 优点：线程安全；在类加载（ClassLoader）的同时已经创建好一个静态对象，调用时反应速度快 
- 缺点：对象提前创建,所以会占据一定的内存,内存占用大 以空间换时间 
```java
public class Singleton {

    /**
     * 私有实例，静态变量会在类加载的时候初始化，是线程安全的
     */
    private static final Singleton instance = new Singleton();

    /**
     * 私有构造方法
     */
    private Singleton() {
    }

    /**
     * 唯一公开获取实例的方法（静态工厂方法）
     *
     * @return
     */
    public static Singleton getInstance() {
        return instance;
    }
}
```
## 懒汉模式（线程不安全）
懒汉式就是“比较懒”，就是在用到的时候才去检查有没有实例，如果有则直接返回，没有则新建 
- 优点：起到了Lazy Loading的效果，但是只能在单线程下使用 
- 缺点：如果在多线程下，两个线程同时进入了if (singleton == null)判断语句块，这时便会产生多个实例 所以在多线程环境下不可使用这种方式 
```java
public class Singleton {

    /**
     * 私有实例
     */
    private static Singleton instance;

    /**
     * 私有构造方法
     */
    private Singleton() {
    }

    /**
     * 唯一公开获取实例的方法（静态工厂方法），
     *
     * @return
     */
    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
```
## 懒汉模式（线程安全）
对getInstance()方法添加synchronized关键字
- 优点：解决了线程不安全问题
- 缺点：每次调用都加锁同步执行，对象返回效率低，不推荐使用 
```java
public class Singleton {

    /**
     * 私有实例
     */
    private static Singleton instance;

    /**
     * 私有构造方法
     */
    private Singleton() {
    }

    /**
     * 唯一公开获取实例的方法（静态工厂方法），该方法使用synchronized加锁，来保证线程安全性
     *
     * @return
     */
    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
```
## 双重检索模式
进入getInstance方法时先不同步，进入方法过后，先检查实例是否存在，如果存在则直接返回，如果不存在才进入synchronized修饰的同步代码块，进入后，再次检查实例是否存在，如果不存在，就在同步的情况下创建一个实例 即保证了懒加载，又保证了高性能 推荐使用 

双重检查加锁机制的实现使用到一个关键字volatile：被volatile修饰的变量的值，保证了多个线程之间的可见性 即被一个线程修改后，其他线程立即可见 
```java
public class Singleton {

    /**
     * 私有实例 volatile
     */
    private volatile static Singleton instance;

    /**
     * 私有构造方法
     */
    private Singleton() {
    }

    /**
     * 唯一公开获取实例的方法（静态工厂方法）
     *
     * @return
     */
    public static Singleton getUniqueInstance() {
        //第一个 if 语句用来避免 uniqueInstance 已经被实例化之后的加锁操作
        if (instance == null) {
            // 加锁
            synchronized (Singleton.class) {
                //第二个 if 语句进行了加锁，所以只能有一个线程进入，就不会出现 instance == null 时两个线程同时进行实例化操作 
                if (instance == null) { 
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
```
## 静态内部类
该模式利用了静态内部类延迟初始化的特性，来达到与双重校验锁方式一样的功能 
```java
public class StaticInnerClassSingleton {

    private StaticInnerClassSingleton(){}


    /**
     * 唯一公开获取实例的方法（静态工厂方法）
     *
     * @return
     */
    public static StaticInnerClassSingleton getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * 私有静态内部类
     */
    private static class LazyHolder {
        private static final StaticInnerClassSingleton INSTANCE = new StaticInnerClassSingleton();
    }
}
```
# 策略模式
使用策略模式可以把行为和环境分割开来 环境类Context负责查询要做什么，各种算法则在具体策略类（ConcreteStrategy）中提供 
当出现新的促销折扣或现有的折扣政策出现变化时，只需要实现新的策略类，并在客户端登记即可 
 - 环境（Context）：有一个Strategy类的引用，和具体的策略类交互 
 - 抽象策略（Strategy）角色：一个接口或抽象类，给出规范 
 - 具体策略（ConcreteStrategy）角色：具体算法或行为
1. 创建接口
```java
package com.example.demo.service;

public interface IFruitService {
    String detail();
}
```
2. 实现策略
```java
package com.example.demo.service.impl;

import org.springframework.stereotype.Component;

@Component("APPLE")
public class AppleService implements IFruitService extends BaseService {
    @Override
    public String detail() {
        return prefix() + "apple";
    }
}
```
```java
package com.example.demo.service.impl;

import org.springframework.stereotype.Component;

@Component("ORANGE")
public class OrangeService implements IFruitService extends BaseService {
    @Override
    public String detail() {
        return prefix() + "orange";
    }
}
```
```java
package com.example.demo.service.impl;

import org.springframework.stereotype.Component;

public class BaseService {
    public String prefix() {
        return "the best fruit is ";
    }
}
```
3. 应用上下文
```java
package com.example.demo.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class FruitContext {
    @Autowired
    private Map<String, IFruitService> fruitServiceMap;

    public String getDetail(String type) {
        IFruitService fruitService = this.fruitServiceMap.get(type)
        if (fruitService != null){
            return fruitService.detail();
        } else {
            throw new RuntimeException("can not find type, type:" + type)
        }
    }
}
```
4. 使用
```java
@GetMapping("/getDetail")
public void getDetail(@Param("type") String type) { // type=APPLE 或 type=ORANGE
    this.fruitContext.getDetail(type);
}
```

# 动态代理
## JDK动态代理
只能对实现了接口的类生成代理，不能针对类  主要通过Proxy+InvocationHandler的组合来实现 
1. 创建接口和实现类
```java
package com.jiangwei.test.service;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/22 14:51
 */
public interface IUserService {

    void hello();

}
```
```java
package com.jiangwei.test.service.impl;

import com.jiangwei.test.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/22 14:53
 */

@Service
public class UserServiceImpl implements IUserService {
    @Override
    public void hello() {
        System.out.println("============hello=============");
    }
}
```
2. 创建代理类继承实现InvocationHandler
```java
package com.jiangwei.test.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/22 16:33
 */
public class MyInvocationHandler implements InvocationHandler {

    // 目标对象
    private final Object target;

    public MyInvocationHandler(Object target) {
        super();
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("==========before===========");
        // 执行目标方法
        Object res = method.invoke(target, args);
        System.out.println("==========after===========");
        return res;
    }

    // 获取目标对象的代理对象
    public Object getProxy() {
        return Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                target.getClass().getInterfaces(),
                this
        );
    }
}
```
3. 使用
```java
package com.jiangwei.test;

import com.jiangwei.test.handler.MyInvocationHandler;
import com.jiangwei.test.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/17 16:45
 */
@SpringBootTest(classes = AppServerApplication.class)
@RunWith(SpringRunner.class)
public class HandlerTest {

    @Resource
    private IUserService userService;

    @Test
    public void test() {
        MyInvocationHandler handler = new MyInvocationHandler(userService);
        // 代理对象
        IUserService proxy = (IUserService) handler.getProxy();
        // 通过代理对象执行对应函数
        proxy.hello();
    }

}
```
## Cglib代理
针对类进行代理 对指定的类生成一个子类，覆盖其中的方法 （注意对应的方法不要声明为final，否则无法重写）主要通过Enhancer + Callback来实现 
1. 自定义一个拦截器, 实现MethodInterceptor
```java
package com.jiangwei.handler;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author jiangwei 30012817
 * @since 2023/3/22 17:02
 */
public class MethodInterceptorImpl implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("==========before," + method.getName() + "===========");
        Object res = methodProxy.invokeSuper(o, objects);
        System.out.println("==========after," + method.getName() + "===========");
        return res;
    }
}

```
2. 自定义一个类
```java
package com.jiangwei.test;

/**
 * @author jiangwei 30012817
 * @since 2023/3/22 17:09
 */
public class UserProcess {
    public void hello(){
        System.out.println("****************UserProcess*******************");
    }
}

```
3. 使用
```java
package com.jiangwei;

import com.jiangwei.handler.MethodInterceptorImpl;
import com.jiangwei.handler.MyInvocationHandler;
import com.jiangwei.service.IUserService;
import com.jiangwei.test.UserProcess;
import net.sf.cglib.proxy.Enhancer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author jiangwei 30012817
 * @since 2023/3/17 16:45
 */
@SpringBootTest(classes = AppServerApplication.class)
@RunWith(SpringRunner.class)
public class HandlerTest {
    @Test
    public void cglib() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserProcess.class);
        enhancer.setCallback(new MethodInterceptorImpl());
        UserProcess o = (UserProcess) enhancer.create();
        o.hello();
        System.out.println(o);
    }
}
//==========before,hello===========
//****************UserProcess*******************
//==========after,hello===========
//==========before,toString===========
//==========before,hashCode===========
//==========after,hashCode===========
//==========after,toString===========
//com.jiangwei.test.UserProcess$$EnhancerByCGLIB$$43a52169@57df09a7
```
## 注意事项
### 为什么JDK动态代理只能代理有接口实现的类？
 - 因为JDK动态代理的时候，需要根据被代理对象实现的接口，来生成对应的字节码文件 
 - 而字节码文件中，代理类会自动生成对应的方法，然后执行 InvocationHandler 中的invoke函数 
 - 同时生成的代理类从字节码文件中可以发现，它继承了Proxy类，而Java是单继承，因此不能再继承其他的类 因此动态代理只能通过实现接口的方式去完成 
### 为什么Cglib案例有三段输出？
Enhancer生成的代理类，会拦截所有非final的方法 而上述代码中用到了这段代码：
```java
System.out.println(o);
```
这个函数在执行的时候，需要用到对象的两个方法：
 - toString
 - hashCode

这两个函数是存在于Object中的，所有的类都是Object类的子类，而这两个函数又没有被final修饰 因此当执行的时候，会多输出两段信息 
# 构造模式
```java
public class BuilderTest {
    public static void main(String[] args) {
        Product product = new Product.ProductBuilder().productName("xxx").part1("1").part2("2").part3("3").build();
        System.out.println(product.toString());
    }
}
class Product{
    private final String productName;
    private final String part1;
    private final String part2;
    private final String part3;

    static class ProductBuilder{
        private  String productName;
        private  String part1;
        private  String part2;
        private  String part3;

        public ProductBuilder productName(String productName){
            this.productName = productName;
            return this;
        }

        public ProductBuilder part1(String part1){
            this.part1 = part1;
            return this;
        }

        public ProductBuilder part2(String part2){
            this.part2 = part2;
            return this;
        }

        public ProductBuilder part3(String part3){
            this.part3 = part3;
            return this;
        }

        Product build(){
            return new Product(this.productName,this.part1,this.part2,this.part3);
        }
    }

    public Product(String productName, String part1, String part2, String part3) {
        this.productName = productName;
        this.part1 = part1;
        this.part2 = part2;
        this.part3 = part3;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", part1='" + part1 + '\'' +
                ", part2='" + part2 + '\'' +
                ", part3='" + part3 + '\'' +
                '}';
    }
}
```
# 享元模式
享元模式运用共享技术有效的支持大量细粒度的对象 
- 优点：如果系统有大量类似的对象，可以节省大量的内存及CUP资源
```java

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FlyWeightTest {

    public static void main(String[] args) {
        //创建多个marker对象，里面的参数BaseMessage相同时使用共享的方式
        Marker marker1 = new Marker(new BigDecimal(10),new BigDecimal(30),BaseMessageFactory.getBaseMessage("xx","yy"));
        Marker marker2 = new Marker(new BigDecimal(60),new BigDecimal(80),BaseMessageFactory.getBaseMessage("xx","yy"));
        System.out.println(marker1);
        System.out.println(marker2);
    }
}

class Marker{
    private final BigDecimal lat;
    private final BigDecimal lon;
    private final BaseMessage baseMessage;

    public Marker(BigDecimal lat, BigDecimal lon, BaseMessage baseMessage) {
        this.lat = lat;
        this.lon = lon;
        this.baseMessage = baseMessage;
    }

    @Override
    public String toString() {
        return "Marker{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", baseMessage=" + baseMessage.toString() +
                '}';
    }
}

class BaseMessageFactory{
    //使用map存放已经创建的对象，供其他线程创建时复用
    private static final Map<String,BaseMessage> map = new ConcurrentHashMap<>();
    public static BaseMessage getBaseMessage(String title,String tip){
        if(map.containsKey(title)){
            return map.get(title);
        } else {
            BaseMessage baseMessage = new BaseMessage(title,tip);
            map.put(title,baseMessage);
            return baseMessage;
        }
    }
}


class BaseMessage{
    //字段使用final修饰，创建后不可改变值
    private final String title;
    private final String tip;

    BaseMessage(String title, String tip) {
        System.out.println("create BaseMessage:"+title);
        this.title = title;
        this.tip = tip;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "title='" + title + '\'' +
                ", tip='" + tip + '\'' +
                '}';
    }
}
```
# 门面模式
门面模式为子系统中的一组接口提供一个一致的接口，Facade模式定义了一个高层接口，这个接口使得这一子系统更加容易使用 
- 应用场景：
    - 当需要使用复杂子系统的有限但直接的接口时，请使用Facade模式 
    - 当您想要将子系统组织成层时，请使用Facade 
- 优点: 简化客户端的调用 
```java

public class FacadeTest {

    public static void main(String[] args) {
        Client client = new Client();
        client.printlnSomething();
    }
}

//客户端1
class Client {
    Facate facate = new Facate();

    public void printlnSomething(){
        //使用门面模式来统一调用
        facate.printAll();
    }
}

//门面模式，有facate来统一调用需要调用的方法
class Facate{
    SubMessage1 subMessage1 = new SubMessage1();
    SubMessage2 subMessage2 = new SubMessage2();
    SubMessage3 subMessage3 = new SubMessage3();

    public void printAll(){
        subMessage1.printSub1();
        subMessage2.printSub2();
        subMessage3.printSub3();
    }
}

//子类
class SubMessage1{
    public void printSub1(){
        System.out.println("printSub1...");
    }
}

class SubMessage2{
    public void printSub2(){
        System.out.println("printSub2...");
    }
}

class SubMessage3{
    public void printSub3(){
        System.out.println("printSub3...");
    }
}
```
# 适配器模式
适配器模式将一个类的接口转换成客户希望的另一个接口，Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作 
- 应用场景
    - 当你希望使用某些现有类，但其接口与你的其它代码不兼容时，请使用适配器类 
    - 当你希望重用几个现有的子类，这些子类缺少一些不能添加到超类中的公共功能时，请使用该模式 
- 优点
    - 符合单一职责原则 
    - 符合开闭原则 
## Object形式的适配器模式（注入类的方式）:
```java
public class AdapterObject {

    public static void main(String[] args) {
        Target target = new Adapter(new Adaptee());
        System.out.println(target.adapter10v());
    }
}

interface Target {
    int adapter10v();
}

class Adapter implements Target {

    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public int adapter10v() {
        int i = adaptee.adapter220v();
        //经过处理，输出10V的电压
        int j = i / 22;
        System.out.printf("原电压  %d  v   转化后电压   %d  v%n", i, j);
        return j;
    }
}

class Adaptee {
    public int adapter220v() {
        return 220;
    }
}
```
## Class形式的适配器模式（继承类的方式）：
```java
public class AdapterClassDemo {
 
    public static void main(String[] args) {
        TargetClass targetClass = new AdapterClass();
        System.out.println(targetClass.adapter10v());
    }
}
 
interface TargetClass{
    int adapter10v();
}
 
//使用类继承的方式
class AdapterClass extends AdapteeClass implements TargetClass{
 
    @Override
    public int adapter10v() {
        int i = adapter220v();
        int j = i /22;
        System.out.println(String.format("原电压  %d  v   转化后电压   %d  v",i,j));
        return j;
    }
}
 
 
class AdapteeClass{
    public int adapter220v(){
        return 220;
    }
}
```
# 装饰者模式
装饰者模式Decorator在不改变原有对象的基础上，将功能附加到对象上 
- 应用场景
    - 扩展一个类的功能或给一个类添加附加功能
- 优点
    - 不改变原有对象的情况下，给一个对象扩展功能
    - 使用不同的组合可以实现不同的效果
    - 符合开闭原则（对扩展开放，对修改关闭）
- [装饰器模式和代理模式的区别](https://www.cnblogs.com/lyh233/p/16004958.html)
```java
public class DecoratorTest {
    public static void main(String[] args) {
        //这是一开始既有的实现
        //Component component = new CreateComponent();
        //component.operation();
        //使用接口接收创建的类；创建具体类的实现作为它实现的接口，此接口是一个参数，即可在不改变原有对象的基础上，扩展附加一些属性
        Coffee coffee = new MilkDecorator(new SugarDecorator(new CreateCoffee()));
        coffee.operation();
    }
}

interface Coffee {
    void operation();
}

class CreateCoffee implements Coffee {
    @Override
    public void operation() {
        System.out.println("咖啡");
    }
}

//抽象类实现接口，可以不实现接口中的方法，当子类继承此抽象类时再对接口中的方法进行实现
abstract class Decorator implements Coffee {
    Coffee coffee;

    public Decorator(Coffee coffee){
        this.coffee = coffee;
    }
}

//子类继承抽象父类，需要实现父类未实现的接口方法，需要调用父类的定义的构造函数
class SugarDecorator extends Decorator{

    //构造父类的构造方法
    public SugarDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public void operation() {
        System.out.println("糖");
        //调用创建CreateDecorator1类时传递到父抽象类Decorator的component
        coffee.operation();
    }
}

class MilkDecorator extends Decorator{

    public MilkDecorator(Coffee coffee) {
        super(coffee);
    }

    @Override
    public void operation() {
        System.out.println("奶");
        coffee.operation();
    }
}
```
# 模板方法模式
模板方法模式Template Method定义一个操作的算法骨架，而将一些步骤延迟到子类中，使得子类可以不改变一个算法的结构，即可重定义改算法的某些特定步骤 

应用场景：
 - 一次性实现一个算法不变的部分，并将可变部分留给子类实现 
 - 各个子类中，公共部分被提取出来，集中到一个公共的父类中，避免代码重复 

优点：
 - 提高代码复用性：将相同部分代码，放到抽象的父类中；
 - 提供扩展性：将不同的代码，放到不同的子类中，通过对子类的扩展，增加新的行为；
 - 符合开闭原则：通过父类调用子类的操作，通过对子类的扩展来增加新的行为 
```java
public class TemplateTest {

    public static void main(String[] args) {
        AbstractClass abstractClass1 = new SubClass1();
        AbstractClass abstractClass2 = new SubClass2();
        abstractClass1.operator();
        abstractClass2.operator();
    }
}

/**
 * 抽象父类
 */
abstract class AbstractClass{
    //一般此方法是默认调用的方法
    public void operator(){
        System.out.println("准备");
        System.out.println("处理");
        //调用模板方法，此方法由子类实现
        templateMethod();
    }

    protected abstract void templateMethod();
}

class SubClass1 extends AbstractClass{
    //重写父类定义的抽象方法
    @Override
    protected void templateMethod() {
        System.out.println("子类1实现的方法");
    }
}

class SubClass2 extends AbstractClass{
    //重写父类定义的抽象方法
    @Override
    protected void templateMethod() {
        System.out.println("子类2实现的方法");
    }
}
```
# 观察者模式
观察者模式Observer定义了对象之间的一对多依赖，让多个观察者对象同时监听某一个主题对象，当主题对象发生变化时，它的所有依赖者都会收到通知并更新 

应用场景：
 - 当更改一个对象的状态可能需要更改其它对象，并且实际的对象集事先未知或动态更改时，请使用观察者模式

优点：
 - 符合开闭原则
 - 可以在运行时建立对象之间的关系
```java
import java.util.ArrayList;
import java.util.List;

public class ObserverTest {// 观察者模式
    public static void main(String[] args) {
        Subject subject = new Subject();
        Observer SubObserverClass1 = new SubObserverClass1();
        Observer SubObserverClass2 = new SubObserverClass2();
        //把观察者对象添加进依赖的对象里面
        subject.add(SubObserverClass1);
        subject.add(SubObserverClass2);
        subject.notifyAll("数据有更新了...");
        System.out.println("...移出某个需要通知的对象后...");
        subject.remove(SubObserverClass1);
        subject.notifyAll("数据再次更新了...");
    }
}

class Subject {
    List<Observer> list = new ArrayList<>();

    //添加需要通知的对象
    public void add(Observer observer){
        if(!list.contains(observer)){
            list.add(observer);
        }
    }

    //移出需要通知的对象
    public void remove(Observer observer){
        list.remove(observer);
    }

    //有变动，通知所有的监听对象
    public void notifyAll(Object object){
        for (Observer observer : list) {
            observer.notify(object);
        }
    }
}

interface Observer{
    void notify(Object object);
}

class SubObserverClass1 implements Observer{
    @Override
    public void notify(Object object) {
        System.out.println("通知消息1："+object);
    }
}

class SubObserverClass2 implements Observer{
    @Override
    public void notify(Object object) {
        System.out.println("通知消息2："+object);
    }
}
```
# 责任链模式
责任链模式Chain Of Responsibility 为请求创建了一个接收者对象的链

应用场景：
 - 一个请求的处理需要多个对象当中的一个或几个协作处理

优点：
 - 请求的发送者和接受者解耦
 - 可以控制执行顺序
 - 符合开闭原则和单一职责原则
 ```java
public class ChainOfResponsibilityTest {// 责任链模式
    public static void main(String[] args) {
        //通过构建者模式创建request
        Request request = new Request.RequestBuilder().isLogin(true).isEnter(true).build();
        //定义责任链，LoginHandler的next为EnterHandler，EnterHandler的next为null
        Handler handler = new LoginHandler(new EnterHandler(null));
        boolean process = handler.process(request);
        if(process){
            System.out.println("校验通过");
        } else {
            System.out.println("校验不通过");
        }
    }
}

//定义一个请求类
class Request {
    private final boolean isLogin;
    private final boolean isEnter;

    public Request(boolean isLogin, boolean isEnter) {
        this.isLogin = isLogin;
        this.isEnter = isEnter;
    }

    //使用建造者模式创建
    static class RequestBuilder{
        private boolean isLogin;
        private boolean isEnter;

        RequestBuilder isLogin(boolean isLogin){
            this.isLogin = isLogin;
            return this;
        }

        RequestBuilder isEnter(boolean isEnter){
            this.isEnter = isEnter;
            return this;
        }

        Request build(){
            return new Request(isLogin,isEnter);
        }
    }

    public boolean isLogin() {
        return isLogin;
    }

    public boolean isEnter() {
        return isEnter;
    }
}

//定义一个抽象父类
abstract class Handler{
    Handler next;
    abstract boolean process(Request request);

    public Handler(Handler next) {
        this.next = next;
    }

    public Handler getNext() {
        return next;
    }

    public void setNext(Handler next) {
        this.next = next;
    }
}

//定义子类
class LoginHandler extends Handler{

    //定义next的值
    public LoginHandler(Handler next) {
        super(next);
    }

    @Override
    boolean process(Request request) {
        System.out.println("登陆校验");
        if(request.isLogin()){
            Handler next = getNext();
            if(null == next){
                return true;
            }
            return next.process(request);
        }
        return false;
    }
}

//定义子类
class EnterHandler extends Handler{

    public EnterHandler(Handler next) {
        super(next);
    }

    @Override
    boolean process(Request request) {
        System.out.println("进入处理");
        if(request.isEnter()){
            Handler next = getNext();
            if(null == next){
                return true;
            }
            return next.process(request);
        }
        return false;
    }
}
 ```
