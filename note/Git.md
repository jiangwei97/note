## 生成公钥
```bash
ssh-keygen -t rsa -C "{email}"
```
## 关闭SSL：
```bash
git config --global http.sslVerify false
```
## 查看近count次提交的ID、作者、时间、注释：
```bash
git log --pretty=format:"%h - %an, %ar : %s {count}"
```
## 查看指定远程分支的commit记录
```bash
git log origin/topic/pink
```
## 指定.git文件夹：
```bash
git --git-dir={local_path}.git {command}
```
## 比较两次commit差异文件：
```bash
git diff --name-status --no-renames {commitId1} {commitId2}
```
## 查看某次提交某个文件内容：
```bash
git show {commitId}:{filePath}
```
## 克隆裸仓库
```bash
git clone --bare {remoteUrl}
git clone --mirror {remoteUrl}
```
## 更新裸仓库
```bash
git fetch --all
```
## 列出全部文件
```bash
git ls-files -c
# 裸仓库
git ls-tree --full-tree -r --name-status 
```
## 追踪过程
```bash
GIT_TRACE=1 GIT_CURL_VERBOSE=1 git xxx
```
## tag
- https://blog.csdn.net/qq_21746331/article/details/120776710
```bash
# 为当前分支所在的提交记录打上轻量标签。
git tag <lightweght_name> 
# 为某次具体的提交记录打上轻量标签。
git tag <lightweght_name> <commit SHA-1 value>
# 为当前分支所在的提交记录打上附注标签。
git tag -a <anotated_name> -m <tag_message>
# 列出所有的标签名。
git tag
# 删除某个标签，本质上就是移除 .git/refs/tags/ 中对应的文件。
git tag -d <tag_name>
# 显示标签对应提交记录的具体信息。
git show <tag_name>
# 推送某个标签到远程仓库。
git push <remote> <tag_name>
#  --tags推送所有标签到远程仓库。
git push <remote>
# 删除远程仓库中的某个标签。
git push <remote> --delete <tag_name>
```