# Dockerfile
### spring-boot打包镜像
```dockerfile
FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/xxx.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```
### MySQL8.0
```dockerfile
FROM mysql:8
ENV MYSQL_ROOT_PASSWORD=root
EXPOSE 3306
CMD ["mysqld"]
```
### 时区设置为上海时间
```dockerfile
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone
```
# Docker命令

### 在Dockerfile目录下打包镜像

```bash
docker build -t {imageName}:{version} .
```
### 列出全部镜像
```bash
docker images
```
### 运行镜像
```bash
# 直接run
docker run -it -d {imageName}:{version}
# 映射端口，容器的80端口映射到宿主机的8000端口，容器的22端口映射到宿主机的2222端口
docker run -p 8000:80 -p 2222:22 -it -d {imageName}:{version}
#  设置容器名称
--name {name}
```
### 列出运行中的镜像
```bash
# 活的
docker ps
# 全部
docker ps -a
```
### 进入运行中的容器
```bash
docker exec -it {containerId} /bin/bash
```
### 查看日志
```bash
# 查看全部日志
docker logs {containerId}
# 动态查看近100行日志
docker logs -f -t --tail=100 {containerId}
docker logs --tail 100 <containerId>
```
### 删除镜像
```bash
docker rmi {imageName}:{version}
```
### 删除容器
```bash
# 删除特定容器
docker rm {containerId}
# 删除全部容器
docker rm -f $(docker ps -a -q)
```
### 查看容器端口映射
```bash
docker port {containerId}
docker port {names}
```
### 把正在运行的镜像打包成新镜像
```bash
docker commit {containerId} {imageName}:{version}
```
### 刷新配置文件
```bash
systemctl daemon-reload
```
### 重启docker
```bash
systemctl restart docker
```
### 设置docker代理
```bash
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo vim /etc/systemd/system/docker.service.d/https-proxy.conf
# 在该文件内添加如下配置项
[Service]
Environment="代理地址"
```
### 查看docker代理
```bash
systemctl show --property=Environment docker
```
### 删除空镜像
```bash
#停止容器
docker stop $(docker ps -a | grep "Exited" | awk '{print $1 }')
#删除容器
docker rm $(docker ps -a | grep "Exited" | awk '{print $1 }')
#删除镜像
docker rmi $(docker images | grep "none" | awk '{print $3}')
```
### 镜像打成tar包
```dockerfile
docker save -o ./demo.tar <containerId>
```
### 加载tar包
```dockerfile
docker load -i demo.tar
```
### 复制宿主机文件到容器
```dockerfile
# 在容器内执行
docker cp <本地文件路径> <容器名称或ID>:<容器内目标路径>
```
### 复制容器文件到宿主机
```dockerfile
# 在宿主机上执行
docker cp <容器名称或ID>:<容器内文件路径> <本地目标路径>
```
