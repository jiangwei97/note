# 目录
- [中文官网](#中文官网)
- [对比Mysql](#对比Mysql)
- [HTTP语法](#HTTP语法)
    - [索引](#索引)
    - [文档](#文档)
    - [映射关系](#映射关系)
- [Java集成（高级别客户端）](#Java集成（高级别客户端）)
    - [依赖](#高级别客户端依赖)
    - [索引操作](#索引操作)
    - [文档操作](#文档操作)
- [Java集成（SpringData）](#Java集成（SpringData）)
    - [依赖](#SpringData依赖)
    - [配置文件](#配置文件)
    - [Dao层](#Dao层)
    - [测试类](#测试类)
- [Kibana(7.8.0)](#Kibana(7.8.0))
    - [准备工作](#准备工作)
    - [Kibana控制台语法](#Kibana控制台语法)
    - [集群管理工具](#集群管理工具)
    - [分词和评分机制](#分词和评分机制)
- [EQL语法](#EQL语法)
    - [准备数据](#准备数据)
    - [EQL语法](#EQL语法)
- [SQL语法](#SQL语法)
# 对比Mysql
## 名词
|  ES   | field  | document | index | mapping | cluster  |
| :---: | :----: | :------: | :---: | :-----: | :------: |
| MYSQL | column | row      | table | schema  | database |
# 中文官网
https://www.elastic.co/cn
# HTTP语法
## 索引
### 创建索引:shopping
```
PUT http://127.0.0.1:9200/shopping
```
```
{
    "acknowledged": true,
    "shards_acknowledged": true,
    "index": "shopping"
}
```
### 查询索引:shopping
```
GET http://127.0.0.1:9200/shopping
```
```
{
    "shopping": {
        "aliases": {},
        "mappings": {},
        "settings": {
            "index": {
                "creation_date": "1671529077835",
                "number_of_shards": "1",
                "number_of_replicas": "1",
                "uuid": "N7O_g2mSRNWzjezlJkDW2g",
                "version": {
                    "created": "7080099"
                },
                "provided_name": "shopping"
            }
        }
    }
}
```
### 删除索引:shopping
```
DELETE http://127.0.0.1:9200/shopping
```
```
{
    "acknowledged": true
}
```
### 查询全部索引
```
GET http://127.0.0.1:9200/_cat/indices?v
```
```
health status index    uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   box      YYEDau4WSVegN5gZ1ZNi8Q   1   1          0            0       208b           208b
yellow open   shopping N7O_g2mSRNWzjezlJkDW2g   1   1          0            0       208b           208b

```
## 文档
### 添加文档
```
POST http://127.0.0.1:9200/shopping/_doc
POST http://127.0.0.1:9200/shopping/_doc/1001 (指定ID)
PUT http://127.0.0.1:9200/shopping/_doc/1001
POST http://127.0.0.1:9200/shopping/_create/1001
PUT http://127.0.0.1:9200/shopping/_create/1001

BODY:
{
    "title":"小米手机",
    "category":"小米",
    "images":"https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
    "price":"3999.00"
}
```
```
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "jto_MoUB_OPly7bBZoQj",
    "_version": 1,
    "result": "created",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 0,
    "_primary_term": 1
}
```
### 修改文档
#### 全覆盖
```
PUT http://127.0.0.1:9200/shopping/_doc/1001
BODY:
{
    "title":"小米手机",
    "category":"小米",
    "images":"https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
    "price":"4999.00"
}
```
```
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001",
    "_version": 2,
    "_seq_no": 3,
    "_primary_term": 1,
    "found": true,
    "_source": {
        "title": "小米手机",
        "category": "小米",
        "images": "https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
        "price": "4999.00"
    }
}
```
#### 修改指定字段
```
POST http://127.0.0.1:9200/shopping/_update/1001
BODY:
{
    "doc": {
        "title":"华为手机"
    }
}
```
```
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001",
    "_version": 3,
    "result": "updated",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 4,
    "_primary_term": 1
}
```
### 删除文档
```
DELETE http://127.0.0.1:9200/shopping/_doc/1001
```
```
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "jto_MoUB_OPly7bBZoQj",
    "_version": 2,
    "result": "deleted",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 5,
    "_primary_term": 1
}
```
### 查询文档
#### 指定ID
```
GET http://127.0.0.1:9200/shopping/_doc/1001
```
```
找到：
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001",
    "_version": 1,
    "_seq_no": 1,
    "_primary_term": 1,
    "found": true,
    "_source": {
        "title": "小米手机",
        "category": "小米",
        "images": "https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
        "price": "3999.00"
    }
}
没找到：
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1003",
    "found": false
}
```
#### 查询列表
```
GET http://127.0.0.1:9200/shopping/_search
```
```
{
    "took": 36,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 3,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "jto_MoUB_OPly7bBZoQj",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
                    "price": "3999.00"
                }
            },
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
                    "price": "3999.00"
                }
            },
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1002",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "https://img2.baidu.com/it/u=2967954403,393920502&fm=253&fmt=auto&app=138&f=JPEG?w=545&h=300",
                    "price": "3999.00"
                }
            }
        ]
    }
}
```
#### 查询category=小米的列表
```
GET http://127.0.0.1:9200/shopping/_search?q=category:小米
```
```
GET http://127.0.0.1:9200/shopping/_search
```
```json
{
    "query": {
        "match":{
            "category":"小米"
        }
    }
}
```
#### 分页查询
```json
{
    "query": {
        "match":{
            "category":"小米"   /* 筛选category=小米 */
        }
    },
    "from": 0,  //(页码-1)*size
    "size": 5,  //页大小
    "_source": ["title"],    //筛选字段
    "sort": {   // 排序
        "price": {
            "order":"desc"
        }
    }
}
```
#### 多条件查询
```json
// where category='小米' and price=4999.00
{
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "category": "小米"
                    }
                },
                {
                    "match": {
                        "price": 4999.00
                    }
                }
            ]
        }
    }
}
// where (category='小米' or price=4999.00)
// and price > 5000 
{
    "query": {
        "bool": {
            "should": [
                {
                    "match": {
                        "category": "小米"
                    }
                },
                {
                    "match": {
                        "category": "华为"
                    }
                }
            ],
            "filter": {
                "range": {
                    "price": {
                        "gt": 5000
                    }
                }
            }
        }
    }
}
```
#### 精确查询 不分词
```json
{
    "query": {
        "match_phrase":{
            "category":"小米"
        }
    },
    "highlight": {  // 高亮显示
        "fields": {
            "category": {}
        }
    }
}
```
#### 聚合查询
```json
// GROUP BY price
{
    "aggs": {   // 聚合操作
        "price_group": {    // 名称 随便起
            "terms": {  // 分组
                "fields": "price"   // 分组字段
            }
        }
    }
}
// price的平均值
{
    "aggs": {   // 聚合操作
        "price_avg": {    // 名称 随便起
            "avg": {  // 分组
                "fields": "price"   // 分组字段
            }
        }
    }
}
```
## 映射关系
```
PUT http://127.0.0.1:9200/user/_mapping
```
```json
{
    "properties": {
        "name": {
            "type": "text",
            "index": true
        },
        "sex": {
            "type": "keyword", //必须精确查询
            "index": true
        },
        "tel": {
            "type": "keyword",
            "index": false //没有被索引 不支持查询
        }
    }
}
```
```json
{
    "acknowledged": true
}
```
# Java集成（高级别客户端）
## pom.xml
```xml
<!-- https://mvnrepository.com/artifact/org.elasticsearch/elasticsearch -->
<dependency>
    <groupId>org.elasticsearch</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>7.8.0</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.elasticsearch.client/elasticsearch-rest-high-level-client -->
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-high-level-client</artifactId>
    <version>7.8.0</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api -->
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-api</artifactId>
    <version>2.19.0</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-core</artifactId>
    <version>2.19.0</version>
</dependency>
```
## 索引操作
```java
package com.jiangwei.test.utils;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;

import java.io.IOException;

/**
 * @author jiangwei97@aliyun.com
 * @since 2022/12/21 16:42
 */
public class EsClient {

    public static void main(String[] args) throws IOException {

        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("127.0.0.1", 9200, "http"))
        );
        String indexName = "user";

        // 创建索引
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        CreateIndexResponse response = esClient.indices().create(request, RequestOptions.DEFAULT);
        // 查询索引
        // GetIndexRequest request = new GetIndexRequest(indexName);
        // GetIndexResponse response = esClient.indices().get(request, RequestOptions.DEFAULT);
        // 删除索引
        // DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        // AcknowledgedResponse response = esClient.indices().delete(request, RequestOptions.DEFAULT);

        esClient.close();

    }

}
```
## 文档操作
```java
package com.jiangwei.test.utils;

import com.alibaba.fastjson.JSONObject;
import com.jiangwei.test.pojo.EsUser;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;

/**
 * @author jiangwei97@aliyun.com
 * @since 2022/12/21 16:42
 */
public class EsDocClient {

    public static void main(String[] args) throws IOException {

        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("127.0.0.1", 9200, "http"))
        );
        String indexName = "user";

        // 插入文档
//        IndexRequest request = new IndexRequest();
//        request.index(indexName).id("1001");
//        EsUser user = new EsUser("zhangsan", "男", 25);
//        String userJson = JSONObject.toJSONString(user);
//        request.source(userJson, XContentType.JSON);
//        IndexResponse response = esClient.index(request, RequestOptions.DEFAULT);

        // 修改文档
//        UpdateRequest request = new UpdateRequest();
//        request.index(indexName).id("1001");
//        request.doc(XContentType.JSON, "sex", "女");
//        UpdateResponse response = esClient.update(request, RequestOptions.DEFAULT);

        // 查询文档
//        GetRequest request = new GetRequest();
//        request.index(indexName).id("1001");
//        GetResponse response = esClient.get(request, RequestOptions.DEFAULT);

        // 删除文档
//        DeleteRequest request = new DeleteRequest();
//        request.index(indexName).id("1001");
//        DeleteResponse response = esClient.delete(request, RequestOptions.DEFAULT);

        // 批量插入
//        EsUser user1 = new EsUser("lisi", "男", 25);
//        EsUser user2 = new EsUser("wangwu", "男", 35);
//        EsUser user3 = new EsUser("xueliu", "女", 45);
//        BulkRequest request = new BulkRequest();
//        request.add(new IndexRequest().index(indexName).id("1002")
//                .source(JSONObject.toJSONString(user1), XContentType.JSON));
//        request.add(new IndexRequest().index(indexName).id("1003")
//                .source(JSONObject.toJSONString(user2), XContentType.JSON));
//        request.add(new IndexRequest().index(indexName).id("1004")
//                .source(JSONObject.toJSONString(user3), XContentType.JSON));
//        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);

        // 批量删除
//        BulkRequest request = new BulkRequest();
//        request.add(new DeleteRequest().index(indexName).id("1002"));
//        request.add(new DeleteRequest().index(indexName).id("1003"));
//        request.add(new DeleteRequest().index(indexName).id("1004"));
//        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);
// --------------------------------------------------------------------------------------------------------
        // 高级查询
        SearchRequest request = new SearchRequest();
        request.indices(indexName);

        // 全部查询
        // MatchAllQueryBuilder query = QueryBuilders.matchAllQuery();
        // SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(query);

        // 条件查询 where age=35
        // TermQueryBuilder query = QueryBuilders.termQuery("age", "35");
        // SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(query);

        // 分页查询、排序 from：从第几条开始 size：页大小 sort：排序
//        MatchAllQueryBuilder query = QueryBuilders.matchAllQuery();
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(query);
//        int pageNum = 2;
//        int pageSize = 2;
//        sourceBuilder.from((pageNum - 1) * pageSize).size(pageSize);
//        sourceBuilder.sort("age", SortOrder.DESC);

        // 过滤字段
//        MatchAllQueryBuilder query = QueryBuilders.matchAllQuery();
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(query);
//        String[] includes = {}; // 包含
//        String[] excludes = {"age"}; // 排除
//        sourceBuilder.fetchSource(includes, excludes);

        // 组合查询
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        // where age = 35 and sex = '男'
////        boolQueryBuilder
////                .must(QueryBuilders.matchQuery("age", 35))
////                .must(QueryBuilders.matchQuery("sex", "男"));
//        // where age = 35 or age = 45
//        boolQueryBuilder
//                .should(QueryBuilders.matchQuery("age", 35))
//                .should(QueryBuilders.matchQuery("age", 45));
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(boolQueryBuilder);

        // 范围查询
//        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("age");
//        rangeQueryBuilder.gte(30).lte(45);
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(rangeQueryBuilder);

        // 模糊查询
//        FuzzyQueryBuilder fuzziness = QueryBuilders.fuzzyQuery("name", "lis").fuzziness(Fuzziness.TWO);
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(fuzziness);

        // 高亮查询
//        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "lisi");
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().query(termQueryBuilder);
//        HighlightBuilder highlightBuilder = new HighlightBuilder();
//        highlightBuilder.preTags("<span style='color:red'>");
//        highlightBuilder.postTags("</span>");
//        highlightBuilder.field("name");
//        sourceBuilder.highlighter(highlightBuilder);

        // 聚合查询 select max(age)
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//        AggregationBuilder aggregationBuilder = AggregationBuilders.max("maxAge").field("age");
//        sourceBuilder.aggregation(aggregationBuilder);

        // 分组查询 group by age
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("ageGroup").field("age");
        sourceBuilder.aggregation(aggregationBuilder);


        request.source(sourceBuilder);
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
        SearchHits hits = response.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        esClient.close();
    }

}

```
# Java集成（Spring Data）
## pom.xml
```xml
<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-elasticsearch -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
    <version>2.7.0</version>
</dependency>
```
## 配置文件
```java
package com.jiangwei.test.configuration;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2022/12/23 15:10
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Configuration
@ConfigurationProperties(prefix = "elasticsearch")
public class ElasticSearchConfig extends AbstractElasticsearchConfiguration {

    private String host;

    private Integer port;

    @Override
    public RestHighLevelClient elasticsearchClient() {
        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port));
        return new RestHighLevelClient(builder);
    }
}
```
## 实体类
```java
package com.jiangwei.test.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2022/12/23 15:08
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(indexName = "product", shards = 3, replicas = 1) // 索引名：product  主分片：3  副本：1
public class EsProduct {

    @Id
    private Long id;

    @Field(type = FieldType.Text) // 分词
    private String title;

    @Field(type = FieldType.Keyword) // 不分词
    private String category;

    @Field(type = FieldType.Double)
    private Double price;

    @Field(type = FieldType.Keyword, index = false) // 不能查询
    private String images;

}
```
## Dao层
```java
package com.jiangwei.test.mapper;

import com.jiangwei.test.pojo.EsProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @date 2022/12/23 10:46
 */

@Repository
public interface EsProductMapper extends ElasticsearchRepository<EsProduct, Long> {
}

```
## 测试类
```java
package com.jiangwei.test;

import com.jiangwei.test.mapper.EsProductMapper;
import com.jiangwei.test.model.PageModel;
import com.jiangwei.test.pojo.EsProduct;
import com.jiangwei.test.pojo.User;
import com.jiangwei.test.service.IMongoService;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2022/12/23 16:10
 */

@SpringBootTest(classes = AppServerApplication.class)
@RunWith(SpringRunner.class)
public class ElasticSearchTest {

    @Resource
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Resource
    private EsProductMapper esProductMapper;

    // 创建索引
    @Test
    public void createIndex(){
        // 根据配置自动创建
    }

    // 删除索引
    @Test
    public void deleteIndex(){
        elasticsearchRestTemplate.deleteIndex(EsProduct.class);
    }

    // 新增文档
    @Test
    public void save(){
        EsProduct esProduct = new EsProduct(
                1L,
                "华为手机",
                "手机",
                9999.5,
                "http://www.baidu.com"
        );
        esProductMapper.save(esProduct);
    }

    // 修改文档ById
    @Test
    public void updateById(){
        EsProduct esProduct = new EsProduct(
                2L,
                "小米手机",
                "手机",
                3999.5,
                "http://www.baidu.com"
        );
        esProductMapper.save(esProduct);
    }

    // 根据ID查询文档
    @Test
    public void findById(){
        EsProduct esProduct = esProductMapper.findById(2L).get();
        System.out.println(esProduct);
    }

    // 查询全部文档
    @Test
    public void findAll(){
        ArrayList<EsProduct> esProducts = Lists.newArrayList(esProductMapper.findAll());
        for (EsProduct product : esProducts) {
            System.out.println(product);
        }
    }

    // 删除文档ById
    @Test
    public void deleteById(){
        esProductMapper.deleteById(1L);
    }

    // 批量插入文档
    @Test
    public void saveBatch(){
        EsProduct esProduct1 = new EsProduct(3L, "苹果手机", "手机", 8999.5, "");
        EsProduct esProduct2 = new EsProduct(4L, "OPPO手机", "手机", 6999.5, "");
        EsProduct esProduct3 = new EsProduct(5L, "VIVO手机", "手机", 5999.5, "");
        ArrayList<EsProduct> esProducts = Lists.newArrayList(esProduct1, esProduct2, esProduct3);
        esProductMapper.saveAll(esProducts);
    }

    // 分页查询文档
    @Test
    public void page(){
        Sort sort = Sort.by(Sort.Direction.DESC, "price");
        int pageNum = 0; // 第一页=0
        int pageSize = 3; // 页大小
        PageRequest pageRequest = PageRequest.of(pageNum, pageSize, sort);
        Page<EsProduct> esProductPage = esProductMapper.findAll(pageRequest);
        for (EsProduct esProduct : esProductPage.getContent()) {
            System.out.println(esProduct);
        }
    }

    // 文档条件查询
    @Test
    public void termQuery(){
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("category", "手机");
        int pageNum = 0; // 第一页=0
        int pageSize = 3; // 页大小
        Sort sort = Sort.by(Sort.Direction.DESC, "price");
        PageRequest pageRequest = PageRequest.of(pageNum, pageSize, sort);
        Page<EsProduct> page = esProductMapper.search(termQueryBuilder, pageRequest);
        for (EsProduct product : page.getContent()) {
            System.out.println(product);
        }
    }

}

```
# Kibana(7.8.0)
## 准备工作
### 下载地址
```
https://www.elastic.co/cn/downloads/kibana#ga-release
```
### 配置文件
```yaml
# 编辑 D:\Kibana\kibana-7.8.0\config\kibana.yml 追加下文
# 默认端口
server.port: 5601
# ES服务器地址
elasticsearch.hosts: ["http://localhost:9200"]
# 索引名
kibana.index: ".kibana"
# 支持中文
i18n.locale: "zh-CN"
```
### 启动kibana
- 启动 D:\Kibana\kibana-7.8.0\bin\kibana.bat
## Kibana控制台语法
- 和HTTP语法相同，取消Host
```
# 查看全部索引
GET _cat/indices?v
```
## 集群管理工具：elasticsearch-head-chrome-plugin
- [下载elasticsearch-head-chrome-plugin](https://codeload.github.com/mobz/elasticsearch-head/zip/master)
- 解压elasticsearch-head-master
- 进入crx目录下，复制es-head.crx,将副本后缀改成rar，然后解压成es-head目录文件夹
- Chrome打开开发者模式>>加载已解压的扩展程序，选择刚刚第3步中解压缩的es-head插件目录
## 分词和评分机制
### IK分词器下载地址
- [下载IK分词器](https://github.com/medcl/elasticsearch-analysis-ik/releases)
- 解压后将文件夹放在 `D:\ES\elasticsearch-7.8.0\plugins`
### 英文分词
```json
GET _analyze
{
  "analyzer": "standard",
  "text": ["i am zhangsan"]
}
```
### 中文分词 最少切分
```json
GET _analyze
{
  "analyzer": "ik_smart",
  "text": ["我是一个三好学生"]
}
```
### 中文分词 最细粒度划分
```json
GET _analyze
{
  "analyzer": "ik_max_word",
  "text": ["我是一个三好学生"]
}
```
### 自定义分词器
- 在 `D:\ES\elasticsearch-7.8.0\plugins\elasticsearch-analysis-ik-7.8.0\config`下添加test.dic(编码UTF-8)
- test.dic添加内容，如 `我是一个三好学生`
- 修改 `D:\ES\elasticsearch-7.8.0\plugins\elasticsearch-analysis-ik-7.8.0\config\IKAnalyzer.cfg.xml`
   ```xml
   <entry key="ext_dict">test.dic</entry>
   ```
### 评分机制（_score）
#### 测试
```
# 添加索引
PUT test_score
# 添加文档
PUT test_score/_doc/1001
{
  "name": "zhang kai shuang shou, ha ha ha"
}
# 添加文档
PUT test_score/_doc/1002
{
  "name": "zhang san"
}
# 查询索引和评分机制计算公式
GET test_score/_search?explain=true
{
  "query": {
    "match": {
      "name": "zhang"
    }
  }
}
```
#### 公式：boost * idf * tf
- boost：
    - 描述：权重系数（默认2.2）
- idf：
    - 描述：逆文档频率Inverse Document Frequency，搜索文本中的各个词条 (temm) 在整索引的所有文档中出现了多少次, 出现的次数越多, 说明越不重要, 也就越不相关, 得分就比较低.
    - 公式：log(1 + (N - n + 0.5) / (n + 0.5))
        - N：在文档中有多少匹配的字段
        - n: 文档当中包含的关键词的数量
- tf
    - 描述：词频Term Frequency，搜索文本中的各个词条(term)在查询文本中对现了了多少次出现次数越多，就越相关，得分会比较高
    - 公式：freq / (freq + k1 * (1 - b + b * dl / avgdl))
        - freq：词条出现的次数
        - k1：默认值1.2
        - b：词条长度和文档长度的关系
        - dl：文档分词的个数
        - avgdl：所有的字段数/匹配的文档数
#### 修改权重
```
GET product/_search?explain=true
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "title": {
              "query": "小米",
              "boost": 1
            }
          }
        },
        {
          "match": {
            "title": {
              "query": "苹果",
              "boost": 1
            }
          }
        },
        {
          "match": {
            "title": {
              "query": "华为",
              "boost": 5
            }
          }
        }
      ]
    }
  }
}
```
# EQL语法
## 准备数据
```json
PUT /gmall
PUT _bulk
{"index":{"_index":"gmall"}}
{
  "@timestamp":"2022-06-01T12:00:00.00+08:00", 
  "event": {"category":"page"},
  "page":{
    "session_id": "42FC7E13-CB3-5C05-0000-0010A0125101",
    "last_page_id": "", 
    "page_id" : "login",
    "user_id" : ""
  }
}
{"index":{"_index":"gmall"}}
{
  "@timestamp":"2022-06-01T12:01:00.00+08:00", 
  "event":{"category":"page"},
  "page":{
    "session_id": "42FC7E13-CB3-5C05-0000-0010A0125101",
    "last_page_id": "login", 
    "page_id" : "good_list",
    "user_id" : "1"
  }
}
{"index":{"_index":"gmall"}}
{
  "@timestamp":"2022-06-01T12:05:00.00+08:00", 
  "event":{"category":"page"},
  "page":{
    "session_id": "42FC7E13-CB3-5C05-0000-0010A0125101",
    "last_page_id": "good_list", 
    "page_id" : "good_detail",
    "user_id" : "1"
  }
}
{"index":{"_index":"gmall"}}
{
  "@timestamp":"2022-06-01T12:07:00.00+08:00", 
  "event":{"category":"page"},
  "page":{
    "session_id": "42FC7E13-CB3-5C05-0000-0010A0125101",
    "last_page_id": "good_detail", 
    "page_id" : "order",
    "user_id" : "1"
  }
}
{"index":{"_index":"gmall"}}
{
  "@timestamp":"2022-06-01T12:08:00.00+08:00", 
  "event":{"category":"page"},
  "page":{
    "session_id": "42FC7E13-CB3-5C05-0000-0010A0125101",
    "last_page_id": "order", 
    "page_id" : "payment",
    "user_id" : "1"
  }
}
```
## EQL语法
### 查询所有user_id=1的文档
```json
GET /gmall/_eql/search
{
  "query": """
      any where page.user_id == "1"
  """
}
```
### 查询所有时间戳范围
```json
GET /gmall/_eql/search
{
  "query": """
    any where true
  """,
  "filter": {
    "range": {
      "@timestamp": {
        "gte": "1654056000000",
        "lt": "1654056005000"
      }
    }
  }
}
```
### session_ id相同，page_id先login再good_detail的文档
```json
GET /gmall/_eql/search
{
  "query": """
      sequence by page.session_ id
        [page where page.page_id=="login"]
        [page where page.page_id=="good_detail"]
  """
}
```
# SQL语法
## SQL测试数据
```
PUT my-sql-index
```
```
PUT my-sql-index/_bulk?refresh
{"index":{"_id": "JAVA"}}
{"name": "JAVA", "author": "zhangsan", "release_date": "2022-05-01", "page_count": 561}
{"index":{"_id": "BIGDATA"}}
{"name": "BIGDATA", "author": "lisi", "release_date": "2022-05-02", "page_count": 482}
{"index":{"_id": "SCALA"}}
{"name": "SCALA", "author": "wangwu", "release date": "2022-05=03", "page_count": 604}
```
## 查询
### 查询某表数据
format可选结果格式：txt, json, smile, yaml, csv, tsv
```
POST _sql?format=txt
{
  "query": """
  SELECT * FROM "my-sql-index" WHERE page_count > 500
  """
}
```
### SQL转DSL语句 `translate`
```
POST _sql/translate
{
  "query": """
  SELECT * FROM "my-sql-index" WHERE page_count > 500
  """
}
```
### SQL和DSL语句混合使用
```
POST _sql?format=json
{
  "query": """SELECT * FROM "my-sql-index" """,
  "filter" : {
    "range" : {
      "page_count" : {
        "from" : 500,
        "to" : 600,
        "include_lower" : false,
        "include_upper" : false,
        "boost" : 1.0
      }
    }
  }
}
```
### 查询索引列表
```
POST _sql?format=txt
{
  "query": """
  show tables like 'my%'
  """
}
```
### 查询索引结构
```
POST _sql?format=txt
{
  "query": """
  describe "my-sql-index"
  """
}
```
### 游标 `cursor`
```
POST _sql?format=json
{
  "query": """
  SELECT * FROM "my-sql-index" WHERE page_count > 100
  """,
  "fetch_size": 2
}
```
一共3条数据 查询其中两条 结果中cursor字段表示剩余数据的缓存 再次调用查询后续数据
```
POST _sql?format=json
{
  "cursor" : "o5GwAwFaAXN4RkdsdVkyeDFaR1ZmWTI5dWRHVjRkRjkxZFdsa0RYRjFaWEo1UVc1a1JtVjBZMmdCRkhjNFVGQlZORmxDTlVablYwbGlWSGhpTFhRM0FBQUFBQUFUVDdNV1oyRXdabUpIVUdSUlJXMHphbmRWVFdaMVh6RnBkdz09/////w8FAWYGYXV0aG9yAQZhdXRob3IBBHRleHQAAAABZgRuYW1lAQRuYW1lAQR0ZXh0AAAAAWYKcGFnZV9jb3VudAEKcGFnZV9jb3VudAEEbG9uZwAAAAFmDHJlbGVhc2UgZGF0ZQEMcmVsZWFzZSBkYXRlAQR0ZXh0AAAAAWYMcmVsZWFzZV9kYXRlAQxyZWxlYXNlX2RhdGUBCGRhdGV0aW1lAQAAAR8="
}
```
如果执行后无结果返回，说明游标数据已处理完毕
### 基础查询操作
- 同mysql
### 聚合函数操作
- 同mysql（Min, Max, Avg, Sum, Count, Distinct）
### 类型转换 `::`
字符串123转为long类型，可以计算
```
POST _sql?format=txt
{
  "query": """
  SELECT '123' :: long AS a
  """
}
```
### 模糊查询
- like同mysql
- rlike：正则表达式
### 聚合分析函数
- 取第一个：first(查询字段， 排序的字段)/first_value(查询字段， 排序的字段)
- 取最后一个：last(查询字段， 排序的字段)/last_value(查询字段， 排序的字段)
```
POST _sql?format=txt
{
  "query": """
  SELECT first(name, release_date) from "my-sql-index"
  """
}

```
- 量化字段的峰值分布
```
POST _sql?format=txt
{
  "query": """
  SELECT KURTOSIS(page_count) from "my-sql-index"
  """
}
```
- 直方矩阵
```
POST _sql?format=txt
{
  "query": """
  SELECT HISTOGRAM(page_count, 10) as c, COUNT(*) FROM "my-sql-index" GROUP BY c
  """
}
```