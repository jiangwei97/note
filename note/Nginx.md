# 安装
## centos
https://www.cnblogs.com/boonya/p/7907999.html
## ubuntu
https://blog.csdn.net/bb159632478/article/details/109450944
# 配置文件
```nginx

#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen 443 ssl;
        server_name jwtool.cn;
        root /data/code/lychee/lychee-web/dist;
	ssl_certificate jwtool.cn_bundle.crt;
    	ssl_certificate_key jwtool.cn.key;
    	ssl_session_timeout 10m;
    	ssl_protocols TLSv1.2 TLSv1.3;
    	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;
	ssl_prefer_server_ciphers on;


        location / {
            try_files $uri $uri/ /index.html;
        }

        location /api {
            proxy_pass http://localhost:8081;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

    }
    server {
	listen 80;
	server_name jwtool.cn;
	return 301 https://$host$request_uri;
    }



    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


    # HTTPS server
    #
    #server {
    #    listen       443 ssl;
    #    server_name  localhost;

    #    ssl_certificate      cert.pem;
    #    ssl_certificate_key  cert.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}

}

```
# 命令
进入 `nginx/sbin` 目录
## 启动 nginx 命令
```bash
./nginx
```
## 启动 nginx 命令
```bash
./nginx
```
## 查看 nginx 的状态
```bash
# 出现master 则启动成功
ps -ef | grep nginx
```
## 停止 nginx 命令
```bash
./nginx -s stop 
```
## 重启 nginx 命令
```bash
./nginx -s reload
```
## windows 强制终止运行中的 nginx.exe 进程及其所有子进程
```bash
taskkill /f /t /im nginx.exe
```