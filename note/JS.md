# 字符串
### 字符串替换replace
```javascript
let reg=new RegExp("终古","g") //创建正则RegExp对象   
let str="终古人民共和国，终古人民"  
let var newstr=str.replace(reg,"中国")
```
### 判断类型是否是字符串
```javascript
if(typeof(item) === 'string'){
    console.log('是')
}
```
### 获得当前url的参数
```javascript
getParameterByName(name) {
  const url = window.location.href
  name = name.replace(/[\[\]]/g, '\\$&')
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}
```
# 日期
### 字符串转Date
```javascript
   let date = new Date(dataStr)
```
### Date转字符串
```javascript
timeToString(date) {
    let y = date.getFullYear()
    let m = date.getMonth() + 1
    m = m < 10 ? ('0' + m) : m
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    let h = date.getHours()
    h=h < 10 ? ('0' + h) : h
    let minute = date.getMinutes()
    minute = minute < 10 ? ('0' + minute) : minute
    let second=date.getSeconds()
    second=second < 10 ? ('0' + second) : second
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second
}
```
### 日期+1天：
```javascript
let date = new Date()
date = date.setDate(date.getDate()+1);
date = new Date(date)
```
# 数组
### 在数组结束位置添加元素：push()
```javascript
var arr = [2,3,4]
arr.push(5,'hello',[6,7,8]); // 可以是数字、字符、数组
console.log(arr); // [ 2, 3, 4, 5, 'hello', [ 6, 7, 8 ] ]
```
### 在数组起始位置添加元素：unshift()
```javascript
var arr = [3,4,5];
arr.unshift('hello',1,[6,8]); // 参数可以是数字、字符、数组
console.log(arr); // [ 'hello', 1, [ 6, 8 ], 3, 4, 5 ]
```
### 在数组任意位置添加元素：splice()
```javascript
// 通过数组名.splice()方法来实现，该方法的返回值是被删除的数组，改变了原来的数组
var arr = [3,4,5];
// 第一个参数表示从第2位开始；
// 第二个参数表示删除0个；
// 第三个参数开始为要添加的元素，可以是数组
arr.splice(2,0,'hello',[6,7,8]); 
console.log(arr); // [ 3, 4, 'hello', [ 6, 7, 8 ], 5 ]
```
### 合并数组添加元素：concat()
```javascript
// concat()方法的参数可以是数组，也可以是数字、字符。
// 如果是数组的话，就将两个数组合并；如果是数字、字符的话，就在原数组末尾直接添加。
// 返回一个新的数组，不会改变原来的数组。
var arr = [2,3];
var res = arr.concat(3,[2,3],[2,[3,4]]);
console.log(res); // [ 2, 3, 3, 2, 3, 2, [ 3, 4 ] ]
```
### 合并数组添加元素：concat()
```javascript
let result = {}
let str = 'a=1,b=2,c=3'
let partitions = str.split(',')
for (let a of partitions) {
result[a.split('=')[0]] = a.split('=')[1]
}
console.log(result) // {a: '1', b: '2', c: '3'}
```
### 数组删除指定元素
```javascript
var arr = ['a','b','c','d'],
value = 'b'
arr = arr.filter(item => item != value)
// arr => ['a','c','d']
```
```javascript
let value = 'b',
arr = ['a','b','c','d']
arr.splice(arr.indexOf(value), 1) // ['b']
// arr => ['a','c','d']
```
### 数组转map
```javascript
// 使用 reduce 方法将对象数组转换为 Map
const objMap = objArray.reduce((map, obj) => {
    map.set(obj.id, obj);
    return map;
}, new Map());

console.log(objMap);
```
# 下载
### 字符串写入文件并下载 file-saver
```javascript
npm install file-saver --save
import { saveAs } from 'file-saver'
```
#### 下载txt文件
```javascript
var blob = new Blob(
    ["Hello, world!"], 
    {type: "text/plain;charset=utf-8"}
);
saveAs(blob, "hello world.txt")
```
#### 下载csv文件
```javascript
// 其中加入exportContent = "\uFEFF";可以防止中文乱码
var exportContent = "\uFEFF";
var blob = new Blob(
    [exportContent + str], 
    {type: "text/plain;charset=utf-8"}
);
saveAs(blob, downloadName + ".csv");
```
#### 下载excel文件
```javascript
// 其中加入exportContent = "\uFEFF";可以防止中文乱码
var exportContent = "\uFEFF";
var blob = new Blob(
    [exportContent + str], {
    type: "application/vnd.ms-excel;charset=charset=utf-8"
});
saveAs(blob, downloadName + ".xls");
```
#### 下载接口的我文件
```javascript
function downloadTemplate() {
  const that = this
  that.confirmLoading = true
  that.loading = true
  let httpUrl = this.url.downloadTemplate
  let fileName = 'xxx.xlsx'
  downFile(httpUrl, '', 30000).then(data => {
    if (!data || data.size === 0) {
      this.$message.error('failed to download')
      return
    }
    if (typeof window.navigator.msSaveBlob !== 'undefined') {
      window.navigator.msSaveBlob(new Blob([data]), fileName)
    } else {
      let url = window.URL.createObjectURL(new Blob([data]))
      let link = document.createElement('a')
      link.style.display = 'none'
      link.href = url
      link.setAttribute('download', fileName)
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link) //下载完成移除元素
      window.URL.revokeObjectURL(url) //释放掉blob对象
    }
  }).finally(() => {
    that.confirmLoading = false
    that.loading = false
  })
}
```
### 预览
```vue
<template>
  <div>
    <select v-model="selectedFile" @change="previewFile">
      <option v-for="(file, index) in files" :key="index" :value="index">{{ file.name }}</option>
    </select>
    <iframe ref="previewFrame" width="100%" height="500"></iframe>
  </div>
</template>

<script>
import JSZip from 'jszip';
import mammoth from 'mammoth';

export default {
  data() {
    return {
      zipFile: null,
      files: [],
      selectedFile: 0
    };
  },
  methods: {
    async handleDownload() {
      const response = await fetch('your_api_endpoint');
      const blob = await response.blob();

      // 使用JSZip解压缩zip文件
      const zip = new JSZip();
      this.zipFile = await zip.loadAsync(blob);

      // 获取所有.docx文件并保存到files数组中
      this.files = [];
      this.zipFile.forEach((relativePath, file) => {
        if (relativePath.endsWith('.docx')) {
          this.files.push(file);
        }
      });
    },
    async previewFile() {
      const file = this.files[this.selectedFile];
      const arrayBuffer = await file.async('arraybuffer');

      // 使用mammoth将.docx文件转换为HTML
      const result = await mammoth.convertToHtml({ arrayBuffer });

      // 使用ref获取iframe元素引用，并操作iframe内容
      this.$refs.previewFrame.contentWindow.document.open();
      this.$refs.previewFrame.contentWindow.document.write(result.value);
      this.$refs.previewFrame.contentWindow.document.close();
    }
  },
  mounted() {
    this.handleDownload(); // 在组件挂载时触发下载和解析
  }
};
</script>

```
# 轮询
```javascript
data() {
    return {
        result: null
    }
},
methods: {
    loadData(){
        // 每过3000ms执行一次fun()
        this.result = setInterval(() => this.fun(), 3000)
    },
    fun(){
        if (flag) { // 停止轮询条件 boolean类型
            // 停止轮询
            clearInterval(this.result)
        }
    }
}
```