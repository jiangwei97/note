 # 语法
 #### 基本指令
 - 查看所有数据库

    ```
    show dbs
    ```
 - 进入指定数据库
    ```
    use {databaseName}
    ```
 - 查看当前所处数据库
    ```
    db
    ```
 - 查看所有集合
    ```
    show collections
    ```
#### 写
 - create 插入文档

    ```
    db.{collectionName}.insert({doc})
    db.{collectionName}.insertOne({doc})
    db.{collectionName}.insertMany({docArr})
    -- 插入一条example
    db.users.insert({"name": "zhangsan", "age": 20, "region": "china"})
    -- 插入多条example
    db.users.insert([
        {"name": "zhangsan", "age": 20, "region": "shanghai"}, 
        {"name": "lisi", "age": 21, "region": "chongqing"}, 
        {"name": "wangwu", "age": 22, "region": "beijing"}
    ])
    ```

 - update 修改文档
    ```
    -- 修改（默认只改第一条）
    db.{collectionName}.update();
    -- 修改一条
    db.{collectionName}.updateOne();
    -- 修改多条
    db.{collectionName}.updateMany()
    -- 操作符
    $set 修改文档的指定属性
    $unset 删除文档的指定属性
    ```
    ```
    -- 将users集合中name=zhangsan的文档的第一条替换为{"age": 30}
    db.users.update({"name":"zhangsan"}, {"age": 30});
    -- 将users集合中name=zhangsan的文档的第一条的age属性改为30
    db.users.update(
        {"name":"zhangsan"}, 
        {$set: {"age": 30}}
    );
    -- 将users集合中name=zhangsan的文档的第一条的region属性删除
    db.users.update(
        {"name":"zhangsan"}, 
        {$unset: {"region": ""}}
    );
    -- 将users集合中name=zhangsan的全部文档的age属性改为30
    db.users.update(
        {"name":"zhangsan"}, 
        {
            $set: {"age": 30}
        },
        {multi: true}
    );

    db.users.updateMany(
        {"name":"zhangsan"}, 
        {$set: {"age": 30}}
    );
    ```

 - delete 删除文档
    ```
    -- 删除（默认删除多个）
    db.{collectionName}.remove();
    -- 删除一条
    db.{collectionName}.deleteOne();
    -- 删除多条
    db.{collectionName}.deleteMany()
    -- 删除集合
    db.{collectionName}.drop()
    ```
#### 读
 - read 查询文档

    ```
    -- 查找集合中所有文档
    db.{collectionName}.find();
    -- 查找集合中符合条件的第一个文档
    db.{collectionName}.findOne();
    -- 查找集合中符合条件的第一个文档的字段的值
    db.{collectionName}.findOne().{字段名};
    ```
    ```
    -- example
    -- 查找users集合中name=zhangsan并且age=20的文档列表
    db.users.find({"name":"zhangsan", "age": 20});
    -- 查找users集合中name=zhangsan的第一个文档的region的值
    db.users.findOne({"name":"zhangsan"}).region;
    -- 查找users集合中name=zhangsan的文档列表的总条数
    db.users.find({"name":"zhangsan"}).count();
    ```
 - 投影
    ```
    -- 查找users集合中region=china的文档的name列表
    db.users.find({"region":"china"}, {"name": 1, "_id": 0});
    ```
 - 条数
    ```
    -- 查找集合的总条数
    db.{collectionName}.find().count();
    ```
 - 排序、分页
    ```
    -- 根据age升序排序
    db.{collectionName}.find().sort({"age": 1});
    -- 根据age降序排序
    db.{collectionName}.find().sort({"age": -1});
    -- 先根据age升序排序, 再根据name降序排序
    db.{collectionName}.find().sort({"age": 1, "name": -1});
    -- 显示前10条数据
    db.{collectionName}.find().limit(10);
    -- 跳过前50条数据（默认0）
    db.{collectionName}.find().skip(50);
    -- 显示集合中51-60的数据
    db.{collectionName}.find().limit(10).skip(50);
    
    ```
# Demo
#### pom.xml
```xml
<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-mongodb -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-mongodb</artifactId>
    <version>2.7.3</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.springframework.data/spring-data-mongodb -->
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-mongodb</artifactId>
    <version>3.1.1</version>
</dependency>
```
#### Interface
```java
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mongodb.client.result.UpdateResult;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2022/9/24 15:36
 */

public interface IMongoService {
    /**
     * 保存一个对象到mongodb
     */
    <T> T save(T t);

    /**
     * 根据id删除对象
     */
    <T> void deleteById(T t);

    /**
     * 删除所有
     */
    <T> void removeAll(Query query, Class<T> entityClass);

    /**
     * 通过条件查询更新数据
     */
    <T> UpdateResult update(Query query, Update update, Class<T> entityClass);

    /**
     * 更新第一条满足条件的文档
     */
    <T> UpdateResult updateFirst(Query query, Update update, Class<T> entityClass);

    /**
     * 新增或插入
     */
    <T> UpdateResult upsert(Query query, Update update, Class<T> entityClass);

    /**
     * 构建update
     */
    <T> Update buildBaseUpdate(T t);

    /**
     * 根据id进行更新
     */
    <T> UpdateResult updateById(String id, T t, Class<T> entityClass);

    /**
     * 根据id进行更新
     */
    <T> UpdateResult updateById(Integer id, T t, Class<T> entityClass);

    <T> UpdateResult updateById(Integer id, Update update, Class<T> entityClass);

    /**
     * 通过条件查询实体(集合)
     */
    <T> List<T> find(Query query, Class<T> entityClass);

    /**
     * 通过主键查询实体
     */
    <T> T findById(Integer id, Class<T> entityClass);

    <T> T findById(String id, Class<T> entityClass);

    <T> List<T> findByIdIn(Iterable<Integer> ids, Class<T> entityClass);

    /**
     * 通过一定的条件查询一个实体
     */
    <T> T findOne(Query query, Class<T> entityClass);

    /**
     * 通过条件查询实体(集合)
     */
    <T> List<T> find(Query query, Class<T> entityClass, String... excludeFields);

    /**
     * 通过条件查询实体
     */
    <T> T findOne(Query query, Class<T> entityClass, String... excludeFields);

    /**
     * 总记录数
     */
    <T> long count(Query query, Class<T> entityClass);

    /**
     * 获取分页数据
     */
    <T> Page<T> pageInfo(int currentPage, int pageSize, Query query, Class<T> entityClass);

    /**
     * 聚合查询
     */
    <T> AggregationResults<T> aggregate(Aggregation aggregation, Class<?> inputType, Class<T> outputType);

    /**
     * 批量插入
     */
    <T> void insertAll(List<T> list);

    /**
     * 批量更新
     */
    <T> UpdateResult updateMulti(Query query, Update update, Class<T> entityClass);

    /**
     * 去重查询
     */
    <T> List<T> queryForDistinct(String key, Object value, Query query, Class<T> entityClass);
}
```
#### Impl
```java
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiangwei.service.IMongoService;
import com.mongodb.BasicDBObject;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.result.UpdateResult;
import lombok.extern.slf4j.Slf4j;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2022/9/24 15:38
 */

@Slf4j
@Service
public class MongoService implements IMongoService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public <T> T save(T t) {
        mongoTemplate.save(t);
        return t;
    }

    @Override
    public <T> void deleteById(T t) {
        mongoTemplate.remove(t);
    }

    @Override
    public <T> void removeAll(Query query, Class<T> entityClass) {
        mongoTemplate.findAllAndRemove(query, entityClass);
    }

    @Override
    public <T> UpdateResult update(Query query, Update update, Class<T> entityClass) {
        return mongoTemplate.updateMulti(query, update, entityClass);
    }

    @Override
    public <T> UpdateResult updateFirst(Query query, Update update, Class<T> entityClass) {
        return mongoTemplate.updateFirst(query, update, entityClass);
    }

    @Override
    public <T> UpdateResult upsert(Query query, Update update, Class<T> entityClass) {
        return mongoTemplate.upsert(query, update, entityClass);
    }

    @Override
    public <T> UpdateResult updateById(String id, T t, Class<T> entityClass) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = this.buildBaseUpdate(t);
        return update(query, update, entityClass);
    }

    @Override
    public <T> UpdateResult updateById(Integer id, T t, Class<T> entityClass) {
        return this.updateById(String.valueOf(id), t, entityClass);
    }

    @Override
    public <T> UpdateResult updateById(Integer id, Update update, Class<T> entityClass) {
        Query query = new Query(Criteria.where("_id").is(String.valueOf(id)));
        return this.updateFirst(query, update, entityClass);
    }

    /**
     * 根据vo构建构建更新条件Update
     */
    @Override
    public <T> Update buildBaseUpdate(T t) {
        Update update = new Update();

        Field[] fields = t.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(t);
                if (value != null) {
                    update.set(field.getName(), value);
                }
            } catch (Exception e) {
                log.error("异常信息：{}", e.getMessage());
            }
        }

        return update;
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass) {
        return mongoTemplate.find(query, entityClass);
    }

    @Override
    public <T> T findById(Integer id, Class<T> entityClass) {
        return this.findById(String.valueOf(id), entityClass);
    }

    @Override
    public <T> T findById(String id, Class<T> entityClass) {
        return mongoTemplate.findById(id, entityClass);
    }

    @Override
    public <T> List<T> findByIdIn(Iterable<Integer> ids, Class<T> entityClass) {
        if(ids != null){
            List<String> strIds = new ArrayList<>();
            ids.forEach(id -> {
                if(id != null){
                    strIds.add(String.valueOf(id));
                }
            });
            return mongoTemplate.find(new Query(Criteria.where("_id").in(strIds)), entityClass);
        }
        return Collections.emptyList();
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass) {
        return mongoTemplate.findOne(query, entityClass);
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass, String... excludeFields) {
        setExcludeFields(query, excludeFields);
        return find(query, entityClass);
    }

    /**
     * 排除MongoDB查询返回的一些字段
     */
    private void setExcludeFields(Query query, String... excludeFields) {
        if (null != query && null != excludeFields) {
            for (String field : excludeFields) {
                query.fields().exclude(field);
            }
        }
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass, String... excludeFields) {
        setExcludeFields(query, excludeFields);
        return findOne(query, entityClass);
    }

    @Override
    public <T> long count(Query query, Class<T> entityClass) {
        return mongoTemplate.count(query, entityClass);
    }

    @Override
    public <T> Page<T> pageInfo(int currentPage, int pageSize, Query query, Class<T> entityClass) {
        if (currentPage == 0) {
            currentPage = 1;
        }
        if (pageSize == 0) {
            pageSize = 10;
        }
        long count = this.count(query, entityClass);
        int skip = (currentPage - 1) * pageSize;
        List<T> list = this.find(query.skip(skip).limit(pageSize), entityClass);
        Page<T> page = new Page<>(currentPage, pageSize, count);
        page.setRecords(list);
        return page;
    }

    @Override
    public <T> AggregationResults<T> aggregate(Aggregation aggregation, Class<?> inputType, Class<T> outputType) {
        return mongoTemplate.aggregate(aggregation, inputType, outputType);
    }

    @Override
    public <T> void insertAll(List<T> tList) {
        mongoTemplate.insertAll(tList);
    }

    @Override
    public <T> UpdateResult updateMulti(Query query, Update update, Class<T> entityClass) {
        return mongoTemplate.updateMulti(query, update, entityClass);
    }

    @Override
    public <T> List<T> queryForDistinct(String key, Object value, Query query, Class<T> entityClass) {
        List<T> result = new ArrayList<>();
        Bson bson = new BasicDBObject(key, value);
        DistinctIterable<T> distinctIterable = mongoTemplate.getCollection(
                mongoTemplate.getCollectionName(entityClass)).distinct(key, bson, entityClass);
        T first = distinctIterable.first();
        result.add(first);
        distinctIterable.forEach(result::add);

        return result;
    }

}
```
#### Entity
```java
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author jiangwei jiangwei97@aliyun.om
 * @since 2022/9/24 15:13
 */

@Data
@Document
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    @Id
    private String id;

    @Indexed
    private Integer userId;

    private String name;

    private String region;

    private Integer deleted;

    // 不需要被mongobd存储的字段可以加@Transient标识
    @Transient
    private String type;
}
```