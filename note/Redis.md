# 命令
- https://blog.csdn.net/wwzzzzzzzzzzzzz/article/details/127217156
# Jedis
## pom.xml
```xml
<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-redis -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <version>3.0.6</version>
</dependency>
<!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.9.0</version>
</dependency>
```
## RedisPoolUtil
```java
import com.jiangwei.common.configuation.RedisConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2023-03-08 11:43
 **/

@Component
@Slf4j
public class RedisPoolUtil {

    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private final static boolean TEST_ON_BORROW = true;

    //在return给pool时，是否提前进行validate操作；
    private final static boolean TEST_ON_RETURN = true;

    private static JedisPool jedisPool = null;

    /**
     * redis过期时间,以秒为单位
     */
    public final static int EXRP_HOUR = 60 * 60; //一小时
    public final static int EXRP_DAY = 60 * 60 * 24; //一天
    public final static int EXRP_MONTH = 60 * 60 * 24 * 30; //一个月

    @Resource
    private RedisConfig redisConfig;

    /**
     * 初始化Redis连接池
     */
    private void initialPool() {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(redisConfig.getMaxActive());
            config.setMaxIdle(redisConfig.getMaxIdle());
            config.setMaxWaitMillis(redisConfig.getMaxWait());
            config.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool = new JedisPool(
                    config,
                    redisConfig.getHost(),
                    redisConfig.getPort(),
                    redisConfig.getTimeout(),
                    redisConfig.getPassword()
            );
        } catch (Exception e) {
            e.printStackTrace();
            log.error("First create JedisPool error : "+e);
        }
    }

    /**
     * 在多线程环境同步初始化
     */
    private synchronized void poolInit() {
        if (jedisPool == null) {
            initialPool();
        }
    }

    /**
     * 同步获取Jedis实例
     * @return Jedis
     */
    public synchronized Jedis getJedis() {
        if (jedisPool == null) {
            poolInit();
        }
        Jedis jedis = null;
        try {
            if (jedisPool != null) {
                jedis = jedisPool.getResource();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get jedis error : "+e);
        }
        return jedis;
    }

    /**
     * 释放jedis资源
     */
    public static void returnResource(final Jedis jedis) {
        if (jedis != null && jedisPool != null) {
            jedisPool.returnResource(jedis);
        }
    }

}
```
## PipelineUtil
```java
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import java.io.IOException;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2023-05-09 17:41
 **/

public class PipelineUtil {

    private Pipeline p;

    public PipelineUtil(Jedis j) {
        this.p = j.pipelined();
    }

    public Response<String> set(String s, String s1) {
        return this.p.set(s, s1);
    }

    public Response<Long> incrBy(String s, Long s1) {
        return this.p.incrBy(s, s1);
    }

    public Response<Long> incr(String s) {
        return this.p.incr(s);
    }

    public Response<Long> hset(String s, String s1, String s2) {
        return this.p.hset(s, s1, s2);
    }

    public Response<Long> hdel(String s, String s1) {
        return this.p.del(s, s1);
    }

    public Response<Long> sadd(String s, String... ss) {
        return p.sadd(s, ss);
    }

    public Response<Long> scard(String s) {
        return p.scard(s);
    }

    public Response<Long> lpush(String s, String... ss) {
        return p.lpush(s, ss);
    }

    public Response<Long> del(String s) {
        return p.del(s);
    }

    public Response<Long> expire(String s, int i) {
        return p.expire(s, i);
    }

    public Response<Long> srem(String s, String... ss) {
        return p.srem(s, ss);
    }

    public Response<Boolean> sismember(String s, String ss) {
        return p.sismember(s, ss);
    }

    public Response<Long> expireAt(String s, long l) {
        return p.expireAt(s, l);
    }

    public Response<Long> pexpireAt(String s, long millseconds) {
        return p.pexpireAt(s, millseconds);
    }

    public Response<Long> hincrBy(String var1, String var2, long var3) {
        return p.hincrBy(var1, var2, var3);
    }

    public Response<Long> incrBy(String var1, long var2) {
        return p.incrBy(var1, var2);
    }

    public void sync() {
        p.sync();
    }

    public void close() throws IOException {
        p.close();
    }

    public Pipeline getP() {
        return p;
    }

    public void setP(Pipeline p) {
        this.p = p;
    }
}
```
## JedisUtil
```java
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.SortingParams;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2023-05-09 17:40
 **/

@Component
public class JedisUtil {

    private static final String DIST_LOCK_SUCCESS = "OK";

    private static final Long DIST_LOCK_RELEASE_SUCCESS = 1L;

    @Resource
    private RedisPoolUtil redisPoolUtil;

    private synchronized Jedis jedis() {
        return redisPoolUtil.getJedis();
    }

    public PipelineUtil pipelined() {
        return new PipelineUtil(jedis());
    }

    private void close(Jedis jedis){
        if(jedis != null){
            jedis.close();
        }
    }


    public String set(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.set(s, s1);
        } finally {
            close(jedis);
        }
    }

    public String get(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.get(s);
        } finally {
            close(jedis);
        }
    }

    public Boolean exists(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.exists(s);
        } finally {
            close(jedis);
        }
    }

    public Long expire(String s, int i) {
        Jedis jedis = jedis();
        try {
            return jedis.expire(s, i);
        } finally {
            close(jedis);
        }
    }

    public Long pexpire(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.pexpire(s, l);
        } finally {
            close(jedis);
        }
    }

    public Long expireAt(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.expireAt(s, l);
        } finally {
            close(jedis);
        }
    }

    public Long pexpireAt(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.pexpireAt(s, l);
        } finally {
            close(jedis);
        }
    }

    public Long ttl(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.ttl(s);
        } finally {
            close(jedis);
        }
    }

    public Long pttl(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.pttl(s);
        } finally {
            close(jedis);
        }
    }

    public Boolean setbit(String s, long l, boolean b) {
        Jedis jedis = jedis();
        try {
            return jedis.setbit(s, l, b);
        } finally {
            close(jedis);
        }
    }

    public Boolean getbit(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.getbit(s, l);
        } finally {
            close(jedis);
        }
    }

    public Long setrange(String s, long l, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.setrange(s, l, s1);
        } finally {
            close(jedis);
        }
    }

    public String getrange(String s, long l, long l1) {
        Jedis jedis = jedis();
        try {
            return jedis.getrange(s, l, l1);
        } finally {
            close(jedis);
        }
    }

    public String getSet(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.getSet(s, s1);
        } finally {
            close(jedis);
        }
    }

    public Long setnx(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.setnx(s, s1);
        } finally {
            close(jedis);
        }
    }

    public String setex(String s, int i, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.setex(s, i, s1);
        } finally {
            close(jedis);
        }
    }

    public String psetex(String s, long l, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.psetex(s, l, s1);
        } finally {
            close(jedis);
        }
    }

    public Long decrBy(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.decrBy(s, l);
        } finally {
            close(jedis);
        }
    }

    public Long decr(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.decr(s);
        } finally {
            close(jedis);
        }
    }

    public Long incrBy(String s, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.incrBy(s, l);
        } finally {
            close(jedis);
        }
    }

    public Double incrByFloat(String s, double v) {
        Jedis jedis = jedis();
        try {
            return jedis.incrByFloat(s, v);
        } finally {
            close(jedis);
        }
    }

    public Long hincrByValue(String s, String s1, long s2) {
        Jedis jedis = jedis();
        try {
            return jedis.hincrBy(s, s1, s2);
        } finally {
            close(jedis);
        }
    }

    public Double hincrByFloat(String s, String s1, long s2) {
        Jedis jedis = jedis();
        try {
            return jedis.hincrByFloat(s, s1, s2);
        } finally {
            close(jedis);
        }
    }

    public Long incr(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.incr(s);
        } finally {
            close(jedis);
        }
    }

    public Long append(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.append(s, s1);
        } finally {
            close(jedis);
        }
    }

    public String substr(String s, int i, int i1) {
        Jedis jedis = jedis();
        try {
            return jedis.substr(s, i, i1);
        } finally {
            close(jedis);
        }
    }

    // 通过key给field设置指定的值,如果key不存在,则先创建
    public Long hset(String s, String s1, String s2) {
        Jedis jedis = jedis();
        try {
            return jedis.hset(s, s1, s2);
        } finally {
            close(jedis);
        }
    }

    //通过key 和 field 获取指定的 value
    public String hget(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.hget(s, s1);
        } finally {
            close(jedis);
        }
    }

    public Long hsetnx(String s, String s1, String s2) {
        Jedis jedis = jedis();
        try {
            return jedis.hsetnx(s, s1, s2);
        } finally {
            close(jedis);
        }
    }

    public String hmset(String s, Map<String, String> map) {
        Jedis jedis = jedis();
        try {
            return jedis.hmset(s, map);
        } finally {
            close(jedis);
        }
    }

    public List<String> hmget(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.hmget(s, strings);
        } finally {
            close(jedis);
        }
    }

    public Long hincrBy(String s, String s1, long l) {
        Jedis jedis = jedis();
        try {
            return jedis.hincrBy(s, s1, l);
        } finally {
            close(jedis);
        }
    }

    public Double hincrByFloat(String s, String s1, double v) {
        Jedis jedis = jedis();
        try {
            return jedis.hincrByFloat(s, s1, v);
        } finally {
            close(jedis);
        }
    }

    //通过key和field判断是否有指定的value存在
    public Boolean hexists(String s, String s1) {
        Jedis jedis = jedis();
        try {
            return jedis.hexists(s, s1);
        } finally {
            close(jedis);
        }
    }

    //通过key 删除指定的 field
    public Long hdel(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.hdel(s, strings);
        } finally {
            close(jedis);
        }
    }

    public Long hlen(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.hlen(s);
        } finally {
            close(jedis);
        }
    }

    public Set<String> hkeys(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.hkeys(s);
        } finally {
            close(jedis);
        }
    }

    public List<String> hvals(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.hvals(s);
        } finally {
            close(jedis);
        }
    }


    public Map<String, String> hgetAll(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.hgetAll(s);
        } finally {
            close(jedis);
        }
    }

    public String rpop(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.rpop(s);
        } finally {
            close(jedis);
        }
    }

    public Long scard(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.scard(s);
        } finally {
            close(jedis);
        }
    }

    public Long sadd(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.sadd(s, strings);
        } finally {
            close(jedis);
        }
    }

    public Boolean sismember(String key, String member) {
        Jedis jedis = jedis();
        try {
            return jedis.sismember(key, member);
        } finally {
            close(jedis);
        }
    }

    //移除
    public Long smove(String s, String s1, String s2) {
        Jedis jedis = jedis();
        try {
            return jedis.smove(s, s1, s2);
        } finally {
            close(jedis);
        }
    }

    //交集
    public Long sinterstore(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.sinterstore(s, strings);
        } finally {
            close(jedis);
        }
    }

    //差集
    public Long sdiffstore(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.sdiffstore(s, strings);
        } finally {
            close(jedis);
        }
    }

    public Long lpush(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.lpush(s, strings);
        } finally {
            close(jedis);
        }
    }

    public Set<String> sinter(String... s) {
        Jedis jedis = jedis();
        try {
            return jedis.sinter(s);
        } finally {
            close(jedis);
        }
    }

    //通过key获取set中所有的value
    public Set<String> smembers(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.smembers(s);
        } finally {
            close(jedis);
        }
    }


    public Long srem(String s, String... strings) {
        Jedis jedis = jedis();
        try {
            return jedis.srem(s, strings);
        } finally {
            close(jedis);
        }
    }

    public List<String> sort(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.sort(s);
        } finally {
            close(jedis);
        }
    }

    public List<String> sort(String s, SortingParams sortingParams) {
        Jedis jedis = jedis();
        try {
            return jedis.sort(s, sortingParams);
        } finally {
            close(jedis);
        }
    }

    //删除指定的key,也可以传入一个包含key的数组
    public Long del(String s) {
        Jedis jedis = jedis();
        try {
            return jedis.del(s);
        } finally {
            close(jedis);
        }
    }

    public Long zadd(String key, double score, String value) {
        Jedis jedis = jedis();
        try {
            return jedis.zadd(key, score, value);
        } finally {
            close(jedis);
        }
    }

    public Double zincrby(String key, double score, String member) {
        Jedis jedis = jedis();
        try {
            return jedis.zincrby(key, score, member);
        } finally {
            close(jedis);
        }
    }

    public Double zscore(String key, String member) {
        Jedis jedis = jedis();
        try {
            return jedis.zscore(key, member);
        } finally {
            close(jedis);
        }
    }

    public Object eval(String script){
        Jedis jedis = jedis();
        try {
            return jedis.eval(script);
        } finally {
            close(jedis);
        }
    }

    public Object eval(String script, List<String> keys, List<String> args){
        Jedis jedis = jedis();
        try {
            return jedis.eval(script, keys, args);
        } finally {
            close(jedis);
        }
    }

    public boolean lock(String uniqueMutex, String holderId, long expireTime) {
        try (Jedis jedis = jedis()) {
            String script = "return redis.call('SET', KEYS[1], ARGV[1], 'PX', ARGV[2], 'NX')";
            Object result = jedis.eval(
                    script,
                    Collections.singletonList(uniqueMutex),
                    Lists.newArrayList(holderId, String.valueOf(expireTime))
            );
            return DIST_LOCK_SUCCESS.equals(result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("lock throws {}", e.getMessage());
        }
        return false;
    }

    public boolean tryLock(String uniqueMutex, String holderId, Long tryTimeout, long expireTime) {
        long startTime = System.currentTimeMillis();
        long endTimeOut = startTime + tryTimeout;
        while (endTimeOut > startTime) {
            if (this.lock(uniqueMutex, holderId, expireTime)) {
                return true;
            }
            LockSupport.parkNanos(1000 * 1000 * 1000);
            startTime = System.currentTimeMillis();
        }
        return false;
    }

    public boolean unlock(String uniqueMutex, String holderId) {
        try (Jedis jedis = jedis()) {
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Object result = jedis.eval(script, Collections.singletonList(uniqueMutex), Collections.singletonList(holderId));
            return DIST_LOCK_RELEASE_SUCCESS.equals(result);
        } catch (Exception e) {
            log.error("unlock throws {}", e.getMessage());
        }
        return false;
    }

}
```
## 使用
```java
import com.google.common.collect.Lists;
import com.jiangwei.common.util.JedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Disabled("Skip when build")
@SpringBootTest
@Slf4j
class RedisTest {

    @Resource
    JedisUtil jedisUtil;

    @Test
    void redisTest1() {
        System.out.println(LocalDateTime.now());
        jedisUtil.set("aaa", "hello world jedisUtil " + LocalDateTime.now());
        System.out.println(jedisUtil.get("aaa"));
        System.out.println(LocalDateTime.now());
    }

    @Test
    void redisTest2() {
        // lua脚本
        System.out.println(LocalDateTime.now());
        ArrayList<String> args = Lists.newArrayList("5", "3");
        ArrayList<String> keys = Lists.newArrayList("testLua");
        String script = " local a = tonumber(ARGV[1]) " +
                " local b = tonumber(ARGV[2]) " +
                " if a >= b then " +
                "   redis.call('set', KEYS[1], a+b) " +
                "   return redis.call('get', KEYS[1]) " +
                " end " +
                " return 0 ";
        Object eval = jedisUtil.eval(script, keys, args);
        System.out.println(eval);
        System.out.println(LocalDateTime.now());
    }

}
```
# RedisTemplate
## pom.xml
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```
## RedisUtil
```java
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author : jiangwei97@aliyun.com
 * @since : 2022-12-14 17:12
 **/

@Component
public class RedisUtil {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 给一个指定的 key 值附加过期时间
     *
     */
    public boolean expire(String key, long time) {
        return Boolean.TRUE.equals(redisTemplate.expire(key, time, TimeUnit.SECONDS));
    }
    /**
     * 根据key 获取过期时间
     *
     */
    public Long getTime(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }
    /**
     * 根据key 获取过期时间
     *
     */
    public boolean hasKey(String key) {
        return Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }
    /**
     * 移除指定key 的过期时间
     *
     */
    public boolean persist(String key) {
        return Boolean.TRUE.equals(redisTemplate.boundValueOps(key).persist());
    }

    //- - - - - - - - - - - - - - - - - - - - -  String类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 根据key获取值
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 将值放入缓存
     *
     * @param key   键
     * @param value 值
     */
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 将值放入缓存并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) -1为无期限
     */
    public void set(String key, String value, long time) {
        if (time > 0) {
            redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
        } else {
            redisTemplate.opsForValue().set(key, value);
        }
    }

    /**
     * 批量添加 key (重复的键会覆盖)
     *
     */
    public void batchSet(Map<String, String> keyAndValue) {
        redisTemplate.opsForValue().multiSet(keyAndValue);
    }

    /**
     * 批量添加 key-value 只有在键不存在时,才添加
     * map 中只要有一个key存在,则全部不添加
     *
     */
    public void batchSetIfAbsent(Map<String, String> keyAndValue) {
        redisTemplate.opsForValue().multiSetIfAbsent(keyAndValue);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是长整型 ,将报错
     *
     */
    public Long increment(String key, long number) {
        return redisTemplate.opsForValue().increment(key, number);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是 纯数字 ,将报错
     *
     */
    public Double increment(String key, double number) {
        return redisTemplate.opsForValue().increment(key, number);
    }

    //- - - - - - - - - - - - - - - - - - - - -  set类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 将数据放入set缓存
     *
     * @param key 键
     */
    public void sSet(String key, String value) {
        redisTemplate.opsForSet().add(key, value);
    }

    /**
     * 获取变量中的值
     *
     * @param key 键
     */
    public Set<Object> members(String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 随机获取变量中指定个数的元素
     *
     */
    public void randomMembers(String key, long count) {
        redisTemplate.opsForSet().randomMembers(key, count);
    }

    /**
     * 随机获取变量中的元素
     *
     */
    public Object randomMember(String key) {
        return redisTemplate.opsForSet().randomMember(key);
    }

    /**
     * 弹出变量中的元素
     *
     */
    public Object pop(String key) {
        return redisTemplate.opsForSet().pop("setValue");
    }

    /**
     * 获取变量中值的长度
     *
     */
    public Long size(String key) {
        return redisTemplate.opsForSet().size(key);
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public Boolean sHasKey(String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 检查给定的元素是否在变量中。
     *
     * @param key 键
     * @param obj 元素对象
     */
    public Boolean isMember(String key, Object obj) {
        return redisTemplate.opsForSet().isMember(key, obj);
    }

    /**
     * 转移变量的元素值到目的变量。
     *
     * @param key     键
     * @param value   元素对象
     * @param destKey 元素对象
     */
    public Boolean move(String key, String value, String destKey) {
        return redisTemplate.opsForSet().move(key, value, destKey);
    }

    /**
     * 批量移除set缓存中元素
     *
     * @param key    键
     * @param values 值
     */
    public void remove(String key, Object... values) {
        redisTemplate.opsForSet().remove(key, values);
    }

    /**
     * 通过给定的key求2个set变量的差值
     *
     * @param key     键
     * @param destKey 键
     */
    public Set<Object> difference(String key, String destKey) {
        return redisTemplate.opsForSet().difference(key, destKey);
    }


    //- - - - - - - - - - - - - - - - - - - - -  hash类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 加入缓存
     *
     * @param key 键
     * @param map 键
     */
    public void add(String key, Map<String, String> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 获取 key 下的 所有  hashkey 和 value
     *
     * @param key 键
     */
    public Map<Object, Object> getHashEntries(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 验证指定 key 下 有没有指定的 hashkey
     *
     */
    public boolean hashKey(String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 获取指定key的值string
     *
     * @param key  键
     * @param key2 键
     */
    public String getMapString(String key, String key2) {
        return Objects.requireNonNull(redisTemplate.opsForHash().get("map1", "key1")).toString();
    }

    /**
     * 获取指定的值Int
     *
     * @param key  键
     * @param key2 键
     */
    public Integer getMapInt(String key, String key2) {
        return (Integer) redisTemplate.opsForHash().get("map1", "key1");
    }

    /**
     * 弹出元素并删除
     *
     * @param key 键
     */
    public String popValue(String key) {
        return Objects.requireNonNull(redisTemplate.opsForSet().pop(key)).toString();
    }

    /**
     * 删除指定 hash 的 HashKey
     *
     * @return 删除成功的 数量
     */
    public Long delete(String key, String... hashKeys) {
        return redisTemplate.opsForHash().delete(key, (Object) hashKeys);
    }

    /**
     * 给指定 hash 的 hashkey 做增减操作
     *
     */
    public Long increment(String key, String hashKey, long number) {
        return redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 给指定 hash 的 hashkey 做增减操作
     *
     */
    public Double increment(String key, String hashKey, Double number) {
        return redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 获取 key 下的 所有 hashkey 字段
     *
     */
    public Set<Object> hashKeys(String key) {
        return redisTemplate.opsForHash().keys(key);
    }

    /**
     * 获取指定 hash 下面的 键值对 数量
     *
     */
    public Long hashSize(String key) {
        return redisTemplate.opsForHash().size(key);
    }

    //- - - - - - - - - - - - - - - - - - - - -  list类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 在变量左边添加元素值
     *
     */
    public void leftPush(String key, Object value) {
        redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 获取集合指定位置的值。
     *
     */
    public Object index(String key, long index) {
        return redisTemplate.opsForList().index("list", 1);
    }

    /**
     * 获取指定区间的值。
     *
     */
    public List<Object> range(String key, long start, long end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 把最后一个参数值放到指定集合的第一个出现中间参数的前面，
     * 如果中间参数值存在的话。
     *
     */
    public void leftPush(String key, String pivot, String value) {
        redisTemplate.opsForList().leftPush(key, pivot, value);
    }

    /**
     * 向左边批量添加参数元素。
     *
     */
    public void leftPushAll(String key, String... values) {
//        redisTemplate.opsForList().leftPushAll(key,"w","x","y");
        redisTemplate.opsForList().leftPushAll(key, values);
    }

    /**
     * 向集合最右边添加元素。
     *
     */
    public void leftPushAll(String key, String value) {
        redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 向左边批量添加参数元素。
     *
     */
    public void rightPushAll(String key, String... values) {
        //redisTemplate.opsForList().leftPushAll(key,"w","x","y");
        redisTemplate.opsForList().rightPushAll(key, values);
    }

    /**
     * 向已存在的集合中添加元素。
     *
     */
    public void rightPushIfPresent(String key, Object value) {
        redisTemplate.opsForList().rightPushIfPresent(key, value);
    }

    /**
     * 向已存在的集合中添加元素。
     *
     */
    public Long listLength(String key) {
        return redisTemplate.opsForList().size(key);
    }

    /**
     * 移除集合中的左边第一个元素。
     *
     */
    public void leftPop(String key) {
        redisTemplate.opsForList().leftPop(key);
    }

    /**
     * 移除集合中左边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     */
    public void leftPop(String key, long timeout, TimeUnit unit) {
        redisTemplate.opsForList().leftPop(key, timeout, unit);
    }

    /**
     * 移除集合中右边的元素。
     *
     */
    public void rightPop(String key) {
        redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 移除集合中右边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     */
    public void rightPop(String key, long timeout, TimeUnit unit) {
        redisTemplate.opsForList().rightPop(key, timeout, unit);
    }
}
```