# 变量
## 声明和使用
 - 变量名外面的花括号是可选的，加不加都行，加花括号是为了帮助解释器识别变量的边界
```bash
# 声明
your_name="hello world"
# 使用
echo $your_name
echo ${your_name}
```
 - 将 /etc 下目录的文件名循环出来
```bash
for file in `ls /etc`
# 或
for file in $(ls /etc)
```
 - 只读变量
```bash
#!/bin/bash

myUrl="https://www.google.com"
readonly myUrl
myUrl="https://www.baidu.com"
# 运行脚本结果：/bin/sh: NAME: This variable is read only.
```
 - 删除变量
```bash
myUrl="https://www.baidu.com"
unset myUrl
echo $myUrl
# 没有输出
```
## 字符串
 - 拼接
```bash
your_name="zhangsan"
str="Hello, I know you are \"$your_name\"! \n"
echo -e $str
# Hello, I know you are "zhangsan"! 
```
 - 获取字符串长度
```bash
string="abcd"
echo ${#string}   # 输出 4
```
 - 提取子字符串 
```bash
# 从字符串第 2 个字符开始截取 4 个字符
string="runoob is a great site"
echo ${string:1:4} # 输出 unoo
```
 - 查找子字符串
```bash
# 查找字符 i 或 o 的位置(哪个字母先出现就计算哪个)
string="runoob is a great site"
echo `expr index "$string" io`  # 输出 4
```
## 数组
 - 定义
```bash
array_name=(value0 value1 value2 value3)
# 或
array_name[0]=value0
array_name[1]=value1
array_name[n]=valuen
#  可以不使用连续的下标，而且下标的范围没有限制。 
```
 - 读取元素
```bash
valuen=${array_name[n]}
# 使用 @ 符号可以获取数组中的所有元素
echo ${array_name[@]}
```
 - 获取数组的长度 
```bash
# 取得数组元素的个数
length=${#array_name[@]}
# 或者
length=${#array_name[*]}
# 取得数组单个元素的长度
lengthn=${#array_name[n]}
``` 
 - 关联数组
```bash
declare -A array=(["google"]="www.google.com" ["runoob"]="www.runoob.com" ["taobao"]="www.taobao.com")
# 或
declare -A array
array["google"]="www.google.com"
array["runoob"]="www.runoob.com"
array["taobao"]="www.taobao.com"
# 读取
echo ${site["google"]} # www.google.com
```
 - 获取数组的所有键
```bash
${!site[*]}
# 或
${!site[@]}
```
## 传递参数
```bash
#!/bin/bash

echo "Shell 传递参数实例！";
echo "执行的文件名：$0";
echo "第一个参数为：$1";
echo "第二个参数为：$2";
echo "第三个参数为：$3";

# 结果：
# $ chmod +x test.sh 
# $ ./test.sh 1 2 3
# Shell 传递参数实例！
# 执行的文件名：./test.sh
# 第一个参数为：1
# 第二个参数为：2
# 第三个参数为：3
```
 参数     | 说明
---------- | -----
$#  |  传递到脚本的参数个数
$*  | 以一个单字符串显示所有向脚本传递的参数
\$\$  |  脚本运行的当前进程ID号
$!  |  后台运行的最后一个进程的ID号
$@  |  	与$*相同，但是使用时加引号，并在引号中返回每个参数
$-  |  显示Shell使用的当前选项，与set命令功能相同
$?  |  显示最后命令的退出状态。0表示没有错误
# 运算符
 - 算数运算符：+ - * / % = == ！=
 - 关系运算符： -eq -ne -gt -lt -ge -le
 - 布尔运算符: 
    - !：非运算
    - -o：或运算
    - -a：与运算
    - &&：and
    - ||：or
 - 字符串运算符：
    - =：检测两个字符串是否相等，相等返回 true [ $a = $b ] 
    - !=：检测两个字符串是否不相等，不相等返回 true [ $a != $b ] 
    - -z：检测字符串长度是否为，为0返回 true [ -z $a ]
    - -n：检测字符串长度是否不为，不为 0 返回 true [ -n "$a" ] 
    - $：检测字符串是否不为空，不为空返回 true [ $a ]
 - 文件测试运算符
    - 运算符

    操作符     | 说明 | 举例
    ---------- | ----- | --------
    -b file|	检测文件是否是块设备文件，如果是，则返回 true。| 	[ -b $file ] 返回 false。
    -c file| 	检测文件是否是字符设备文件，如果是，则返回 true。| 	[ -c $file ] 返回 false。
    -d file| 	检测文件是否是目录，如果是，则返回 true。| 	[ -d $file ] 返回 false。
    -f file| 	检测文件是否是普通文件（既不是目录，也不是设备文件），如果是，则返回 true。| 	[ -f $file ] 返回 true。
    -g file| 	检测文件是否设置了 SGID 位，如果是，则返回 true。| 	[ -g $file ] 返回 false。
    -k file| 	检测文件是否设置了粘着位(Sticky Bit)，如果是，则返回 true。| 	[ -k $file ] 返回 false。
    -p file|	检测文件是否是有名管道，如果是，则返回 true。| 	[ -p $file ] 返回 false。
    -u file| 	检测文件是否设置了 SUID 位，如果是，则返回 true。| 	[ -u $file ] 返回 false。
    -r file| 	检测文件是否可读，如果是，则返回 true。| 	[ -r $file ] 返回 true。
    -w file| 	检测文件是否可写，如果是，则返回 true。| 	[ -w $file ] 返回 true。
    -x file| 	检测文件是否可执行，如果是，则返回 true。| 	[ -x $file ] 返回 true。
    -s file| 	检测文件是否为空（文件大小是否大于0），不为空返回 true。| 	[ -s $file ] 返回 true。
    -e file| 	检测文件（包括目录）是否存在，如果是，则返回 true。| 	[ -e $file ] 返回 true。
    - test指令
        ```bash
        cd /bin
        if test -e ./bash
        then
            echo '文件已存在!'
        else
            echo '文件不存在!'
        fi
        ```
    操作符     | 说明 
    ---------- | ----- 
    -e 文件名 |	如果文件存在则为真
    -r 文件名 |	如果文件存在且可读则为真
    -w 文件名 |	如果文件存在且可写则为真
    -x 文件名 |	如果文件存在且可执行则为真
    -s 文件名 |	如果文件存在且至少有一个字符则为真
    -d 文件名 |	如果文件存在且为目录则为真
    -f 文件名 |	如果文件存在且为普通文件则为真
    -c 文件名 |	如果文件存在且为字符型特殊文件则为真
    -b 文件名 |	如果文件存在且为块特殊文件则为真
# 流程控制
## if else-if else
 - 语法格式
    ```bash
    if <condition1>
    then
        <command1>
    elif <condition2> 
    then 
        <command2>
    else
        <commandN>
    fi
    ```
 - if else 的 [...] 判断语句中大于使用 -gt，小于使用 -lt
    ```bash
    if [ "$a" -gt "$b" ]; then
        ...
    fi
    ```
 - 如果使用 ((...)) 作为判断语句，大于和小于可以直接使用 > 和 <
    ```bash
    if (( a > b )); then
        ...
    fi
    ```
## for 循环 
 - 循环列表
    ```bash
    for var in item1 item2 ... itemN
    do
        command1
        command2
        ...
        commandN
    done
    ```
 - 循环一个数组
    ```bash
    arr=("a" "b" "c")
    for i in ${arr[@]}
    do
            echo $i
    done
    ```
 - 循环100-150的数字
    ```bash
    for i in {100..150}
    do    
      echo $i
    done
    ```
    ```bash
    for ((i=1; i<=100; i++))
    do
      echo $i
    done
    ```
## while 语句
 - 语法格式
    ```bash
    int=1
    while(( $int<=5 ))
    do
        echo $int
        let "int++"
    done
    ```
## until 循环
 - until 循环执行一系列命令直至条件为 true 时停止。 
 - 语法格式
    ```bash
    a=0
    until [ ! $a -lt 10 ]
    do
    echo $a
    a=`expr $a + 1`
    done
    # 输出0 1 2 3 4 5 6 7 8 9
    ```
## case ... esac
 - 语法格式
    ```bash
    echo '输入 1 到 4 之间的数字:'
    echo '你输入的数字为:'
    read aNum
    case $aNum in
        1)  echo '你选择了 1'
        ;;
        2)  echo '你选择了 2'
        ;;
        3)  echo '你选择了 3'
        ;;
        4)  echo '你选择了 4'
        ;;
        *)  echo '你没有输入 1 到 4 之间的数字'
        ;;
    esac
    ```
## 跳出循环
 - beark
 - continue
# 函数
 - 语法格式
    ```bash
    demoFun(){
        echo "这是我的第一个 shell 函数!"
    }
    echo "-----函数开始执行-----"
    demoFun
    echo "-----函数执行完毕-----"
    ```
 - 返回值
    ```bash
    funWithReturn(){
        echo "这个函数会对输入的两个数字进行相加运算..."
        echo "输入第一个数字: "
        read aNum
        echo "输入第二个数字: "
        read anotherNum
        echo "两个数字分别为 $aNum 和 $anotherNum !"
        return $(($aNum+$anotherNum))
    }
    funWithReturn
    echo "输入的两个数字之和为 $? !"
    # 这个函数会对输入的两个数字进行相加运算...
    # 输入第一个数字: 
    # 1
    # 输入第二个数字: 
    # 2
    # 两个数字分别为 1 和 2 !
    # 输入的两个数字之和为 3 !
    ```
 - 参数
    - 调用函数时可以向其传递参数。在函数体内部，通过 $n 的形式来获取参数的值，例如，$1表示第一个参数，$2表示第二个参数
    - \$10 不能获取第十个参数，获取第十个参数需要\${10}。当n>=10时，需要使用${n}来获取参数。 
    ```bash
    funWithParam(){
        echo "第一个参数为 $1 !"
        echo "第二个参数为 $2 !"
        echo "第十个参数为 $10 !"
        echo "第十个参数为 ${10} !"
        echo "第十一个参数为 ${11} !"
        echo "参数总数有 $# 个!"
        echo "作为一个字符串输出所有参数 $* !"
    }
    funWithParam 1 2 3 4 5 6 7 8 9 34 73
    # 第一个参数为 1 !
    # 第二个参数为 2 !
    # 第十个参数为 10 !
    # 第十个参数为 34 !
    # 第十一个参数为 73 !
    # 参数总数有 11 个!
    # 作为一个字符串输出所有参数 1 2 3 4 5 6 7 8 9 34 73 !
    ```