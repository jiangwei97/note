# LINUX
### 查看当前是什么系统
```bash
lsb_release -a
```
### 资源占用率
```bash
top
```
### 内存使用率
```bash
free -h
```
### 查看运行的端口、PID
```bash 
netstat -ntlp 
```
### 杀死进程
```bash
sudu kill -9 {pid}
ps -ef | grep <pidName> | grep -v grep | awk '{print $2}' | xargs kill -15
```
### 查看正在运行的服务
```bash
service --status-all | grep running
```
### 后台运行jar
```bash
nohup java -jar -Dfile.encoding=utf-8 xxx.jar >> <SERVICE_NAME>.log 2>&1 & echo $! > <SERVICE_NAME>.pid
```
### 查看运行中的指令
```bash
ps -ef|grep java
```
### 删除文件
```bash
rm -rf <FILE_NAME>
```
### 创建文件
```bash
touch <FILE_NAME>
```
### 文件追加
```bash
cat >> /data/text.txt <<EOF
> aaaaaa a
> wwwww b
> ssssss c
> EOF
```
### 授权
```bash
chmod 777 <FILE_NAME>
```
### 设置命令超时时间
```bash
# timeout DURATION COMMAND [ARG]...
# 其中Duration是希望命令运行的秒数 
# 如果执行未完成，则中止。在我们掌握的时间里
# s -> seconds
# m -> minutes
# h -> hours
# d -> days
timeout -5s ping baidu.com
```
### 查看文件信息
```bash
stat <FILE_NAME>
```
### 查看文件大小
```bash
# 查看文件大小
du -h <FILE_NAME>
# 查看目录大小
du -sh <DIR_NAME>
# 查询当前目录总大小
du -sh
# 查看当前目录下各文件、文件夹的大小
du -h --max-depth=1 *
# 只显示直接子目录文件及文件夹大小统计值
du -h --max-depth=0 *
```
### 打包压缩、解压缩
```bash
# 将pack压缩为pack.tar
tar -cvf pack.tar pack
# 将pack压缩为pack.tar.gz
tar -zcvf pack.tar.gz pack
# 将pack压缩为pack.tar.bz2
tar -jcvf pack.tar.bz2 pack
# 将pack压缩为pack.tar.xz
tar -Jcvf pack.tar.xz pack
```
```bash
# 解压pack.tar
tar -xvf pack.tar
# 解压pack.tar.gz
tar -zxvf pack.tar.gz
# 解压pack.tar.bz2
tar -jxvf pack.tar.bz2
# 解压pack.tar.xz
tar -Jxvf pack.tar.xz
```
### 清空文件
```bash
> <FILE_NAME>
```
### 查看IP
```bash
ip a
hostname -I
```
### 查看日志
```bash
tail -f xxx.log
```
### 查看日志关键字
```bash
tail -fn 1000 xxx.log | grep '{关键字}'
```
### yum忽略SSL证书
```bash
vi /etc/yum.conf
# 添加一行
sslverify=false
```
### wget忽略SSL证书
```bash
wget --no-check-certificate <URL>
```
### curl
- `-k` 参数表示忽略SSL证书验证；
- `-X POST` 表示使用POST请求方式；
- `-H "Content-Type: application/json"` 表示设置请求头的Content-Type为application/json；
- `-d '{"id":1,"type":1,"src":"abcde"}'` 表示发送的请求体参数为 {"id": 1}；
- `http://localhost:8080/api/xxx` 是目标API的URL。
- -w '%{http_code}'显示状态码
```bash
curl -w '%{http_code}' 'http://localhost:8080/api/xxx' \
-k \
-H "Content-Type:application/json" \
-H 'Authorization:xxx' \
-X POST \
-d '{"id":1,"type":1,"src":"abcde"}'
```
### 跨服务器复制
```bash
# 把A服务器的/root/xxx.txt 复制到本机的/home/data下
scp  -i /home/his_ose/.ssh/crawl -o StrictHostKeyChecking=no "<A-IP>":"/root/xxx.txt" "/home/data"
```
```bash
# 把本机的/home/data/xxx.txt 复制到A服务器的/root下
scp  -i /home/his_ose/.ssh/crawl -o StrictHostKeyChecking=no "/home/data/xxx.txt" "<A-IP>":"/root"
```
### 将A服务器文件复制到B服务器，使用 SSH 免密码登录
- 在 A 服务器上生成 SSH 密钥对：
    ```bash
    ssh-keygen -t rsa
    ```
    生成的密钥对默认会保存在 ~/.ssh/id_rsa（私钥）和 ~/.ssh/id_rsa.pub（公钥）
- 登录到 B 服务器，在 B 服务器的 ~/.ssh/authorized_keys 文件中粘贴 A 服务器的公钥内容。
    ```bash
    vi ~/.ssh/authorized_keys
    ```
- 保存文件并确保权限设置为 600。
- 测试 SSH 免密码登录
    ```bash
    ssh username@B_server_IP
    ```
### 查看历史命令
```bash
# 查看包含netstat的历史命令
history | grep 'netstat'
```
### 防火墙（firewall-cmd）
- 开放端口 --permanent永久生效，没有此参数重启后失效
```bash
firewall-cmd --zone=public --add-port=6379/tcp --permanent
```
- 删除一个要开放的端口号8080/tcp
```bash
  sudo firewall-cmd --remove-port=8080/tcp --permanent 
```
- 重新载入
```bash
firewall-cmd --reload
```
- 查看所有打开的端口
```bash
firewall-cmd --zone=public --list-ports
```
- 防火墙的关闭
```bash
systemctl stop firewalld
service firewalld stop
```
- 防火墙的启动
```bash
systemctl start firewalld  
service firewalld start 
```
- 防火墙的状态查询
```bash
systemctl status firewalld  
service firewalld status
```
### 安装deb
- 下载deb及其依赖的deb
```bash
# 有网络的环境
apt-get install --download-only <package-name>
# 执行后deb文件放在/var/cache/apt/archives下（ubuntu）
```
- 安装deb文件
```bash
dpkg -i *.deb
```
- 删除安装的依赖
```bash
apt-get remove <package-name>
```
- 搜索组件
```bash
apt-cache search <package-name>
```
# WINDOWS
### 查看IP
```bash
ipconfig /all
```
### 查看某端口进程号pid
```bash
netstat -ano | findstr "8090"
```
### 创建文件
```bash
type nul><FILE_NAME>
```
### Chrome关闭非安全站点证书检查
```bash
C:\Program Files\Google\Chrome\Application\chrome.exe --ignore-certificate-errors
```