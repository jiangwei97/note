# 命令
## 消费者组
### 查看消费者组
```badh
./kafka-consumer-groups.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --list
```
### 查看所有消费组详情
```badh
./kafka-consumer-groups.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --describe --all-groups
```
### 查看指定消费组详情
```badh
./kafka-consumer-groups.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --describe --group <GROUP>
```
### 查询所有消费者成员信息
```badh
./kafka-consumer-groups.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT>  --describe --all-groups --members
```
### 查询指定消费者成员信息
```badh
./kafka-consumer-groups.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT>  --describe --members --group <GROUP>
```
## 主题
### 查看当前服务器中的所有 topic
```bash
./kafka-topics.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --list
```
### 创建 topic 并设置分区数
```badh
./kafka-topics.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --create --topic <TOPIC_NAME> --partitions <PART_NUM> --replication-factor <REPL_NUM>
```
### 查看主题的详情
```bash
./kafka-topics.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --describe --topic <TOPIC_NAME>
```
### 修改分区数量
```bash
./kafka-topics.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --alter --topic <TOPIC_NAME> --partitions <PART_NUM>
```
### 删除 topic
```bash
./kafka-topics.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --delete --topic <TOPIC_NAME>
```
## 消费
```bash
./kafka-console-consumer.sh --bootstrap-server <KAFKA_IP>:<KAFKA_PORT> --from-beginning --topic <TOPIC_NAME> --group <GROUP>
```
# pom
```xml
<dependency>
    <groupId>org.apache.kafka</groupId>
    <artifactId>kafka-clients</artifactId>
    <version>2.4.1</version>
</dependency>
```
# Java Demo
## 消息生产者
```java
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/31 10:49
 */
public class KafkaProducerTest {

    public static void main(String[] args) throws IOException {
        sendMessage("testTopic", "this is a message");
    }

    private static void sendMessage(String topic, String msg){
        // 1. 创建用于连接Kafka的Properties配置
        Properties props = new Properties();
        props.put("enable.idempotence", true);
        props.put("bootstrap.servers", "127.0.0.1:9092");
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 2. 创建一个生产者对象KafkaProducer
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        // 3. 调用send发送1-100消息到指定Topic test
        try {
            // 获取返回值Future，该对象封装了返回值
            Future<RecordMetadata> future = producer.send(new ProducerRecord<>(topic, null, msg));
            // 调用一个Future.get()方法等待响应
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        // 4. 关闭生产者
        producer.close();
    }
}
```
## 消息消费者
```java
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author jiangwei jiangwei97@aliyun.com
 * @since 2023/3/31 11:00
 */
public class KafkaConsumerTest {
    private static final String brokerList="127.0.0.1:9092";
    private static final String topic="testTopic";
    private static final String groupId="JIANG_GROUP";
    public static void main(String[] args) {
        try {
            Properties props = new Properties();
            props.put(ConsumerConfig.GROUP_ID_CONFIG,groupId);//分组ID
            //props.put("bootstrap.servers", brokerList);
            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList);//broker地址列表
            //props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());//反序列化器
            //props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            KafkaConsumer<String, String> consumer= new KafkaConsumer<>(props);
            consumer.subscribe(Collections.singletonList(topic));//topic列表
            while (true){
                ConsumerRecords<String,String> records=consumer.poll(Duration.ofMillis(3000));
                if(records != null && records.count() > 0){
                    for(ConsumerRecord<String,String> record:records){
                        System.out.println(record.topic()+":"+record.offset()+":"+record.value());
                    }
                } else {
                    TimeUnit.SECONDS.sleep(5);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
```
