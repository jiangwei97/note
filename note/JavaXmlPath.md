# XmlPath For Java
## pom.xml
```xml
<!-- https://mvnrepository.com/artifact/cn.wanghaomiao/JsoupXpath -->
<dependency>
    <groupId>cn.wanghaomiao</groupId>
    <artifactId>JsoupXpath</artifactId>
    <version>2.5.1</version>
</dependency>
```
## 使用
```java
import org.seimicrawler.xpath.JXDocument;
import org.seimicrawler.xpath.JXNode;

public void getStrByXmlPath(String xmlStr, String xpath) throws IOException {
    JXDocument jxDocument = JXDocument.create(xmlStr);
    List<JXNode> jxNodes = jxDocument.selN(xpath.replace("html", ""));
    JXNode jxNode = jxDocument.selNOne(xpath);
    jxNodes.forEach(this::strWithoutTag);
}

private String strWithoutTag(JXNode node){
    String str = strWithTag(node);
    if(StringUtils.isEmpty(str)){
        return "";
    }
    return str.replaceAll("<.*?>","");
}

private String strWithTag(JXNode node){
    if(node == null){
        return "";
    }
    return node.asString();
}
```
## XmlPath语法
 - 获得id="my-id"的全部标签

    ```
    //*[@id='my-id']
    ```
 - 获得id="my-id"标签下的全部div标签下的第一个div标签列表
    ```
    //*[@id="my-id"]/div[*]/div[1]
    ```
 - 获得以id为"labels-"开头的ul标签
    ```
    //ul[starts-with(@id, 'labels-')]
    ```
 - 获得以id为"-user"结尾的ul标签
    ```
    //ul[ends-with(@id, '-user')]
    ```
 - 获得class包含"my-class"的div标签
    ```
    //div[contains(concat('', @class, ''), 'my-class')]
    ```
 - 获得class包含"blog-link"的a标签的href属性
    ```
    //a[contains(concat('', @class, ''), 'blog-link')]/@href
    ```