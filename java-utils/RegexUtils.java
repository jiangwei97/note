import com.google.common.base.Strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    /**
     * is Ip
     */
    public static boolean isIp(String ip) {
        if (Strings.isNullOrEmpty(ip))
            return false;
        String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
                + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
        return ip.matches(regex);
    }

    /**
     * is Email
     */
    public static boolean isEmail(String email) {
        return email.matches("[a-zA-Z\\d]\\w+@[a-zA-Z\\d]+\\.(cn|com|com.cn|net|gov)+");
    }

    /**
     * is hwc smn topic
     */
    public static boolean isHwcSmnTopic(String topic) {
        return topic.matches("^[a-zA-Z\\d]([a-zA-Z\\d_-]+)?$");
    }

    /**
     * have Chinese
     */
    public static boolean isChinese(String text) {
        if (Strings.isNullOrEmpty(text))
            return false;
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(text);
        return m.find();
    }

    /**
     * is Decimal
     */
    public static boolean isDecimal(String decimal, int count) {
        if (Strings.isNullOrEmpty(decimal))
            return false;
        String regex = "^(-)?(([1-9]\\d*)|(0))(\\.(\\d){" + count+ "})?$";
        return decimal.matches(regex);
    }

    /**
     * is Mobile Phone Number
     */
    public static boolean isMobilePhoneNumber(String phoneNumber) {
        if (Strings.isNullOrEmpty(phoneNumber))
            return false;
        String regex = "^((13\\d)|(15\\d)|(18[1-9]))\\d{8}$";
        return phoneNumber.matches(regex);
    }

    /**
     * is Phone Number
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        if (Strings.isNullOrEmpty(phoneNumber))
            return false;
        String regex = "^1\\d{10}$";
        return phoneNumber.matches(regex);
    }

    /**
     * has Special Char
     */
    public static boolean hasSpecialChar(String text) {
        if (Strings.isNullOrEmpty(text))
            return false;
        return text.replaceAll("[a-z]*[A-Z]*\\d*-*_*\\s*", "").length() == 0;
    }

    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION;
    }

    /**
     * is english letters, numbers, ., and underscores
     */
    public static boolean isFileName(String str) {
        if (Strings.isNullOrEmpty(str))
            return false;
        String regex = "^[\\w.]*$";
        return str.matches(regex);
    }

    /**
     * is english letters, numbers, /, and underscores
     */
    public static boolean isDirName(String str) {
        if (Strings.isNullOrEmpty(str))
            return false;
        String regex = "^[\\w/]+$";
        return str.matches(regex);
    }

    /**
     * is english letters, numbers, / and -
     */
    public static boolean isGitJobTemplateName(String str) {
        if (Strings.isNullOrEmpty(str))
            return false;
        String regex = "^[\\w\\.@\\:/\\-~]+$";
        return str.matches(regex);
    }

    public static String[] reFormat(String format, String text) {
        format = format.replaceAll("\\{([0-9])*\\}", "(.+)");
        format = format.replaceAll("\\[",  "\\\\[");
        Matcher m = Pattern.compile(format).matcher(text);
        String[] paramVars;
        if (m.find()) {
            int count = m.groupCount();
            paramVars = new String[count];
            int start = 1;
            while (start <= count) {
                paramVars[start - 1] = (m.group(start++));
            }
        } else {
            paramVars = new String[]{text};
        }
        return paramVars;
    }

}
