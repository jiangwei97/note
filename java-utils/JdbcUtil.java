import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class JdbcUtil {

    @Resource
    private DataSource dataSource;

    public List<JSONObject> selectSql(String sql){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            boolean isSelect = statement.execute();
            if (!isSelect) {
                log.error("only select");
                return null;
            }

            rs = statement.getResultSet();
            int columnCount = rs.getMetaData().getColumnCount();

            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= columnCount; i++) {
                String columnName = rs.getMetaData().getColumnLabel(i);
                columns.add(columnName);
            }
            List<JSONObject> list = new ArrayList<>();
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                ResultSet finalRs = rs;
                columns.forEach(t -> {
                    try {
                        Object value = finalRs.getObject(t);
                        jo.put(t, value);
                    } catch (SQLException throwable) {
                        throwable.printStackTrace();
                    }
                });
                list.add(jo);
            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    log.error("connection rollback failed");
                    ex.printStackTrace();
                }
            }
            return null;
        } finally {
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    log.error("rs close failed");
                    e.printStackTrace();
                }
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    log.error("statement close failed");
                    e.printStackTrace();
                }
            }
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("connection close failed");
                    e.printStackTrace();
                }
            }
        }
    }
}
